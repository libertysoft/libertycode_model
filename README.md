LibertyCode_Model
=================



Description
-----------

Library contains model components, 
allows to design data structure, 
and engine to storage it.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/model ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/model": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Entity

Entities allows to design data structure,
to manage and retrieve its data.

_Elements_

- Entity

    Allows to design an entity, 
    who is an item containing attributes and 
    allows to manage its data.

- ItemEntity

    Extends entity features. 
    Allows to design an item entity.
    Can be stored in entity collection.

- ValidEntity

    Extends item entity features. 
    Allows to design a valid entity. 
    Allows to validate its attributes.
    
- ConfigEntity

    Extends valid entity features. 
    Allows to configure its attributes.
    
- ValidatorConfigEntity

    Extends configured entity features. 
    Allows to use validation validator, 
    for attributes validation.
    
- SaveEntity

    Extends valid entity features. 
    Allows to design a savable entity.
    Can be used in repository, 
    to be stored in safe support.
    
- SaveConfigEntity

    Extends validator configured entity features. 
    Allows to design a savable entity.
    Uses configuration,
    to be used in repository, 
    to be stored in safe support.
    
- EntityCollection

    Allows to design collection of entities.
    Uses list of entities, 
    to retrieve it and 
    store entity objects with same type, if required.

- ValidEntityCollection

    Extends entity collection features. 
    Allows to design valid collection of entities.
    Allows to validate its entities.
    
- FixEntityCollection

    Extends valid entity collection features. 
    Allows to design collection of entities,
    where specified entity class path of items
    can be updated only on this class.
    
- EntityFactory

    Allows to design an entity factory, 
    to provide specific entity objects,
    with specified attributes hydrate configuration,
    from specified DI provider and configuration.
    
    Note: Entity factory requirement:
    Provided instances (entity objects) without specific limit 
    on overwritten design class and constructor.

- FixEntityFactory

    Extends entity factory features. 
    Allows to design an entity factory, 
    from a specified fixed configuration.
    
_Example_

```php
// Define new configured entity type
use liberty_code\model\entity\model\ValidatorConfigEntity;
class Entity extends ValidatorConfigEntity
{
    protected function getTabConfig()
    {
        return array(
            // Attribute 1
            [
                'key' => 'attribute_1',
                'name' => 'Attribute 1',
                'default_value' => ''
            ],
            ...
            // Attribute N
            [
                'key' => 'attribute_N',
                'name' => 'Attribute N',
                'default_value' => null
            ]
        );
    }
    
    protected function getTabRuleConfig()
    {
        return array(
            'attribute_1' => ['type_string' ...rule configuration for attribute 1],
            ...,
            'attribute_N' => [...rule configuration for attribute N]
        );
    }
}
...
// Get entities
$entity1 = new Entity(
    array(
        'attribute_1' => 'Entity 1',
        ...
        'attribute_N' => 7
    ), 
    $validator
);
$entity2 = new Entity(
    array(
        'attribute_1' => 'Entity 2',
        ...
        'attribute_N' => 8
    ),
    $validator
);
...
// Get entity collection, containing only Entity type
use liberty_code\model\entity\model\DefaultValidEntityCollection;
$entityCollection = new DefaultValidEntityCollection(Entity::class);
...
// Hydrate entity collection
$entityCollection->setItem($entity1);
$entityCollection->setItem($entity2);
...
foreach($entityCollection->getTabKey() as $key) {
    /** @var Entity $entity */
    $entity = $entityCollection->getItem($key);
    
    // Echo attribute 1 only if entity is valid
    if($entity->checkAttributeValid()) {
        echo($entity->attribute_1 . '<br />');
    }
}
/**
 * Show: 
 * Entity 1
 * Entity 2
 */
...
```

#### Persistence

Persistence allows to design save engine, 
to save one or multiple entities data, 
on specific storage support.

_Elements_

- Persistor

    Allows to design data storage engine, 
    to save entities data, 
    for all kind of entity types.

#### Repository

Repository allows to manage and prepare entity data, 
to search, load, save and remove entity,
on specific storage support,
from persistence and specific configuration.

_Elements_

- Repository
    
    Allows to design a repository,
    who is an item containing all information, 
    to manage and prepare data from entity, 
    to save in persistence.
    Repository is specific for one entity type.

- SubRepository

    Extends repository features. 
    Uses specific configuration,
    to configure sub-repositories,
    to save entity with sub-entities attributes.
    Sub-entity attribute can be entity or entity collection.
    
- SimpleRepository

    Extends sub-repository features. 
    Simple repository allows 
    to prepare data for basic saving, 
    in persistence.
    
- FixSimpleRepository

    Extends simple repository features.
    Uses specified fixed configuration, 
    to prepare and save data.
    
- MultiRepository

    Extends sub-repository features. 
    Multiple repository allows 
    to prepare data for complex saving, in persistence.
    Multiple repository focus on entity that include in group, 
    and uses id to identify entity in group.

- FixMultiRepository

    Extends multi repository features.
    Uses specified fixed configuration, 
    to prepare and save data.
    
- CollectionRepository

    Allows to design a collection repository,
    who is an item containing all information, 
    and a specific entity repository, 
    to manage and prepare data,
    from specific entity collection (list of entities),
    to save at same time in persistence.
    Collection repository is specific for one entity type.

- SubCollectionRepository

    Extends collection repository features. 
    Uses specific configuration to configure sub-repositories,
    and sub-repository,
    to save entities with sub-entities attributes.
    Sub-entities can be saved in collection too, 
    to benefit multi-saving, if it's configured.

- SimpleCollectionRepository

    Extends sub-collection repository features.
    Simple collection repository uses simple repository  
    to prepare data from entity collection, 
    for basic saving, in persistence.

- FixSimpleCollectionRepository

    Extends simple collection repository features.
    Uses specified fixed configuration, 
    to prepare and save data.
    
- MultiCollectionRepository

    Extends sub-collection repository features. 
    Multiple collection repository uses multiple repository 
    to prepare data from entity collection, 
    for complex saving, in persistence.

- FixMultiCollectionRepository

    Extends multi collection repository features. 
    Uses specified fixed configuration, 
    to prepare and save data.
    
_Example_

```php
// Define new entity repository type
use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\repository\multi\model\MultiRepository;
class Repository extends MultiRepository
{
    public function __construct(PersistorInterface $objPersistor)
    {
        parent::__construct(
            array(
                'entity_class_path' => 'entity class path',
                'attribute_key_id' => 'attribute_id',
                ...
            ), 
            $objPersistor
        );
    }
}
...
// Get repository
$persistor = ...(persistor object);
$repository = new Repository($persistor);
...
// Save entity
$entity = ...(new save entity object);
$saved = $repository->save($entity);
$id = $entity->attribute_id;
...
// Load entity
$entity = ...(new save entity object);
$loaded = $repository->load($entity, $id);
...
// Remove entity
$removed = $repository->remove($entity);
...
```

#### Datetime

Date, time and timezone features, 
usable in model components.

_Elements_

- DateTimeFactory

    Allows to design a datetime factory, 
    to provide new datetime instances,
    from specified configuration,
    can be used on model components.

#### Rule

Rules allows to check specified data validation, 
using model components.

_Elements_

- EntityCollectionRule

    Extends rule features. 
    Allows to check specified data validation, 
    using entity collection.

- AttrExistEntityCollectionRule

    Extends entity collection rule features. 
    Allows to check if specified data exists,
    in specified entity collection.
    
- ValidEntityRule

    Extends rule features. 
    Allows to check if specified data is entity,
    which successes validation.
    
- ValidEntityCollectionRule

    Extends rule features. 
    Allows to check if specified data is entity collection,
    which successes validation.
    
- NewEntityRule

    Extends rule features. 
    Allows to check if specified data is new entity or not.
    
- NewEntityCollectionRule

    Extends rule features. 
    Allows to check if specified data is entity collection,
    where each entity is new entity or not.

---


