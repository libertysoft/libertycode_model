<?php

namespace liberty_code\model\entity\test;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\model\ValidatorConfigEntity;



/**
 * @property string $id
 * @property string $nm
 */
class ClassTest3 extends ValidatorConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $result = $this->getAttributeValue('id');
        if(is_null($result))
        {
            $result = parent::getStrBaseKey();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array(
            // Attribute 1
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'id',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'identifier',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Identifier',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute 2
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'nm',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Last name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'DOE'
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Init var
        $result = false;
        $tabError = array();
        $strAlias = $this->getAttributeAlias($strKey);
        $strNm = (
            is_null($strAlias) ?
                $this->getAttributeName($strKey) :
                $strAlias
        );

        // Check by attribute
        switch($strKey)
        {
            case'id':
                $result =
                    is_null($value) ||
                    is_int($value);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid integer.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'nm':
                $result = is_string($value) && (trim($value) != '');

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;
        }

        // Return result
        return $result;
    }



}