<?php

// Init var
$strTestRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strTestRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strTestRootAppPath . '/include/Include.php');

// Load library test
require_once($strTestRootAppPath . '/vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

// Load test
require_once($strTestRootAppPath . '/src/entity/test/ClassTest.php');
require_once($strTestRootAppPath . '/src/entity/test/ClassTest2.php');
require_once($strTestRootAppPath . '/src/entity/test/ClassTest3.php');

// Use
use liberty_code\model\entity\test\ClassTest;
use liberty_code\model\entity\test\ClassTest2;
use liberty_code\model\entity\test\ClassTest3;
use liberty_code\model\entity\model\DefaultValidEntityCollection;



// Init var
$objCollectionTest = new DefaultValidEntityCollection(ClassTest::class);
$objCollectionTest2 = new DefaultValidEntityCollection(ClassTest::class);
$objTest = new ClassTest(
    array(
    'id' => 1,
    'nmFirst' => 'Serge',
    'nm' => 'BOUZID'
    ),
    $objValidator
);



// Test check/get attribute
$tabKey = array(
    'id', // Found
	'nmFirst', // Found
	'nm', // Found
    'test', // Not found
    'nmNick', // Found
    7, // Invalid format
    'age', // Found
    'isDev', // Found
    true // Invalid format
);

foreach($tabKey as $strKey)
{
	echo('Test check, get attribute "'.strval($strKey).'": <br />');
	try{
		echo('Check: <pre>');var_dump($objTest->checkAttributeExists($strKey));echo('</pre>');
        echo('Get name: <pre>');var_dump($objTest->getAttributeName($strKey));echo('</pre>');
        echo('Get alias: <pre>');var_dump($objTest->getAttributeAlias($strKey));echo('</pre>');
        echo('Get default value: <pre>');var_dump($objTest->getAttributeDefaultValue($strKey));echo('</pre>');
        echo('Get value: <pre>');var_dump($objTest->getAttributeValue($strKey));echo('</pre>');

	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test reset attribute
$objTest->resetAttribute();

foreach($objTest->getTabAttributeKey() as $strKey)
{
    echo('Test reset attribute "'.strval($strKey).'": <br />');
    echo('Get value: <pre>');var_dump($objTest->getAttributeValue($strKey));echo('</pre>');
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test valid attribute
$objTest->nmNick = true;
$objTest->age = 'test';
$objTest->isDev = 'test 2';
$tabError = array();
echo('Test valid attribute: <br />');
echo('Check: <pre>');var_dump($objTest->checkAttributeValid(null, $tabError));echo('</pre>');
echo('Error: <pre>');var_dump($tabError);echo('</pre>');

try
{
    $objTest->setAttributeValid();
}
catch(Exception $e)
{
    echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
}
finally
{
    echo('Validation done!<br /><br />');
}

foreach($objTest->getTabAttributeKey() as $strKey)
{
    echo('Get '.$strKey.' value: <pre>');var_dump($objTest->getAttributeValue($strKey, false));echo('</pre>');
    echo('Get formatted '.$strKey.' value: <pre>');var_dump($objTest->getAttributeValue($strKey));echo('</pre>');
    echo('<br />');
}

echo('<br /><br /><br />');



// Test valid attribute 2
$objTest->nmNick = 'Sergio';
$objTest->age = 30;
$objTest->isDev = '1';
$tabError = array();
echo('Test valid attribute 2: <br />');
echo('Check: <pre>');var_dump($objTest->checkAttributeValid(null, $tabError));echo('</pre>');
echo('Error: <pre>');var_dump($tabError);echo('</pre>');

try
{
    $objTest->setAttributeValid();
}
catch(Exception $e)
{
    echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
}
finally
{
    echo('Validation done!<br /><br />');
}

foreach($objTest->getTabAttributeKey() as $strKey)
{
    echo('Get '.$strKey.' value: <pre>');var_dump($objTest->getAttributeValue($strKey, false));echo('</pre>');
    echo('Get formatted '.$strKey.' value: <pre>');var_dump($objTest->getAttributeValue($strKey));echo('</pre>');
    echo('<br />');
}

echo('<br /><br /><br />');



// Test set collection item
$tabItem = array(
    new ClassTest(
        array(
            'id' => 1,
            'nmFirst' => 'Fnm_1',
            'nm' => 'NM_1',
            'age' => 18,
            'isDev' => 1
        ),
        $objValidator
    ), // Ok
    new ClassTest2(
        array(
            'id' => 2,
            'nmFirst' => 'Fnm_2',
            'nm' => 'NM_2',
            'age' => 25,
            'isDev' => 0
        ),
        $objValidator
    ), // Ok
    new ClassTest3(
        array(
            'id' => 31,
            'nm' => 'NM_31'
        ),
        $objValidator
    ), // Ko: bad type
    new ClassTest(
        array(
            'id' => 3,
            'nmFirst' => 'Fnm_3',
            'nm' => 'NM_3',
            'age' => 27,
            'isDev' => true
        ),
        $objValidator
    ), // Ok
    new ClassTest(
        array(
            'id' => 4,
            'nmFirst' => 'Fnm_4',
            'nm' => 'NM_4',
            'age' => 30,
            'isDev' => false
        ),
        $objValidator
    ), // Ok
    new ClassTest3(
        array(
            'id' => 32,
            'nm' => 'NM_32'
        ),
        $objValidator
    ), // Ko: bad type
    new ClassTest2(
        array(
            'id' => 5,
            'nmFirst' => 'Fnm_5',
            'nm' => 5,
            'age' => 37,
            'isDev' => '1'
        ),
        $objValidator
    ), // Ok
    new ClassTest(
        array(
            'id' => 6,
            'nmFirst' => 'Fnm_6',
            'nm' => '',
            'age' => 40,
            'isDev' => '0'
        ),
        $objValidator
    ) // Ok
);

foreach($tabItem as $item)
{
    try{
        $objCollectionTest->setItem($item);

        /** @var ClassTest $item */
        echo('Test set item "'.$item->getStrKey().'": <br />');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

$tabError = array();
try
{
    $objCollectionTest->setValid();
}
catch(Exception $e)
{
    echo(get_class($e) . ' - ' . $e->getMessage().'<br /><br />');
    $objCollectionTest->checkValid(null, null, $tabError);
}
finally
{
    echo('Validation done!<br /><br />');
    if(count($tabError) > 0)
    {
        echo('Error: <pre>');var_dump($tabError);echo('</pre>');
    }
}

echo('<br /><br /><br />');



// Test check/get collection item
$tabKey = array(
    ClassTest::getStrFormatKey(1), // Found
    ClassTest::getStrFormatKey(2), // Found
    ClassTest::getStrFormatKey(3), // Found
    ClassTest::getStrFormatKey(4), // Found
    'test', // Not found
    ClassTest::getStrFormatKey(5), // Found
    true, // Invalid format
    ClassTest::getStrFormatKey(6) // Found
);

foreach($tabKey as $strKey)
{
    echo('Test check, get item "'.strval($strKey).'": <br />');
    try{
        //$strKey = ClassTest::getStrFormatKey($strKey);
        $boolExist = $objCollectionTest->checkExists($strKey);
        $item = $objCollectionTest->getItem($strKey);
        echo('Check: <pre>');var_dump($boolExist);echo('</pre>');

        if($boolExist)
        {
            echo('Get: <pre>');
            var_dump($item->id);
            var_dump($item->nmFirst);
            var_dump($item->nm);
            var_dump($item->nmNick);
            var_dump($item->age);
            var_dump($item->isDev);
            echo('</pre>');
        }
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get collection item
foreach($objCollectionTest->getTabKey() as $strKey)
{
    try{
        $item = $objCollectionTest->getItem($strKey);

        /** @var ClassTest $item */
        echo('Test get item "'.$strKey.'": <pre>');
        var_dump($item->id);
        var_dump($item->nmFirst);
        var_dump($item->nm);
        var_dump($item->nmNick);
        var_dump($item->age);
        var_dump($item->isDev);
        echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test search collection item
$tabGetConfig = array(
    //*
    [
        'attribute_key' => 'isDev',
        'operation' => '=',
        'value' => 1
    ],
    [
        'attribute_key' => 'id',
        'operation' => '<=',
        'value' => 3,
        'operation_not' => true
    ],
    [
        'attribute_key' => 'nm',
        'operation' => 'start',
        'value' => 'NM',
        'operation_not' => false
    ],
    //*/
    /*
    [
        'attribute_key' => 'nm',
        'operation' => 'end',
        'value' => '5',
        'operation_not' => true
    ],
    [
        'attribute_key' => 'nmFirst',
        'operation' => 'contain',
        'value' => 'm_',
        'operation_not' => 0
    ]
    //*/
);

foreach($objCollectionTest->getTabItem($tabGetConfig) as $strKey => $item)
{
    try{
        /** @var ClassTest $item */
        echo('Test search item "'.$strKey.'": <pre>');
        var_dump($item->id);
        var_dump($item->nmFirst);
        var_dump($item->nm);
        var_dump($item->nmNick);
        var_dump($item->age);
        var_dump($item->isDev);
        echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test remove collection item
$objCollectionTest2->setTabItem($objCollectionTest);
$objCollectionTest->removeItemAll();
echo('Test remove item: <pre>');var_dump($objCollectionTest->getTabKey());echo('</pre>');

echo('<br /><br /><br />');



// Test set collection tabItem
foreach($objCollectionTest2->getTabKey() as $strKey)
{
    try{
        $item = $objCollectionTest2->getItem($strKey);

        /** @var ClassTest $item */
        echo('Test set list items "'.$strKey.'": <pre>');
        var_dump($item->id);
        var_dump($item->nmFirst);
        var_dump($item->nm);
        var_dump($item->nmNick);
        var_dump($item->age);
        var_dump($item->isDev);
        echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


