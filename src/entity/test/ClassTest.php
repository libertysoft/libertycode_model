<?php

namespace liberty_code\model\entity\test;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\model\ValidatorConfigEntity;



/**
 * @property string $id
 * @property string $nmFirst
 * @property string $nm
 * @property string $nmNick
 * @property integer $age
 * @property boolean $isDev
 */
class ClassTest extends ValidatorConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $result = $this->getAttributeValue('id');
        if(is_null($result))
        {
            $result = parent::getStrBaseKey();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute 1
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'id',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'identifier',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Identifier',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute 2
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'nmFirst',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'FirstName',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'First name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'John'
            ],

            // Attribute 3
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'nm',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Last name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'DOE'
            ],

            // Attribute 4
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'nmNick',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'NickName',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Nick name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute 5
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'age',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Age',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Age',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 20
            ],

            // Attribute 6
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'isDev',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'IsDevelopper',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Is developper',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => true
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array(
            'nmFirst' => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ]
            ],
            'nm' => [
                'type_string',
                [
                    'sub_rule_not',
                    [
                        'rule_config' => ['is_empty'],
                        'error_message_pattern' => '%1$s is empty.'
                    ]
                ]
            ],
            'nmNick' => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => [
                                'type_string',
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty']
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a valid string.'
                    ]
                ]
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Init var
        $result = parent::checkAttributeValueValid($strKey, $value, $tabError);
        $strAlias = $this->getAttributeAlias($strKey);
        $strNm = (
            is_null($strAlias) ?
                $this->getAttributeName($strKey) :
                $strAlias
        );

        // Check by attribute
        switch($strKey)
        {
            case'id':
                $result =
                    is_null($value) ||
                    is_int($value);

                if(!$result)
                {
                    $tabError['integer'] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid integer.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

                /*
            case 'nmFirst':
            case 'nm':
                $result = is_string($value) && (trim($value) != '');

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'nmNick':
                $result =
                    is_null($value) ||
                    (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError['null_string'] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;
                //*/

            case'age':
                $result = is_int($value) && ($value > 0);

                if(!$result)
                {
                    $tabError['positive_integer'] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a strict positive integer.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case'isDev':
                $result = is_bool($value);

                if(!$result)
                {
                    $tabError['boolean'] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a boolean.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $result = parent::getAttributeValueFormatGet($strKey, $value);

        // Format by attribute
        switch($strKey)
        {
            case'isDev':
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $result = parent::getAttributeValueFormatSet($strKey, $value);;

        // Format by attribute
        switch($strKey)
        {
            case'isDev':
                $result = (
                    // Case boolean
                    is_bool($value) ? $value : (
                        // Case numeric
                        is_numeric($value) ? intval($value) != 0 : (
                            // Case else: format impossible
                            $value
                        )
                    )
                );

                break;
        }

        // Return result
        return $result;
    }



}