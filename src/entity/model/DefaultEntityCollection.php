<?php
/**
 * Description :
 * This class allows to define default entity collection class.
 * key => entity.
 * Can be consider is base of all collection types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\model;

use liberty_code\library\bean\model\DefaultBean;
use liberty_code\model\entity\api\EntityCollectionInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\api\ItemEntityInterface;
use liberty_code\model\entity\exception\CollectionItemClassPathInvalidFormatException;
use liberty_code\model\entity\exception\CollectionKeyInvalidFormatException;
use liberty_code\model\entity\exception\CollectionValueInvalidFormatException;
use liberty_code\model\entity\exception\CollectionGetConfigInvalidFormatException;



class DefaultEntityCollection extends DefaultBean implements EntityCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /** @var null|string */
    protected $strItemClassPath;
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param string $strItemClassPath = null
	 * @param array $tabData = array()
     */
    public function __construct($strItemClassPath = null, array $tabData = array())
    {
        // Init item class path
        $this->setItemClassPath($strItemClassPath);

        // Call parent constructor
        parent::__construct($tabData);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value, $this->getStrItemClassPath());

            // Check key argument
            /** @var ItemEntityInterface $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }
	
	
	
	
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
	public function checkExists($strKey)
	{
		// Return result
        return (!is_null($this->getItem($strKey)));
	}



	
	
	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrItemClassPath()
    {
        // Return result
        return $this->strItemClassPath;
    }



    /**
     * @inheritdoc
     */
    public function getTabKey(array $tabConfig = null)
    {
        // Return result
        return array_keys($this->getTabItem($tabConfig));
    }



    /**
     * Selection configuration format:
     * [
     *     // Selection 1
     *     [
     *         attribute_key(required): "string attribute key",
     *         operation(required): "string operation value" (@see ConstEntity::getTabCollectionGetConfigOperation() ),
     *         value(required): mixed value to compare,
     *         operation_not(optional: got false if not found): true / false
     *     ],
     *     ...,
     *     // Selection N
     *     [...]
     * ]
     * @inheritdoc
     * @throws CollectionGetConfigInvalidFormatException
     */
    public function getTabItem(array $tabConfig = null)
    {
        // Init var
        $result = array();
        $tabGetConfig = $tabConfig;
        $tabKey = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);

        // Check configuration
        CollectionGetConfigInvalidFormatException::setCheck($tabGetConfig);

        foreach($tabKey as $strKey)
        {
            // Get info
            $objItem = $this->getItem($strKey);

            // Check selection
            $boolSelect = true;
            if(!is_null($tabGetConfig))
            {
                for($intCpt = 0; ($intCpt < count($tabGetConfig)) && $boolSelect; $intCpt++)
                {
                    // Get info
                    $getConfig = $tabGetConfig[$intCpt];
                    $strAttributeKey = $getConfig[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY];
                    $strOperation = $getConfig[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION];
                    $value = $getConfig[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE];
                    $itemValue = $objItem->getAttributeValue($strAttributeKey);
                    $boolNot = (
                        isset($getConfig[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION_NOT]) ?
                            (intval($getConfig[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION_NOT]) != 0) :
                            false
                    );

                    // Check get configuration is valid
                    if(!in_array($strAttributeKey, $objItem->getTabAttributeKey()))
                    {
                        throw new CollectionGetConfigInvalidFormatException(serialize($getConfig));
                    }

                    // Check selection
                    $boolSelect =
                        (
                            (is_object($itemValue) && is_object($value)) ||
                            ((!is_object($itemValue)) && (!is_object($value)))
                        ) &&
                        (
                            (
                                $strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL &&
                                ($itemValue == $value)
                            ) ||
                            (
                                $strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_STRICT_EQUAL &&
                                ($itemValue === $value)
                            ) ||
                            (
                                $strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_GREATER &&
                                ($itemValue > $value)
                            ) ||
                            (
                                $strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_GREATER_EQUAL &&
                                ($itemValue >= $value)
                            ) ||
                            (
                                $strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_LESS &&
                                ($itemValue < $value)
                            ) ||
                            (
                                $strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_LESS_EQUAL &&
                                ($itemValue <= $value)
                            ) ||
                            (
                                ($strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_START) &&
                                (strlen($itemValue) >= strlen($value)) &&
                                (substr($itemValue, 0, strlen($value)) == $value)
                            ) ||
                            (
                                $strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_END &&
                                (strlen($itemValue) >= strlen($value)) &&
                                (substr($itemValue, (strlen($value) * -1)) == $value)
                            ) ||
                            (
                                $strOperation == ConstEntity::COLLECTION_GET_CONFIG_OPERATION_CONTAIN &&
                                (strpos($itemValue, $value) !== false)
                            )
                        );

                    // Application operator not, if required
                    $boolSelect =
                        ($boolNot && (!$boolSelect)) ||
                        ((!$boolNot) && $boolSelect);
                }
            }

            // Register item in result, if required
            if($boolSelect)
            {
                $result[$strKey] = $objItem;
            }
        }

        // Return result
        return $result;
    }



	/**
	 * @inheritdoc
	 */
	public function getItem($strKey)
	{
		// Init var
		$result = null;
		
		// Try to get item entity if found
		try
		{
			if($this->beanDataExists($strKey))
			{
				$result = $this->beanGetData($strKey);
			}
		}
		catch(\Exception $e)
		{
		}
		
		// Return result
		return $result;
	}
	
	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * @inheritdoc
     * @throws CollectionItemClassPathInvalidFormatException
     */
    public function setItemClassPath($strClassPath)
    {
        // Check arguments
        CollectionItemClassPathInvalidFormatException::setCheck($strClassPath);

        // Set data
        $this->strItemClassPath = $strClassPath;
    }



	/**
	 * @inheritdoc
	 * @throws CollectionKeyInvalidFormatException
	 * @throws CollectionValueInvalidFormatException
     */
	public function setItem(ItemEntityInterface $objItem)
	{
		// Init var
		$strKey = $objItem->getStrKey();
		
		// Register instance
		$this->beanPutData($strKey, $objItem);
		
		// return result
		return $strKey;
	}



    /**
     * @inheritdoc
     */
    public function setTabItem($tabItem)
    {
        // Init var
        $result = array();

        // Case index array of items
        if(is_array($tabItem))
        {
            // Run all items and for each, try to set
            foreach($tabItem as $item)
            {
                $strKey = $this->setItem($item);
                $result[] = $strKey;
            }
        }
        // Case collection of items
        else if($tabItem instanceof EntityCollectionInterface)
        {
            // Run all items and for each, try to set
            foreach($tabItem->getTabKey() as $strKey)
            {
                $objItem = $tabItem->getItem($strKey);
                $strKey = $this->setItem($objItem);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeItem($strKey)
    {
        // Init var
        $result = $this->getItem($strKey);

        // Remove item
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function removeItemAll(array $tabConfig = null)
    {
        // Ini var
        $tabKey = $this->getTabKey($tabConfig);

        foreach($tabKey as $strKey)
        {
            $this->removeItem($strKey);
        }
    }



}