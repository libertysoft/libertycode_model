<?php
/**
 * Description :
 * This class allows to define default entity class.
 * Default entity uses bean to manage attributes data.
 * Can be consider is base of all entity types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\model\entity\api\EntityInterface;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\model\entity\exception\ConfigInvalidFormatException;



abstract class DefaultEntity extends FixBean implements EntityInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabValue = array())
    {
        // Call parent constructor
        parent::__construct();

        // Hydrate attribute values
        $this->hydrate($tabValue, true);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    protected function beanHydrateDefault()
    {
        // Reset attribute data
        $this->resetAttribute();
    }



    /**
     * @inheritdoc
     */
    public function hydrate(array $tabValue = array(), $boolCheckExist = true)
    {
        // Init var
        $result = true;
        $boolCheckExist = (is_bool($boolCheckExist) ? $boolCheckExist : true);

        // Run all attributes
        foreach($tabValue as $strKey => $value)
        {
            $boolSet  = false;

            // Set attribute value, if required
            if((!$boolCheckExist) || $this->checkAttributeExists($strKey))
            {
                $this->setAttributeValue($strKey, $value);
                $boolSet  = true;
            }

            $result = $result && $boolSet;
        }

        // Return result
        return $result;
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = $this->getTabAttributeKey();
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Return result
        return true;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkAttributeExists($strKey)
    {
        // Return result
        return parent::beanDataExists($strKey);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabAttributeKey()
    {
        // Return result
        return $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
    }



    /**
     * @inheritdoc
     * Overwrite it to implement specific name.
     */
    public function getAttributeName($strKey)
    {
        // Return result
        return $strKey;
    }



    /**
     * @inheritdoc
     * Overwrite it to implement specific alias.
     */
    public function getAttributeAlias($strKey)
    {
        // Return result
        return null;
    }



    /**
     * @inheritdoc
     * Overwrite it to implement specific default value.
     */
    public function getAttributeDefaultValue($strKey)
    {
        // Return result
        return null;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValue($strKey, $boolFormat = true)
    {
        // Init var
        $result = parent::beanGetData($strKey);

        // Format value, if required
        if($boolFormat)
        {
            $result = $this->getAttributeValueFormatGet($strKey, $result);
        }

        // Return result
        return $result;
    }



    /**
     * Get specified attribute formatted value when get action required.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified attribute formatted value when set action required.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabData($boolFormat = true)
    {
        // Init var
        $result = array();

        // Run all attribute keys
        foreach($this->getTabAttributeKey() as $strKey)
        {
            // Get info
            $value = $this->getAttributeValue($strKey, $boolFormat);

            // Register attribute in result
            $result[$strKey] = $value;
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

	/**
	 * @inheritdoc
	 */
    public function setAttributeValue($strKey, $value, $boolFormat = true)
    {
        // Format value, if required
        if($boolFormat)
        {
            $value = $this->getAttributeValueFormatSet($strKey, $value);
        }

        // Set data
        parent::beanSetData($strKey, $value);
    }



    /**
     * @inheritdoc
     */
    public function resetAttribute($strKey = null)
    {
        // Case whole attributes reset required
        if(is_null($strKey))
        {
            // Run all attributes
            foreach($this->getTabAttributeKey() as $strKey)
            {
                // Reset attribute
                $this->resetAttribute($strKey);
            }
        }
        // Else: case specified attribute reset required
        else
        {
            // Set attribute default value
            $this->setAttributeValue($strKey, $this->getAttributeDefaultValue($strKey));
        }
    }





    // Methods public interfaces overwrite (required for magic methods)
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanDataExists($key)
    {
        // Return result
        return $this->checkAttributeExists($key);
    }



    /**
     * @inheritdoc
     */
    public function beanSetData($key, $val)
    {
        $this->setAttributeValue($key, $val, true);
    }



    /**
     * @inheritdoc
     */
    public function beanGetData($key)
    {
        // Return result
        return $this->getAttributeValue($key, true);
    }



}