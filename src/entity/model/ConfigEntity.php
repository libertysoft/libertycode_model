<?php
/**
 * Description :
 * This class allows to define configured entity class.
 * Configured entity allows to configure its attributes.
 * Can be consider is base of all entity types, where attributes configuration required.
 *
 * Configured entity allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     "string attribute key",
 *
 *     OR
 *
 *     [
 *         key(required): "string attribute key",
 *         name(optional: got key, if not found): "string attribute name",
 *         alias(optional: got null, if not found): "string attribute alias",
 *         default_value(optional: got null, if not found): mixed attribute default value
 *     ]
 *
 *     ...
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\model;

use liberty_code\model\entity\model\DefaultValidEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\exception\ConfigInvalidFormatException;



abstract class ConfigEntity extends DefaultValidEntity
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Attribute configuration array.
     * @var array
     */
    protected $tabConfig;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabValue = array())
    {
        // Init var
        $this->tabConfig = array();

        // Call parent constructor
        parent::__construct($tabValue);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    protected function beanHydrateDefault()
    {
        // Set attribute configuration
        $this->hydrateTabConfig();

        // Hydrate attribute data
        $this->hydrateTabData();

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate attribute configuration array,
     * from configuration specified in feature 'getTabConfig'.
     *
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateTabConfig()
    {
        // Init var
        $this->tabConfig = array();
        $tabConfigSrc = $this->getTabConfig();

        // Set check configuration
        ConfigInvalidFormatException::setCheck($tabConfigSrc);

        // Run all attribute configurations
        foreach($tabConfigSrc as $configSrc)
        {
            // Get info
            $strKey = (is_string($configSrc) ? $configSrc : $configSrc[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY]);

            // Get configuration
            $tabConfig = array();
            $tabConfigKey = $this->getTabAttributeConfigKey();
            foreach($tabConfigKey as $strConfigKey)
            {
                if(array_key_exists($strConfigKey, $configSrc))
                {
                    $tabConfig[$strConfigKey] = $configSrc[$strConfigKey];
                }
            }

            // Register attribute configuration
            $this->tabConfig[$strKey] = $tabConfig;
        }
    }



    /**
     * Hydrate attribute data array,
     * from configuration property.
     */
    protected function hydrateTabData()
    {
        // Init data, from configuration
        $tabData = array();
        foreach(array_keys($this->tabConfig) as $strKey)
        {
            $tabData[$strKey] = null;
        }

        // Hydrate data
        $this->__beanTabData = $tabData;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of attribute configuration.
     *
     * @return array
     */
    abstract protected function getTabConfig();



    /**
     * Get associative array of specified attribute configuration.
     *
     * @param string $strKey
     * @return array
     */
    protected function getAttributeConfig($strKey)
    {
        // Set check argument
        parent::beanGetData($strKey);

        // Init var
        $result = array();
        $tabConfig = $this->tabConfig;

        // Check configuration found
        if(array_key_exists($strKey, $tabConfig))
        {
            $result = $tabConfig[$strKey];
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of attribute configuration keys, for specified attribute if required.
     * If no attribute specified, get global attribute configuration keys.
     *
     * @param string $strKey = null
     * @return array
     */
    protected function getTabAttributeConfigKey($strKey = null)
    {
        // Init var
        $result = array(
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME,
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS,
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE
        );

        // Get attribute configuration keys, for specified attribute if required
        if(!is_null($strKey))
        {
            $result = array_keys($this->getAttributeConfig($strKey));
        }

        // Return result
        return $result;
    }



    /**
     * Get specified attribute configuration item.
     *
     * @param string $strKey
     * @param string $strItem
     * @param mixed $default = null
     * @return mixed
     */
    protected function getAttributeConfigItem($strKey, $strItem, $default = null)
    {
        // Init var
        $result = $default;
        $tabConfig = $this->getAttributeConfig($strKey);

        // Check configuration found
        if(isset($tabConfig[$strItem]))
        {
            $result = $tabConfig[$strItem];
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeName($strKey)
    {
        // Return result
        return $this->getAttributeConfigItem(
            $strKey,
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME,
            parent::getAttributeName($strKey)
        );
    }



    /**
     * @inheritdoc
     */
    public function getAttributeAlias($strKey)
    {
        // Return result
        return $this->getAttributeConfigItem(
            $strKey,
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS,
            parent::getAttributeAlias($strKey)
        );
    }



    /**
     * @inheritdoc
     */
    public function getAttributeDefaultValue($strKey)
    {
        // Return result
        return $this->getAttributeConfigItem(
            $strKey,
            ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE,
            parent::getAttributeDefaultValue($strKey)
        );
    }



}