<?php
/**
 * Description :
 * This class allows to define validator configured entity class.
 * Validator configured entity allows to use validator, for attributes validation.
 * Can be consider is base of all configured entity types.
 *
 * Validator configured entity allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     Configured entity configuration
 * ]
 *
 * Note:
 * -> Attributes are used as data, on validator:
 *     - Data name = attribute key.
 *     - Data value = attribute value.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\model;

use liberty_code\model\entity\model\ConfigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ToolBoxValidEntity;



abstract class ValidatorConfigEntity extends ConfigEntity
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Validator object
     * @var null|ValidatorInterface
     */
    protected $objValidator;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|ValidatorInterface $objValidator = null
     */
    public function __construct(array $tabValue = array(), ValidatorInterface $objValidator = null)
    {
        // Call parent constructor
        parent::__construct($tabValue);

        // Set validator
        $this->setValidator($objValidator);
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Init var
        $result = true;
        $tabError = array();
        $objValidator = $this->getObjValidator();

        // Get attribute validation, if required
        if(!is_null($objValidator))
        {
            // Get attribute rule configuration
            $tabRuleConfig = $this->getTabRuleConfig();
            $tabRuleConfig = (array_key_exists($strKey, $tabRuleConfig) ? $tabRuleConfig[$strKey] : array());

            // Get attribute validation
            $tabErrorException = array();
            $result = $objValidator->checkDataIsValid(
                $strKey,
                $value,
                $tabRuleConfig,
                $tabError,
                $tabErrorException
            );

            // Get attribute errors (complete errors with exceptions, if required)
            $tabError = ToolBoxValidEntity::getTabMergeAttributeError(
                $tabError,
                $tabErrorException
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get rule configurations array.
     * Overwrite it to implement specific rule configurations.
     *
     * Return array format:
     * @see ValidatorInterface::checkTabDataIsValid() rule configurations array format.
     *
     * @return array
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return array();
    }



    /**
     * Get validator object.
     *
     * @return null|ValidatorInterface
     */
    public function getObjValidator()
    {
        // Return result
        return $this->objValidator;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set validator object.
     *
     * @param null|ValidatorInterface $objValidator = null
     */
    public function setValidator(ValidatorInterface $objValidator = null)
    {
        // Set Validator
        $this->objValidator = $objValidator;
    }



}