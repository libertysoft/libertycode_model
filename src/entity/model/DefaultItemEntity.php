<?php
/**
 * Description :
 * This class allows to define default item entity class.
 * Can be consider is base of all item entity types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\model;

use liberty_code\model\entity\model\DefaultEntity;
use liberty_code\model\entity\api\ItemEntityInterface;

use liberty_code\model\entity\library\ConstEntity;



abstract class DefaultItemEntity extends DefaultEntity implements ItemEntityInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string basic key (main part of key).
     * Overwrite it to implement specific base key.
     *
     * @return string
     */
    protected function getStrBaseKey()
    {
        // Return result
        return static::getStrHash($this);
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Return result
        return static::getStrFormatKey($this->getStrBaseKey());
    }





    // Methods statics
    // ******************************************************************************

    /**
     * Get string hash
     *
     * @param ItemEntityInterface $objEntity
     * @return string
     */
    public static function getStrHash(ItemEntityInterface $objEntity)
    {
        // Return result
        return spl_object_hash($objEntity);
    }



    /**
     * Get string formatted key, from specified key.
     *
     * @param mixed $key
     * @return string
     */
    public static function getStrFormatKey($key)
    {
        // Return result
        return sprintf(ConstEntity::CONF_DEFAULT_ENTITY_KEY_PATTERN_DECORATION, strval($key));
    }



}