<?php
/**
 * Description :
 * This class allows to define default valid entity collection class.
 * Can be consider is base of all valid collection types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\model;

use liberty_code\model\entity\model\DefaultEntityCollection;
use liberty_code\model\entity\api\ValidEntityCollectionInterface;

use liberty_code\model\entity\library\ToolBoxValidEntity;
use liberty_code\model\entity\api\ValidEntityInterface;



class DefaultValidEntityCollection extends DefaultEntityCollection implements ValidEntityCollectionInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkValid(
        array $tabConfig = null,
        $attrKey = null,
        array &$tabError = array()
    )
    {
        // Init check entity function
        $callCheckEntityIsValid = function($objEntity, array &$tabError) use ($attrKey)
        {
            return (
                ($objEntity instanceof ValidEntityInterface) ?
                    $objEntity->checkAttributeValid($attrKey, $tabError) :
                    true
            );
        };

        // Init var
        $tabError = array();
        $result = ToolBoxValidEntity::checkEntityCollectionIsValid(
            $this,
            $callCheckEntityIsValid,
            $tabError,
            $tabConfig
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function setValid(
        array $tabConfig = null,
        $attrKey = null
    )
    {
        // Check validation
        $result = true;

        // Run all items
        $tabItem = $this->getTabItem($tabConfig);
        foreach($tabItem as $objEntity)
        {
            // Get info
            $boolEntityValid = (
                ($objEntity instanceof ValidEntityInterface) ?
                    $objEntity->setAttributeValid($attrKey) :
                    true
            );

            // Register result
            $result = $result && $boolEntityValid;
        }

        // Return result
        return $result;
    }



}