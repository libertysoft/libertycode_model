<?php
/**
 * Description :
 * This class allows to define default valid entity class.
 * Valid entity can validate its attributes, in differed time.
 * It can be used validation in real time too, if coupled with @see DefaultValidEntity::beanCheckValidValue() .
 * Can be consider is base of all entity types, where attributes validation required.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\model;

use liberty_code\model\entity\model\DefaultItemEntity;
use liberty_code\model\entity\api\ValidEntityInterface;

use Exception;
use liberty_code\model\entity\library\ToolBoxValidEntity;
use liberty_code\model\entity\exception\ValidationFailException;



abstract class DefaultValidEntity extends DefaultItemEntity implements ValidEntityInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified attribute has valid specified value.
     * Associative array of errors can be provided.
     * Overwrite it to implement specific check.
     *
     * Associative array of errors format:
     * @see ValidEntityInterface::checkAttributeValid() for specified attribute.
     *
     * @param string $strKey
     * @param mixed $value
     * @param array &$tabError = array()
     * @return boolean
     */
    protected function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Check by attribute
        switch($strKey)
        {
            default:
                $result = true;
                $tabError = array();
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Set validation for specified attribute.
     * Overwrite it to implement specific validation.
     *
     * @param string $strKey
     * @param mixed $value
     * @return boolean
     * @throws ValidationFailException
     */
    protected function setAttributeValueValid($strKey, $value)
    {
        // Check validation
        $tabError = array();
        $result = $this->checkAttributeValueValid($strKey, $value, $tabError);

        // Throw exception if validation not pass
        if(!$result)
        {
            // Get error from array of errors
            $tabError = array_values($tabError);
            $resultError = (isset($tabError[0]) ? $tabError[0] : null);

            // Throw custom exception
            if($resultError instanceof Exception)
            {
                throw $resultError;
            }
            // Throw standard exception
            else
            {
                throw new ValidationFailException($resultError);
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkAttributeValid($key = null, array &$tabError = array())
    {
        // Init var
        $key = (
            is_string($key) ?
                $key :
                (
                    is_array($key) ?
                        array_filter($key, function($strKey) {return is_string($strKey);}) :
                        null
                )
        );
        $tabError = array();
        $result = (
            (is_null($key) || is_array($key)) ?
                // Case specified index array of attributes or whole attributes validation required
                ToolBoxValidEntity::checkEntityIsValid(
                    $this,
                    function($strKey, array &$tabError){return $this->checkAttributeValid($strKey, $tabError);},
                    $tabError,
                    (is_array($key) ? $key : null)
                ) :
                // Else: case specified attribute validation required
                $this->checkAttributeValueValid(
                    $key,
                    $this->getAttributeValue($key, false),
                    $tabError
                )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ValidationFailException
     */
    public function setAttributeValid($key = null)
    {
        // Init var
        $result = true;
        $key = (
            is_string($key) ?
                $key :
                (
                    is_array($key) ?
                        array_filter($key, function($strKey) {return is_string($strKey);}) :
                        null
                )
        );

        // Case specified index array of attributes or whole attributes validation required
        if(is_null($key) || is_array($key))
        {
            // Run each attribute
            $tabKey = (is_array($key) ? $key : $this->getTabAttributeKey());
            foreach($tabKey as $strKey)
            {
                $result = $this->setAttributeValid($strKey) && $result;
            }
        }
        // Else: case specified attribute validation required
        else
        {
            // Set check argument
            parent::beanGetData($key);
            $value = $this->getAttributeValue($key, false);
            $result = $this->setAttributeValueValid($key, $value);
        }

        // Return result
        return $result;
    }



}