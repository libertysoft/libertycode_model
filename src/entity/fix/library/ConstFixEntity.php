<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\fix\library;



class ConstFixEntity
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
    const EXCEPT_MSG_COLLECTION_ITEM_CLASS_PATH_INVALID_FORMAT =
        'Item class path invalid! The class path "%1$s" must be null or a valid string class path.';
}