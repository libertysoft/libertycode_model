<?php
/**
 * Description :
 * This class allows to define fixed entity collection class.
 * Fixed entity collection allows to specify class path of items,
 * can be updated only on this class.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\fix\model;

use liberty_code\model\entity\model\DefaultValidEntityCollection;

use liberty_code\model\entity\fix\exception\CollectionItemClassPathInvalidFormatException;



abstract class FixEntityCollection extends DefaultValidEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabData = array())
    {
        // Init var
        $strItemClassPath = $this->getStrFixItemClassPath();

        // Call parent constructor
        parent::__construct($strItemClassPath, $tabData);
    }



	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get fixed class path of items.
     *
     * @return null|string
     */
    abstract protected function getStrFixItemClassPath();




	
	// Methods setters
	// ******************************************************************************

    /**
     * @inheritdoc
     * @throws CollectionItemClassPathInvalidFormatException
     */
    public function setItemClassPath($strClassPath)
    {
        // Init var
        $strItemClassPath = $this->getStrFixItemClassPath();

        // Check arguments
        CollectionItemClassPathInvalidFormatException::setCheck($strClassPath, $strItemClassPath);

        // Call parent method
        parent::setItemClassPath($strClassPath);
    }



}