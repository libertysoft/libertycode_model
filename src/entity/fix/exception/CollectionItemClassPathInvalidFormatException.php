<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\fix\exception;

use liberty_code\model\entity\fix\library\ConstFixEntity;



class CollectionItemClassPathInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $classPath
     */
	public function __construct($classPath)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstFixEntity::EXCEPT_MSG_COLLECTION_ITEM_CLASS_PATH_INVALID_FORMAT, strval($classPath));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

	/**
	 * Check if specified class path option has valid format.
	 * 
     * @param mixed $classPath
     * @param string $strFixClassPath = null
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($classPath, $strFixClassPath = null)
    {
		// Init var
		$result =
            (is_null($strFixClassPath) || is_string($strFixClassPath)) &&
            ($classPath === $strFixClassPath);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($classPath);
		}
		
		// Return result
		return $result;
    }
	
	
	
}