<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\exception;

use liberty_code\model\entity\library\ConstEntity;



class ConfigInvalidFormatException extends \Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstEntity::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified attribute config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    public static function checkAttributeConfigIsValid($config)
    {
        // Init var
        $result =
            // Case string: check key valid
            (is_string($config) && (trim($config) != '')) ||

            // Case array
            (
                is_array($config) &&

                // Check key valid
                isset($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY]) &&
                is_string($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY]) &&
                (trim($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY]) != '') &&

                // Check name valid, if required
                (
                    (!isset($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME])) ||
                    (
                        is_string($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME]) &&
                        (trim($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME]) != '')
                    )
                ) &&

                // Check alias valid, if required
                (
                    (!isset($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS])) ||
                    (
                        is_string($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS]) &&
                        (trim($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS]) != '')
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param array $config
     * @return boolean
     */
    public static function checkConfigIsValid(array $config)
    {
        // Init var
        $result = true;
        $tabKey = array();
        $tabNm = array();

        // Run all attribute configurations
        $tabAttrConfig = array_values($config);
        for($intCpt = 0; ($intCpt < count($tabAttrConfig)) && $result; $intCpt++)
        {
            // Get info
            $attrConfig = $tabAttrConfig[$intCpt];

            // Check attribute config valid
            if(static::checkAttributeConfigIsValid($attrConfig))
            {
                // Get info
                $strKey = (
                    is_string($attrConfig) ?
                        $attrConfig :
                        $attrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY]
                );
                $strNm = (
                    isset($attrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME]) ?
                        $attrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME] :
                        $strKey
                );

                // Check attribute
                $result =
                    (!in_array($strKey, $tabKey)) && // Check attribute key unique
                    (!in_array($strNm, $tabNm)); // Check attribute name unique

                // Register unique info
                $tabKey[] = $strKey;
                $tabNm[] = $strNm;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);


        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }



}