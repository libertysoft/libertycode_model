<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\exception;

use liberty_code\model\entity\library\ConstEntity;



class CollectionGetConfigInvalidFormatException extends \Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstEntity::EXCEPT_MSG_COLLECTION_GET_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $tabConfigOpe = ConstEntity::getTabCollectionGetConfigOperation();
        $result =
            // Check valid array
            is_array($config) &&

            // Check attribute key valid
            array_key_exists(ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY, $config) &&
            is_string($config[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY]) &&
            (trim($config[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY]) != '') &&

            // Check operation not valid
            (
                (!array_key_exists(ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION_NOT, $config)) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION_NOT]) ||
                    is_int($config[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION_NOT]) ||
                    (
                        is_string($config[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION_NOT]) &&
                        ctype_digit($config[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION_NOT])
                    )
                )
            ) &&

            // Check operation valid
            array_key_exists(ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION, $config) &&
            in_array($config[ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION], $tabConfigOpe) &&

            // Check value valid
            array_key_exists(ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE, $config);

        // Return result
        return $result;
    }



    /**
     * Check if specified config array has valid format.
     *
     * @param array $tabConfig
     * @return boolean
     */
    protected static function checkTabConfigIsValid(array $tabConfig)
    {
        // Init var
        $result = true;

        // Run all criteria configurations
        for($intCpt = 0; ($intCpt < count($tabConfig)) && $result; $intCpt++)
        {
            // Get info
            $config = $tabConfig[$intCpt];

            // Check criteria valid
            $result = static::checkConfigIsValid($config);
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkTabConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }



}