<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\exception;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\api\ItemEntityInterface;
use liberty_code\model\entity\exception\CollectionItemClassPathInvalidFormatException;



class CollectionValueInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $value
     */
	public function __construct($value) 
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstEntity::EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT, strval($value));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified value has valid format
	 *
     * @param mixed $value
     * @param string $strClassPath = null
	 * @return boolean
     * @throws CollectionItemClassPathInvalidFormatException
	 * @throws static
     */
	static public function setCheck($value, $strClassPath = null)
    {
        // Set check arguments
        CollectionItemClassPathInvalidFormatException::setCheck($strClassPath);

		// Init var
		$result =
            (!is_null($value)) && ($value instanceof ItemEntityInterface) && // Check value is valid
            (
                // Check specified class path, not required
                is_null($strClassPath) ||

                // Check specified class path, required
                (
                    ($value instanceof $strClassPath) // Check value has valid type
                )
            );
            // && (is_a($value, $strClassPath) || is_subclass_of($value, $strClassPath));

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($value);
		}
		
		// Return result
		return $result;
    }
	
	
	
}