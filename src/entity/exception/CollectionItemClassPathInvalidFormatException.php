<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\exception;

use liberty_code\model\entity\library\ConstEntity;



class CollectionItemClassPathInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $classPath
     */
	public function __construct($classPath)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstEntity::EXCEPT_MSG_COLLECTION_ITEM_CLASS_PATH_INVALID_FORMAT, strval($classPath));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

	/**
	 * Check if specified class path option has valid format.
	 * 
     * @param mixed $classPath
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($classPath)
    {
		// Init var
		$result =
            is_null($classPath) ||
            (is_string($classPath) && class_exists($classPath));
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($classPath);
		}
		
		// Return result
		return $result;
    }
	
	
	
}