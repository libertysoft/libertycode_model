<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\library;



class ConstEntity
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Configuration
    const TAB_CONFIG_KEY_ATTRIBUTE_KEY = 'key';
    const TAB_CONFIG_KEY_ATTRIBUTE_NAME = 'name';
    const TAB_CONFIG_KEY_ATTRIBUTE_ALIAS = 'alias';
    const TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE = 'default_value';

    const CONF_DEFAULT_ENTITY_KEY_PATTERN_DECORATION = 'entity_%1$s';

    // Collection configuration get
    const TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY = 'attribute_key';
    const TAB_COLLECTION_GET_CONFIG_KEY_OPERATION_NOT = 'operation_not';
    const TAB_COLLECTION_GET_CONFIG_KEY_OPERATION = 'operation';
    const TAB_COLLECTION_GET_CONFIG_KEY_VALUE = 'value';

    // Collection configuration get operation
    const COLLECTION_GET_CONFIG_OPERATION_EQUAL = '=';
    const COLLECTION_GET_CONFIG_OPERATION_STRICT_EQUAL = '==';
    const COLLECTION_GET_CONFIG_OPERATION_GREATER = '>';
    const COLLECTION_GET_CONFIG_OPERATION_GREATER_EQUAL = '>=';
    const COLLECTION_GET_CONFIG_OPERATION_LESS = '<';
    const COLLECTION_GET_CONFIG_OPERATION_LESS_EQUAL = '<=';
    const COLLECTION_GET_CONFIG_OPERATION_START = 'start';
    const COLLECTION_GET_CONFIG_OPERATION_END = 'end';
    const COLLECTION_GET_CONFIG_OPERATION_CONTAIN = 'contain';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following config "%1$s" invalid! The config must be an array, not empty and following the entity configuration standard.';
    const EXCEPT_MSG_VALIDATION_FAIL = 'Validation failed!%1$s';
    const EXCEPT_MSG_COLLECTION_ITEM_CLASS_PATH_INVALID_FORMAT = 'Item class path invalid! The class path "%1$s" must be null or a valid string class path.';
    const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
    const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a valid object in collection.';
    const EXCEPT_MSG_COLLECTION_GET_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the default entity collection get configuration standard.';





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods statics getters
    // ******************************************************************************

    /**
     * Get collection configuration get operations array.
     *
     * @return array
     */
    static public function getTabCollectionGetConfigOperation()
    {
        // Init var
        $result = array(
            self::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
            self::COLLECTION_GET_CONFIG_OPERATION_STRICT_EQUAL,
            self::COLLECTION_GET_CONFIG_OPERATION_GREATER,
            self::COLLECTION_GET_CONFIG_OPERATION_GREATER_EQUAL,
            self::COLLECTION_GET_CONFIG_OPERATION_LESS,
            self::COLLECTION_GET_CONFIG_OPERATION_LESS_EQUAL,
            self::COLLECTION_GET_CONFIG_OPERATION_START,
            self::COLLECTION_GET_CONFIG_OPERATION_END,
            self::COLLECTION_GET_CONFIG_OPERATION_CONTAIN
        );

        // Return result
        return $result;
    }



}