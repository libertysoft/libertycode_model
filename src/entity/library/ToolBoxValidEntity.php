<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\validation\rule\exception\ValidationFailException;
use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\api\ValidEntityInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\api\ValidEntityCollectionInterface;



class ToolBoxValidEntity extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified entity is valid,
     * from specified callable.
     * Associative array of errors can be provided.
     *
     * Check attribute is valid callable format:
     * boolean function(string $strKey, array &$tabError)
     *
     * Associative array of errors format:
     * @see ValidEntityInterface::checkAttributeValid() for all attributes.
     *
     * Attribute key format:
     * Can be string attribute key or index array of attribute keys.
     *
     * @param EntityInterface $objEntity
     * @param callable $callCheckAttributeIsValid
     * @param array &$tabError
     * @param string|string[] $attrKey = null
     * @return boolean
     */
    public static function checkEntityIsValid(
        EntityInterface $objEntity,
        $callCheckAttributeIsValid,
        array &$tabError,
        $attrKey = null
    )
    {
        // Init var
        $result = true;
        $tabError = array();

        // Run each attribute, if required
        if(is_callable($callCheckAttributeIsValid))
        {
            $tabKey = (
                is_array($attrKey) ?
                    $attrKey :
                    (
                        is_string($attrKey) ?
                            array($attrKey) :
                            $objEntity->getTabAttributeKey()
                    )
            );
            foreach($tabKey as $strKey)
            {
                if($objEntity->checkAttributeExists($strKey))
                {
                    // Get info
                    $tabAttrError = array();
                    $boolValid = $callCheckAttributeIsValid($strKey, $tabAttrError);

                    // Get error
                    if(!$boolValid)
                    {
                        $tabError[$strKey] = $tabAttrError;
                    }

                    // Register result
                    $result = $result && $boolValid;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified entity collection is valid,
     * from specified callable.
     * Associative array of errors can be provided.
     *
     * Check entity is valid callable format:
     * boolean function(EntityInterface $objEntity, array &$tabError)
     *
     * Associative array of errors format:
     * @see ValidEntityCollectionInterface::checkValid().
     *
     * Configuration array format:
     * @see EntityCollectionInterface::getTabKey() configuration format.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param callable $callCheckEntityIsValid
     * @param array &$tabError
     * @param array $tabConfig = null
     * @return boolean
     */
    public static function checkEntityCollectionIsValid(
        EntityCollectionInterface $objEntityCollection,
        $callCheckEntityIsValid,
        array &$tabError,
        array $tabConfig = null
    )
    {
        // Init var
        $result = true;
        $tabError = array();

        // Run each entity
        if(is_callable($callCheckEntityIsValid))
        {
            $tabKey = $objEntityCollection->getTabKey($tabConfig);
            foreach($tabKey as $strKey)
            {
                // Get info
                $objEntity = $objEntityCollection->getItem($strKey);
                $tabEntityError = array();
                $boolValid = $callCheckEntityIsValid($objEntity, $tabEntityError);

                // Get error
                if(!$boolValid)
                {
                    $tabError[$strKey] = $tabEntityError;
                }

                // Register result
                $result = $result && $boolValid;
            }
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Merge specified 2 associative arrays of attribute errors:
     * Add specified associative array of attribute errors 2, if required,
     * on specified associative array of attribute errors 1.
     *
     * Associative array of errors format:
     * @see ValidEntityInterface::checkAttributeValid() for specified attribute.
     *
     * @param array $tabError1
     * @param array $tabError2
     * @return array
     */
    public static function getTabMergeAttributeError(array $tabError1, array $tabError2)
    {
        // Init var
        $result = $tabError1;

        // Run each error from array 2
        foreach($tabError2 as $key => $error)
        {
            // Add error, if required
            if(
                (!is_null($error)) &&
                (
                    (!array_key_exists($key, $result)) ||
                    is_null($result[$key]) ||
                    (
                        is_object($error) &&
                        ($error instanceof ValidationFailException) &&
                        (count($error->getTabInfo()) > 0)
                    )
                )
            )
            {
                $result[$key] = $error;
            }
        }

        // Return result
        return $result;
    }



}