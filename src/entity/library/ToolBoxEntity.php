<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;



class ToolBoxEntity extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified entity collection,
     * from specified hydration callable,
     * with previous entities safe.
     * Return hydration callable result.
     *
     * Hydration callable format:
     * mixed function(EntityCollectionInterface $objEntityCollection)
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param callable $callHydrate
     * @return mixed
     */
    public static function hydrateEntityCollectionOldSafe(
        EntityCollectionInterface $objEntityCollection,
        callable $callHydrate
    )
    {
        // Init var
        $tabEntityOld = $objEntityCollection->getTabItem();

        // Hydrate entity collection
        $objEntityCollection->removeItemAll();
        $result = $callHydrate($objEntityCollection);

        // Get new entities
        $tabEntityNew = $objEntityCollection->getTabItem();

        // Set entities
        $objEntityCollection->removeItemAll();
        $objEntityCollection->setTabItem($tabEntityOld);
        $objEntityCollection->setTabItem($tabEntityNew);

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set specified value,
     * on specified attribute(s),
     * from specified entity.
     *
     * Attribute key format:
     * Can be string attribute key or index array of attribute keys.
     *
     * Check set callable format:
     * boolean function(EntityInterface $objEntity, string $strAttrKey)
     *
     * Format format:
     * boolean|callable: boolean function(EntityInterface $objEntity, string $strAttrKey)
     *
     * @param EntityInterface $objEntity
     * @param string|array $attrKey
     * @param mixed $value = null
     * @param callable $callCheckSet = null
     * @param boolean|callable $format = true
     */
    public static function setEntityAttrValue(
        EntityInterface $objEntity,
        $attrKey,
        $value = null,
        $callCheckSet = null,
        $format = true
    )
    {
        // Init var
        $tabAttrKey = (is_array($attrKey) ? $attrKey : array($attrKey));
        $callCheckSet = (is_callable($callCheckSet) ? $callCheckSet : null);

        // Run each attribute keys
        foreach($tabAttrKey as $strAttrKey)
        {
            // Update attribute, if required
            if(
                $objEntity->checkAttributeExists($strAttrKey) &&
                (
                    (!is_callable($callCheckSet)) ||
                    $callCheckSet($objEntity, $strAttrKey)
                )
            )
            {
                $boolFormat = (
                    is_bool($format) ?
                        $format :
                        (
                            is_callable($format) ?
                                $format($objEntity, $strAttrKey) :
                                true
                        )

                );
                $objEntity->setAttributeValue($strAttrKey, $value, $boolFormat);
            }
        }
    }



    /**
     * Set specified value,
     * on specified attribute(s),
     * from specified entity collection.
     *
     * Attribute key format:
     * @see setEntityAttrValue() attribute key format.
     *
     * Check set callable format:
     * @see setEntityAttrValue() check set callable format.
     *
     * Format format:
     * @see setEntityAttrValue() format format.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param string|array $attrKey
     * @param mixed $value = null
     * @param callable $callCheckSet = null
     * @param boolean|callable $format = true
     */
    public static function setEntityCollectionAttrValue(
        EntityCollectionInterface $objEntityCollection,
        $attrKey,
        $value = null,
        $callCheckSet = null,
        $format = true
    )
    {
        // Run each entity
        $tabKey = $objEntityCollection->getTabKey();
        foreach($tabKey as $strKey)
        {
            // Get info
            $objEntity = $objEntityCollection->getItem($strKey);

            // Set attribute value on entity
            static::setEntityAttrValue(
                $objEntity,
                $attrKey,
                $value,
                $callCheckSet,
                $format
            );
        }
    }



}