<?php
/**
 * Description :
 * This class allows to describe behavior of entity factory class.
 * Entity factory allows to provide new specific entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\factory\api;

use liberty_code\model\entity\api\EntityInterface;



interface EntityFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get new entity instance.
     *
     * Attribute values array format:
     * @see EntityInterface::hydrate() attribute values array format.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure entity getting execution.
     * Null means no specific execution configuration required.
     *
     * @param array $tabValue = array()
     * @param array $tabConfig = null
     * @return EntityInterface
     */
    public function getObjEntity(array $tabValue = array(), array $tabConfig = null);
}