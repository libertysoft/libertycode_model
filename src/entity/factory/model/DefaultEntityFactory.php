<?php
/**
 * Description :
 * This class allows to define default entity factory class.
 * Default entity factory allows to provide new specific entities,
 * from specified DI provider and specified configuration.
 *
 * Default entity factory allows to specify parts of configuration (entity type):
 * [
 *     entity_class_path(required): "string entity class path",
 *
 *     hydrate_attribute_exist_only_require(optional: got true, if not found): true / false,
 *
 *     set_new_require(optional: got true, if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\model\entity\factory\api\EntityFactoryInterface;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\entity\factory\exception\ConfigInvalidFormatException;
use liberty_code\model\entity\factory\exception\ExecConfigInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 */
class DefaultEntityFactory extends DefaultFactory implements EntityFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objProvider
     * @param array $tabConfig
     */
    public function __construct(ProviderInterface $objProvider, array $tabConfig)
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init configuration
        $this->setTabConfig($tabConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        if(!$this->beanExists(ConstEntityFactory::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstEntityFactory::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstEntityFactory::DATA_KEY_DEFAULT_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstEntityFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check hydrate attribute exist only required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntity() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    public function checkHydrateAttrExistOnlyRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!isset($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_HYDRATE_ATTRIBUTE_EXIST_ONLY_REQUIRE])) ||
            (intval($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_HYDRATE_ATTRIBUTE_EXIST_ONLY_REQUIRE]) != 0) ||
            (
                isset($tabExecConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_HYDRATE_ATTRIBUTE_EXIST_ONLY_REQUIRE]) &&
                (intval($tabExecConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_HYDRATE_ATTRIBUTE_EXIST_ONLY_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check set new required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntity() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    public function checkSetNewRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabExecConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SET_NEW_REQUIRE]) ?
                (intval($tabExecConfig[ConstEntityFactory::TAB_EXEC_CONFIG_KEY_SET_NEW_REQUIRE]) != 0) :
                (
                    isset($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SET_NEW_REQUIRE]) ?
                        (intval($tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_SET_NEW_REQUIRE]) != 0) :
                        true
                )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get class path of entity.
     *
     * @return string
     */
    public function getStrEntityClassPath()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH];

        // Return result
        return $result;
    }



    /**
     * Get new object instance entity.
     * Overwrite it to set specific feature.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see getObjEntity() configuration array format.
     *
     * @param array $tabConfig = null
     * @return EntityInterface
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $strClassPath = $this->getStrEntityClassPath();
        /** @var null|EntityInterface $result */
        $result = $this->getObjInstance($strClassPath);

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     hydrate_attribute_exist_only_require(optional: got configuration hydrate_attribute_exist_only_require, if not found): true / false,
     *
     *     set_new_require(optional: got configuration set_new_require, if not found): true / false
     * ]
     *
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     * @throws ExecConfigInvalidFormatException
     */
    public function getObjEntity(array $tabValue = array(), array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $strClassPath = $this->getStrEntityClassPath();
        /** @var null|EntityInterface $result */
        $result = $this->getObjEntityNew($tabConfig);

        // Check entity valid
        if(
            is_null($result) ||
            (!($result instanceof $strClassPath))
        )
        {
            throw new ConfigInvalidFormatException($this->getTabConfig());
        }

        // Hydrate entity
        $boolCheckExist = $this->checkHydrateAttrExistOnlyRequired($tabConfig);
        $result->hydrate($tabValue, $boolCheckExist);

        if($result instanceof SaveEntityInterface)
        {
            $boolIsNew = $this->checkSetNewRequired($tabConfig);
            $result->setIsNew($boolIsNew);
        }

        // Return result
        return $result;
    }



}