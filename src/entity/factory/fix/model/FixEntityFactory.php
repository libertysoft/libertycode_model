<?php
/**
 * Description :
 * This class allows to define fix entity factory class.
 * Fix entity factory allows to provide new specific entities,
 * from a specified fixed configuration.
 *
 * Fix entity factory allows to specify parts of configuration:
 * [
 *     Default entity factory configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\factory\fix\model;

use liberty_code\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\entity\factory\fix\exception\ConfigInvalidFormatException;



abstract class FixEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(ProviderInterface $objProvider)
    {
        // Init var
        $tabConfig = $this->getTabFixConfig();

        // Call parent constructor
        parent::__construct($objProvider, $tabConfig);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstEntityFactory::DATA_KEY_DEFAULT_CONFIG:
                    $tabConfig = $this->getTabFixConfig();
                    ConfigInvalidFormatException::setCheck($value, $tabConfig);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     *
     * @return array
     */
    abstract protected function getTabFixConfig();



}