<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\factory\library;



class ConstEntityFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_ENTITY_CLASS_PATH = 'entity_class_path';
    const TAB_CONFIG_KEY_HYDRATE_ATTRIBUTE_EXIST_ONLY_REQUIRE = 'hydrate_attribute_exist_only_require';
    const TAB_CONFIG_KEY_SET_NEW_REQUIRE = 'set_new_require';

    // Execution configuration
    const TAB_EXEC_CONFIG_KEY_HYDRATE_ATTRIBUTE_EXIST_ONLY_REQUIRE = 'hydrate_attribute_exist_only_require';
    const TAB_EXEC_CONFIG_KEY_SET_NEW_REQUIRE = 'set_new_require';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default entity factory configuration standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default entity factory execution configuration standard.';



}