<?php
/**
 * Description :
 * This class allows to describe behavior of entity class.
 * Entity is item, containing attributes and allows to manage its data.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\api;



interface EntityInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified attribute values.
     * Return true if all attribute values success,
     * false if an error occurs on at least one attribute value.
     *
     * Attribute values array format:
     * {
     * 		'Attribute key 1' => mixed value 1,
     * 		...,
     * 		'Attribute key N' => mixed value N,
     * }
     *
     * @param array $tabValue = array()
     * @param boolean $boolCheckExist = true
     * @return boolean
     */
    public function hydrate(array $tabValue = array(), $boolCheckExist = true);





	// Methods check
	// ******************************************************************************

    /**
     * Check if specified attribute exists.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkAttributeExists($strKey);




	
	// Methods getters
	// ******************************************************************************

    /**
     * Get index array of attribute keys.
     *
     * @return array
     */
    public function getTabAttributeKey();



    /**
     * Get specified attribute name,
     * used on rendering.
     *
     * @param string $strKey
     * @return string
     */
    public function getAttributeName($strKey);



    /**
     * Get specified attribute alias,
     * used on other components.
     *
     * @param string $strKey
     * @return null|string
     */
    public function getAttributeAlias($strKey);



    /**
     * Get specified attribute default value.
     *
     * @param string $strKey
     * @return mixed
     */
    public function getAttributeDefaultValue($strKey);



    /**
     * Get specified attribute value.
     *
     * @param string $strKey
     * @param boolean $boolFormat = true
     * @return mixed
     */
    public function getAttributeValue($strKey, $boolFormat = true);



    /**
     * Get array of attribute data (Attribute key => attribute value).
     *
     * @param boolean $boolFormat = true
     * @return array
     */
    public function getTabData($boolFormat = true);


	
	
	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set specified attribute value.
	 * 
	 * @param string $strKey
     * @param boolean $boolFormat = true
     * @param mixed $value
	 */
	public function setAttributeValue($strKey, $value, $boolFormat = true);



    /**
     * Reset specified attribute value with default value.
     * If no attribute specified, reset all attributes.
     *
     * @param string $strKey = null
     */
    public function resetAttribute($strKey = null);
}