<?php
/**
 * Description :
 * This class allows to describe behavior of valid entity class.
 * Valid entity is entity, can validate its attributes, in differed time.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\api;

use Exception;
use liberty_code\model\entity\api\EntityInterface;



interface ValidEntityInterface extends EntityInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods check
	// ******************************************************************************

    /**
     * Check if specified attribute or index array of attributes has valid value(s).
     * If no attribute specified, validation check for all attributes.
     * Associative array of errors can be provided.
     *
     * Associative array of errors format, for specified attribute:
     * [
     *     error key 1 => error 1 (null|string|Exception),
     *     ...,
     *     error key N => error N
     * ]
     *
     * Associative array of errors format, for index array attributes|all attributes:
     * [
     *     attribute key 1 => Associative array of errors for attribute 1 (see format above),
     *     ...,
     *     attribute key N => Associative array of errors for attribute N
     * ]
     *
     * @param string|string[] $key = null
     * @param array &$tabError = array()
     * @return boolean
     */
    public function checkAttributeValid($key = null, array &$tabError = array());



    /**
     * Set validation for specified attribute or index array of attributes.
     * If no attribute specified, set validation for all attributes.
     *
     * @param string|string[] $key = null
     * @return boolean
     * @throws Exception
     */
    public function setAttributeValid($key = null);
}