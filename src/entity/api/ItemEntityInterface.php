<?php
/**
 * Description :
 * This class allows to describe behavior of item entity class.
 * Item entity is entity can be stored in collection.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\api;

use liberty_code\model\entity\api\EntityInterface;



interface ItemEntityInterface extends EntityInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get string key (considered as id in collection).
     *
     * @return string
     */
    public function getStrKey();
}