<?php
/**
 * Description :
 * This class allows to describe behavior of valid entities collection class.
 * Valid collection is collection, can validate its entities, in differed time.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\api;

use liberty_code\model\entity\api\EntityCollectionInterface;

use Exception;
use liberty_code\model\entity\api\ValidEntityInterface;



interface ValidEntityCollectionInterface extends EntityCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * Check if collection is valid (validation for entity items).
     * Associative array of errors can be provided.
     *
     * Configuration array format:
     * @see EntityCollectionInterface::getTabKey() configuration format.
     *
     * Attribute key format:
     * Can be string attribute key or index array of attribute keys.
     *
     * Associative array of errors format:
     * [
     *     entity item key 1 => error 1 (@see ValidEntityInterface::checkAttributeValid() for all attributes),
     *     ...,
     *     entity item key N => error N
     * ]
     *
     * @param array $tabConfig = null
     * @param string|string[] $attrKey = null
     * @param array &$tabError = array()
     * @return boolean
     */
    public function checkValid(
        array $tabConfig = null,
        $attrKey = null,
        array &$tabError = array()
    );



    /**
     * Set validation for collection (entity items).
     *
     * Configuration array format:
     * @see EntityCollectionInterface::getTabKey() configuration format.
     *
     * Attribute key format:
     * Can be string attribute key or index array of attribute keys.
     *
     * @param array $tabConfig = null
     * @param string|string[] $attrKey = null
     * @return boolean
     * @throws Exception
     */
    public function setValid(
        array $tabConfig = null,
        $attrKey = null
    );
}