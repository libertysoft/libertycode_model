<?php
/**
 * Description :
 * This class allows to describe behavior of entities collection class.
 * Collection allows to store entity objects with same type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\api;

use liberty_code\model\entity\api\ItemEntityInterface;



interface EntityCollectionInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods value
	// ******************************************************************************

	/**
	 * Check if specified entity item key is found.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkExists($strKey);
	
	
	
	
	
	// Methods getters
	// ******************************************************************************

    /**
     * Get class path of items.
     *
     * @return null|string
     */
    public function getStrItemClassPath();



    /**
     * Get index array of keys.
     *
     * Configuration format:
     * Selection configuration can be provided.
     * Null means no specific selection required and all keys will be selected.
     *
     * @param array $tabConfig = null
     * @return array
     */
    public function getTabKey(array $tabConfig = null);



    /**
     * Get associative array of items.
     * Format: key => value: item.
     *
     * Configuration format: @see EntityCollectionInterface::getTabKey()
     *
     * @param array $tabConfig = null
     * @return array
     */
    public function getTabItem(array $tabConfig = null);


	
	/**
	 * Get item from specified key.
	 * 
	 * @param string $strKey
	 * @return null|ItemEntityInterface
	 */
	public function getItem($strKey);

	
	
	
	
	// Methods setters
	// ******************************************************************************

    /**
     * Set class path of items.
     *
     * @param null|string $strClassPath
     */
    public function setItemClassPath($strClassPath);



	/**
	 * Set item and return its key.
	 * 
	 * @param ItemEntityInterface $objItem
	 * @return string
     */
	public function setItem(ItemEntityInterface $objItem);



    /**
     * Set list of items (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|EntityCollectionInterface $tabItem
     * @return array
     */
    public function setTabItem($tabItem);



    /**
     * Remove item and return its instance.
     *
     * @param string $strKey
     * @return ItemEntityInterface
     */
    public function removeItem($strKey);



    /**
     * Remove all items.
     *
     * Configuration format: @see EntityCollectionInterface::getTabKey()
     *
     * @param array $tabConfig = null
     */
    public function removeItemAll(array $tabConfig = null);
}