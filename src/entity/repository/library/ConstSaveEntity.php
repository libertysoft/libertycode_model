<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\repository\library;



class ConstSaveEntity
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

	// Configuration
    const TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE = 'name_save';
    const TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE = 'savable';
    const TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET = 'savable_get';
    const TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET = 'savable_set';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT = 'Following config "%1$s" invalid! The config must be an array, not empty and following the save entity configuration standard.';
    const EXCEPT_MSG_IS_NEW_INVALID_FORMAT = 'Following is new option "%1$s" invalid! The option must be a boolean.';
}