<?php
/**
 * Description :
 * This class allows to define default save configured entity class.
 * Can be consider is base of all save configured entity types.
 *
 * Save configured entity allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     "attribute key",
 *
 *     OR
 *
 *     [
 *         Validator configured entity configuration,
 *         name_save(optional: got key, if not found): "string attribute save name",
 *         savable(optional: got true, if not found):
 *             true / false
 *             OR
 *             Check attribute is savable callable format:
 *             boolean function(SaveConfigEntity $objEntity, string $strKey),
 *         savable_get(optional: got true, if not found):
 *             true / false
 *             OR
 *             Check attribute is savable, only for getting, callable format:
 *             boolean function(SaveConfigEntity $objEntity, string $strKey),
 *         savable_set(optional: got true, if not found):
 *             true / false
 *             OR
 *             Check attribute is savable, only for setting, callable format:
 *             boolean function(SaveConfigEntity $objEntity, string $strKey)
 *     ]
 *
 *     ...
 * ]
 *
 * Note:
 * -> Configuration save enabling (savable, savable_get, savable_set):
 *     - Configuration savable allows to enable both, save getting|setting, in same time.
 *     If set, other configuration about save enabling, are ignored.
 *     - Configuration savable_get allows to enable only save getting.
 *     - Configuration savable_set allows to enable only save setting.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\repository\model;

use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\model\entity\repository\api\SaveEntityInterface;

use liberty_code\library\bean\model\FixBean;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\entity\repository\exception\ConfigInvalidFormatException;
use liberty_code\model\entity\repository\exception\IsNewInvalidFormatException;



abstract class SaveConfigEntity extends ValidatorConfigEntity implements SaveEntityInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Specify if entity is new
     * @var boolean
     */
    protected $boolIsNew;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabValue = array(), ValidatorInterface $objValidator = null)
    {
        // Call parent constructor
        parent::__construct($tabValue, $objValidator);

        // Init is new
        $this->setIsNew(true);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateTabConfig()
    {
        // Set check configuration
        ConfigInvalidFormatException::setCheck($this->getTabConfig());

        // Hydrate attribute configuration array
        parent::hydrateTabConfig();
    }



    /**
     * @inheritdoc
     */
    public function hydrateSave(array $tabData = array(), $boolCheckExist = true)
    {
        // Init var
        $result = true;
        $boolCheckExist = (is_bool($boolCheckExist) ? $boolCheckExist : true);

        // Run each data value
        foreach($tabData as $strNm => $value)
        {
            $boolSet  = false;

            // Get key
            $strKey = $this->getAttributeKeyFromNameSave($strNm);

            // Set attribute value, if required
            if(
                ((!$boolCheckExist) || $this->checkAttributeExists($strKey)) &&
                $this->checkAttributeSaveSetEnabled($strKey)
            )
            {
                $this->setAttributeValueSave($strKey, $value);
                $boolSet  = true;
            }

            $result = $result && $boolSet;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsNew()
    {
        // Return result
        return $this->boolIsNew;
    }



    /**
     * Check if save getting enabled,
     * for specified attribute.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkAttributeSaveGetEnabled($strKey)
    {
        // Init var
        $result = $this->getAttributeConfigItem(
            $strKey,
            ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE,
            $this->getAttributeConfigItem(
                $strKey,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET,
                true
            )
        );
        $result = (is_callable($result) ? $result($this, $strKey) : $result);
        $result = (intval($result) != 0);

        // Return result
        return $result;
    }



    /**
     * Check if save setting enabled,
     * for specified attribute.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkAttributeSaveSetEnabled($strKey)
    {
        // Init var
        $result = $this->getAttributeConfigItem(
            $strKey,
            ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE,
            $this->getAttributeConfigItem(
                $strKey,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET,
                true
            )
        );
        $result = (is_callable($result) ? $result($this, $strKey) : $result);
        $result = (intval($result) != 0);

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabAttributeConfigKey($strKey = null)
    {
        // Init var
        $result = (
            is_null($strKey) ?
                array(
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE
                ) :
                parent::getTabAttributeConfigKey($strKey)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeNameSave($strKey)
    {
        // Return result
        return $this->getAttributeConfigItem(
            $strKey,
            ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE,
            $strKey
        );
    }



    /**
     * Get attribute key,
     * from specified attribute save name, if found.
     *
     * @param string $strNm
     * @return null|string
     */
    public function getAttributeKeyFromNameSave($strNm)
    {
        // Init var
        $result = null;

        if(is_string($strNm))
        {
            // Run each attribute config
            $tabAttributeKey = array();
            foreach($this->tabConfig as $strAttributeKey => $attributeConfig)
            {
                $tabAttributeKey[] = $strAttributeKey;

                // Try to found save name in attribute config
                if(
                    isset($attributeConfig[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE]) &&
                    ($attributeConfig[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE] == $strNm)
                )
                {
                    $result = $strAttributeKey;
                    break;
                }
            }

            // Try to found save name in attribute keys
            $result = (
                is_null($result) && in_array($strNm, $tabAttributeKey) ?
                    $strNm :
                    $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get specified attribute formatted value, to save.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified attribute saved formatted value.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueSave($strKey, $boolFormat = true)
    {
        // Init var
        $boolFormat = (is_bool($boolFormat) ? $boolFormat : true);
        $result = FixBean::beanGetData($strKey);
        $result = (
            $boolFormat ?
                $this->getAttributeValueSaveFormatGet($strKey, $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabDataSave()
    {
        // Init var
        $result = array();

        // Run each attribute
        foreach($this->getTabAttributeKey() as $strKey)
        {
            // Register data value, if required
            if($this->checkAttributeSaveGetEnabled($strKey))
            {
                // Get info
                $strNmSave = $this->getAttributeNameSave($strKey);
                $value = $this->getAttributeValueSave($strKey);

                // Register data value
                $result[$strNmSave] = $value;
            }
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws IsNewInvalidFormatException
     */
    public function setIsNew($boolIsNew)
    {
        // Check arguments
        IsNewInvalidFormatException::setCheck($boolIsNew);

        // Set property
        $this->boolIsNew = $boolIsNew;
    }



    /**
     * @inheritdoc
     */
    public function setAttributeValueSave($strKey, $value, $boolFormat = true)
    {
        // Init var
        $boolFormat = (is_bool($boolFormat) ? $boolFormat : true);
        $value = (
            $boolFormat ?
                $this->getAttributeValueSaveFormatSet($strKey, $value) :
                $value
        );

        // Set attribute value
        parent::beanSetData($strKey, $value);
    }



}