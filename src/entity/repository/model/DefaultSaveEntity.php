<?php
/**
 * Description :
 * This class allows to define default save entity class.
 * Can be consider is base of all save entity types.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\repository\model;

use liberty_code\model\entity\model\DefaultValidEntity;
use liberty_code\model\entity\repository\api\SaveEntityInterface;

use liberty_code\library\bean\model\FixBean;
use liberty_code\model\entity\repository\exception\IsNewInvalidFormatException;



abstract class DefaultSaveEntity extends DefaultValidEntity implements SaveEntityInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Specify if entity is new
     * @var boolean
     */
    protected $boolIsNew;



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabValue = array())
    {
        // Call parent constructor
        parent::__construct($tabValue);

        // Init is new
        $this->setIsNew(true);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function hydrateSave(array $tabData = array(), $boolCheckExist = true)
    {
        // Init var
        $result = true;
        $boolCheckExist = (is_bool($boolCheckExist) ? $boolCheckExist : true);

        // Run each data value
        foreach($tabData as $strNm => $value)
        {
            $boolSet  = false;

            // Get key
            $strKey = $this->getAttributeKeyFromNameSave($strNm);
            $strKey = (is_null($strKey) ? $strNm : $strKey);

            // Set attribute value, if required
            if(
                ((!$boolCheckExist) || $this->checkAttributeExists($strKey)) &&
                $this->checkAttributeSaveSetEnabled($strKey)
            )
            {
                $this->setAttributeValueSave($strKey, $value);
                $boolSet  = true;
            }

            $result = $result && $boolSet;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsNew()
    {
        // Return result
        return $this->boolIsNew;
    }



    /**
     * Check if save getting enabled,
     * for specified attribute.
     * Overwrite it to implement specific feature.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkAttributeSaveGetEnabled($strKey)
    {
        // Return result
        return true;
    }



    /**
     * Check if save setting enabled,
     * for specified attribute.
     * Overwrite it to implement specific feature.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkAttributeSaveSetEnabled($strKey)
    {
        // Return result
        return true;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * Overwrite it to implement specific feature.
     */
    public function getAttributeNameSave($strKey)
    {
        // Set check argument
        parent::beanGetData($strKey);

        // Return result
        return $strKey;
    }



    /**
     * Get attribute key,
     * from specified attribute save name, if found.
     * Overwrite it to implement specific feature.
     *
     * @param string $strNm
     * @return null|string
     */
    public function getAttributeKeyFromNameSave($strNm)
    {
        // Return result
        return (is_string($strNm) ? $strNm : null);
    }



    /**
     * Get specified attribute formatted value, to save.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified attribute saved formatted value.
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueSave($strKey, $boolFormat = true)
    {
        // Init var
        $boolFormat = (is_bool($boolFormat) ? $boolFormat : true);
        $result = FixBean::beanGetData($strKey);
        $result = (
            $boolFormat ?
                $this->getAttributeValueSaveFormatGet($strKey, $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabDataSave()
    {
        // Init var
        $result = array();

        // Run each attribute
        foreach($this->getTabAttributeKey() as $strKey)
        {
            // Register data value, if required
            if($this->checkAttributeSaveGetEnabled($strKey))
            {
                // Get info
                $strNmSave = $this->getAttributeNameSave($strKey);
                $value = $this->getAttributeValueSave($strKey);

                // Register data value
                $result[$strNmSave] = $value;
            }
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws IsNewInvalidFormatException
     */
    public function setIsNew($boolIsNew)
    {
        // Check arguments
        IsNewInvalidFormatException::setCheck($boolIsNew);

        // Set property
        $this->boolIsNew = $boolIsNew;
    }



    /**
     * @inheritdoc
     */
    public function setAttributeValueSave($strKey, $value, $boolFormat = true)
    {
        // Init var
        $boolFormat = (is_bool($boolFormat) ? $boolFormat : true);
        $value = (
            $boolFormat ?
                $this->getAttributeValueSaveFormatSet($strKey, $value) :
                $value
        );

        // Set attribute value
        parent::beanSetData($strKey, $value);
    }



}