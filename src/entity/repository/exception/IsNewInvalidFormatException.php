<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\repository\exception;

use Exception;

use liberty_code\model\entity\repository\library\ConstSaveEntity;



class IsNewInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $isNew
     */
	public function __construct($isNew)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstSaveEntity::EXCEPT_MSG_IS_NEW_INVALID_FORMAT, strval($isNew));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified is new option has valid format.
	 * 
     * @param mixed $isNew
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($isNew)
    {
		// Init var
		$result = is_bool($isNew);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($isNew);
		}
		
		// Return result
		return $result;
    }
	
	
	
}