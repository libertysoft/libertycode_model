<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\repository\exception;

use Exception;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\exception\ConfigInvalidFormatException as BaseConfigInvalidFormatException;
use liberty_code\model\entity\repository\library\ConstSaveEntity;



class ConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstSaveEntity::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified attribute config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkAttributeConfigIsValid($config)
    {
        // Init var
        $result =
            // Check name valid, if required
            (
                (!isset($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE])) ||
                (
                    is_string($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE]) &&
                    (trim($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE]) != '')
                )
            ) &&

            // Check savable valid, if required
            (
                (!isset($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE])) ||
                // Check is boolean or numeric (false == 0, true != 0)
                is_bool($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]) ||
                is_int($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]) ||
                (
                    is_string($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]) &&
                    ctype_digit($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE])
                ) ||
                // Check is boolean callable
                is_callable($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE])
            ) &&

            // Check savable get valid, if required
            (
                (!isset($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET])) ||
                // Check is boolean or numeric (false == 0, true != 0)
                is_bool($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET]) ||
                is_int($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET]) ||
                (
                    is_string($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET]) &&
                    ctype_digit($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET])
                ) ||
                // Check is boolean callable
                is_callable($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET])
            ) &&

            // Check savable set valid, if required
            (
                (!isset($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET])) ||
                // Check is boolean or numeric (false == 0, true != 0)
                is_bool($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET]) ||
                is_int($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET]) ||
                (
                    is_string($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET]) &&
                    ctype_digit($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET])
                ) ||
                // Check is boolean callable
                is_callable($config[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET])
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param array $config
     * @return boolean
     * @throws BaseConfigInvalidFormatException
     */
    protected static function checkConfigIsValid(array $config)
    {
        // Set check configuration
        BaseConfigInvalidFormatException::setCheck($config);

        // Init var
        $result = true;
        $tabKey = array();
        $tabNmSave = array();

        // Run all attribute configurations
        $tabAttrConfig = array_values($config);
        for($intCpt = 0; ($intCpt < count($tabAttrConfig)) && $result; $intCpt++)
        {
            // Get info
            $attrConfig = $tabAttrConfig[$intCpt];

            // Check attribute config valid
            if(static::checkAttributeConfigIsValid($attrConfig))
            {
                // Get info
                $strKey = (
                    is_string($attrConfig) ?
                        $attrConfig :
                        $attrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY]
                );
                $strNmSave = (
                    isset($attrConfig[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE]) ?
                        $attrConfig[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE] :
                        $strKey
                );

                // Check attribute
                $result =
                    (!in_array($strKey, $tabKey)) && // Check attribute key unique
                    (!in_array($strNmSave, $tabNmSave)); // Check attribute save name unique

                // Register unique info
                $tabKey[] = $strKey;
                $tabNmSave[] = $strNmSave;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws BaseConfigInvalidFormatException
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            // Check valid config
            static::checkConfigIsValid($config);


        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }



}