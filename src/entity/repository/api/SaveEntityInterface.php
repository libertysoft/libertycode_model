<?php
/**
 * Description :
 * This class allows to describe behavior of save entity class.
 * Save entity is entity can be used in repository, to be stored in safe support.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\entity\repository\api;

use liberty_code\model\entity\api\EntityInterface;



interface SaveEntityInterface extends EntityInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * Hydrate attribute values,
     * from specified saved data array.
     * Return true if all attribute values success,
     * false if an error occurs on at least one attribute value.
     *
     * Data array format:
     * [
     *     'Attribute 1 save name' => mixed value,
     * 	   ...,
     * 	   'Attribute N save name' => mixed value
     * ]
     *
     * @param array $tabData = array()
     * @param boolean $boolCheckExist = true
     * @return boolean
     */
    public function hydrateSave(array $tabData = array(), $boolCheckExist = true);





	// Methods check
	// ******************************************************************************

    /**
     * Check if entity is new.
     *
     * @return boolean
     */
    public function checkIsNew();




	
	// Methods getters
	// ******************************************************************************

    /**
     * Get specified attribute save name.
     *
     * @param string $strKey
     * @return string
     */
    public function getAttributeNameSave($strKey);



    /**
     * Get specified attribute value, to save.
     *
     * @param string $strKey
     * @param boolean $boolFormat = true
     * @return mixed
     */
    public function getAttributeValueSave($strKey, $boolFormat = true);



    /**
     * Get data array to save,
     * from attribute values.
     *
     * Return data array format:
     * @see hydrateSave() data array format.
     *
     * @return array
     */
    public function getTabDataSave();




	
	// Methods setters
	// ******************************************************************************
	
	/**
	 * Set if entity is new.
	 *
     * @param boolean $boolIsNew
	 */
	public function setIsNew($boolIsNew);



    /**
     * Set specified attribute saved value.
     *
     * @param string $strKey
     * @param boolean $boolFormat = true
     * @param mixed $value
     */
    public function setAttributeValueSave($strKey, $value, $boolFormat = true);



}