<?php
/**
 * Description :
 * This class allows to define multi collection repository class.
 * Multi collection repository allows to prepare array of data for complex save, in persistence.
 * Multi collection repository focus on entities that include in same group.
 *
 * Multi collection repository allows to specify parts of configuration:
 * [
 *     Sub-repo collection repository configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\multi\model;

use liberty_code\model\repository\sub_repository\model\SubRepoCollectionRepository;

use liberty_code\model\entity\library\ToolBoxEntity;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\exception\CollectionInvalidFormatException;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\sub_repository\library\ToolBoxSubRepo;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\model\repository\multi\model\MultiRepository;
use liberty_code\model\repository\multi\exception\IdInvalidFormatException;
use liberty_code\model\repository\multi\exception\CollectionRepositoryInvalidFormatException;
use liberty_code\model\repository\multi\exception\CollectionExecConfigInvalidFormatException;



/**
 * @method null|MultiRepository getObjRepository() @inheritdoc .
 */
class MultiCollectionRepository extends SubRepoCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY:
                    CollectionRepositoryInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     persistor_add_config(optional): [
     *         // Optional additional persistor configuration
     *         @see PersistorInterface::getTabData() configuration array format
     *     ],
     *
     *     sub_repository_execution_require(optional: got true if not found): true / false,
     *
     *     sub_repository_execution_config(optional: got all of current configuration, if not found): [
     *         @see loadSubRepo() configuration array format
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     persistor_info(optional): [
     *         @see PersistorInterface::getTabData() return information array format
     *     ],
     *
     *     sub_repository_info(optional): [
     *         @see loadSubRepo() return information array format (for all sub-elements)
     *     ]
     * ]
     *
     * @inheritdoc
     * @param mixed $tabId (=> $tabCriteria)
     * @throws CollectionInvalidFormatException
     * @throws IdInvalidFormatException
     */
    public function load(
        EntityCollectionInterface $objCollection,
        array $tabId = array(),
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);

        // Init var
        $result = false;
        $tabEntityId = array_map(
            function($id)
            {
                $entityId = ToolBoxSubRepo::getCriteria($id);
                IdInvalidFormatException::setCheck($entityId);
                return $entityId;
            },
            $tabId
        );
        $boolSubRepo = $this->checkSubRepoExecutionRequired($tabConfig);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $objPersistor = $this->getObjPersistor();

        if(!is_null($objPersistor))
        {
            // Get data from persistence
            $tabPersistorInfo = (is_null($tabInfo) ? null : array());
            $tabData = $objPersistor->getTabData(
                $tabEntityId,
                $this->getTabConfigPersistGet($objCollection, $tabConfig),
                $tabPersistorInfo
            );

            // Register info, if required
            if(
                (!is_null($tabPersistorInfo)) &&
                (count($tabPersistorInfo) > 0)
            )
            {
                $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO] = $tabPersistorInfo;
            }

            // Check data valid
            if(is_array($tabData))
            {
                $result = (
                    $boolSubRepo ?
                        // Case sub-element loading: hydrate entity collection, with previous entities safe
                        ToolBoxEntity::hydrateEntityCollectionOldSafe(
                            $objCollection,
                            function(EntityCollectionInterface $objCollection)
                            use($tabId, $tabData, $tabConfig, &$tabInfo)
                            {
                                $tabSubRepoConfig = (
                                    (
                                        (!is_null($tabConfig)) &&
                                        array_key_exists(
                                            ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG,
                                            $tabConfig
                                        )
                                    ) ?
                                        $tabConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG] :
                                        $tabConfig
                                );
                                $tabSubRepoInfo = (is_null($tabInfo) ? null : array());
                                $result =
                                    // Set entities with data
                                    ($this->setTabData($objCollection, $tabData, false) !== false) &&

                                    // Load entities sub-element
                                    $this->loadSubRepo($objCollection, $tabId, $tabSubRepoConfig, $tabSubRepoInfo);

                                // Register sub-repository info, if required
                                if(
                                    (!is_null($tabSubRepoInfo)) &&
                                    (count($tabSubRepoInfo) > 0)
                                )
                                {
                                    $tabInfo[ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO] = $tabSubRepoInfo;
                                }

                                return $result;
                            }
                        ) :
                        // Case else: set entities with data only
                        ($this->setTabData($objCollection, $tabData, false) !== false)
                );
            }
        }

        // Return result
        return $result;
    }



    /**
     * Save specified entities,
     * following specified method (create, update).
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure saving execution.
     * Null means no specific execution configuration required.
     * [
     *     persistor_add_config(optional): [
     *         // Optional additional persistor configuration, in case of method create
     *         @see PersistorInterface::createTabData() configuration array format
     *
     *         OR
     *
     *         // Optional additional persistor configuration, in case of method update
     *         @see PersistorInterface::updateTabData() configuration array format
     *     ]
     * ]
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about saving execution.
     * Null means return information not required.
     * [
     *     persistor_info(optional): [
     *         // In case of method create
     *         @see PersistorInterface::createTabData() return information array format
     *
     *         OR
     *
     *         // In case of method update
     *         @see PersistorInterface::updateTabData() return information array format
     *     ]
     * ]
     *
     * @param SaveEntityInterface[] $tabEntity
     * @param boolean $boolCreate
     * @param EntityCollectionInterface $objCollection
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     * @throws IdInvalidFormatException
     */
    protected function saveEngine(
        array $tabEntity,
        $boolCreate,
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;
        $objRepository = $this->getObjRepository();
        $objPersistor = $this->getObjPersistor();
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());

        if((!is_null($objRepository)) && (!is_null($objPersistor)))
        {
            // Check at least one entity to save
            $result = (count($tabEntity) == 0);
            if(!$result)
            {
                // Get index array of data
                $tabData = array_map(
                    function($objEntity) use ($objRepository) {return $objRepository->getTabData($objEntity);},
                    $tabEntity
                );
                $tabOriginalData = $tabData;

                // Save data
                $tabId = array();
                $tabPersistorInfo = (is_null($tabInfo) ? null : array());
                $result = (
                    $boolCreate ?
                        $objPersistor->createTabData(
                            $tabData,
                            $this->getTabConfigPersistCreate($objCollection, $tabConfig),
                            $tabId,
                            $tabPersistorInfo
                        ) :
                        $objPersistor->updateTabData(
                            $tabData,
                            $this->getTabConfigPersistUpdate($objCollection, $tabConfig),
                            $tabPersistorInfo
                        )
                );

                // Register info, if required
                if((!is_null($tabPersistorInfo)) && (count($tabPersistorInfo) > 0))
                {
                    $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO] = $tabPersistorInfo;
                }

                // Update entities, if required
                if($result)
                {
                    // Run each entity and update it, if required
                    $tabData = array_filter(
                        array_values($tabData),
                        function($data) {return is_array($data);}
                    );
                    $boolTabDataValid = (count($tabEntity) == count($tabData));
                    for($intCpt = 0; ($intCpt < count($tabEntity)); $intCpt++)
                    {
                        // Get info
                        $objEntity = $tabEntity[$intCpt];
                        $id = (isset($tabId[$intCpt]) ? $tabId[$intCpt] : null);

                        // Set data, if required
                        if($boolTabDataValid)
                        {
                            $data = $tabData[$intCpt];
                            $originalData = $tabOriginalData[$intCpt];
                            if($data !== $originalData)
                            {
                                $objRepository->setData($objEntity, $data);
                            }
                        }

                        // Set new id, if required
                        if($boolCreate)
                        {
                            IdInvalidFormatException::setCheck($id);
                            $objEntity->setAttributeValueSave($objRepository->getStrAttributeKeyId(), $id);
                        }

                        $objEntity->setIsNew(false);
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     create_execution_config(optional): [
     *         @see saveEngine() configuration array format (In case of method create)
     *     ],
     *
     *     update_execution_config(optional): [
     *         @see saveEngine() configuration array format (In case of method update)
     *     ],
     *
     *     sub_repository_execution_require(optional: got true if not found): true / false,
     *
     *     sub_repository_execution_config(optional: got all of current configuration, if not found): [
     *         @see saveSubRepo() configuration array format
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     create_info(optional): [
     *         @see saveEngine() return information array format (In case of method create)
     *     ],
     *
     *     update_info(optional): [
     *         @see saveEngine() return information array format (In case of method update)
     *     ],
     *
     *     sub_repository_info(optional): [
     *         @see saveSubRepo() return information array format (for all sub-elements)
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws CollectionInvalidFormatException
     * @throws IdInvalidFormatException
     * @throws CollectionExecConfigInvalidFormatException
     */
    public function save(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);
        CollectionExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $boolSubRepo = $this->checkSubRepoExecutionRequired($tabConfig);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $result = $this->executeTransaction(
            function() use ($objCollection, $boolSubRepo, $tabConfig, &$tabInfo)
            {
                // Init entities
                $tabCreateEntity = array();
                $tabUpdateEntity = array();
                foreach($objCollection->getTabKey() as $strKey)
                {
                    /** @var SaveEntityInterface $objEntity */
                    $objEntity = $objCollection->getItem($strKey);
                    if($objEntity->checkIsNew())
                    {
                        $tabCreateEntity[] = $objEntity;
                    }
                    else{
                        $tabUpdateEntity[] = $objEntity;
                    }
                }

                // Save collection
                $tabCreateConfig = (
                (
                    (!is_null($tabConfig)) &&
                    array_key_exists(
                        ConstMultiRepository::TAB_EXEC_CONFIG_KEY_CREATE_EXECUTION_CONFIG,
                        $tabConfig
                    )
                ) ?
                    $tabConfig[ConstMultiRepository::TAB_EXEC_CONFIG_KEY_CREATE_EXECUTION_CONFIG] :
                    null
                );
                $tabCreateInfo = (is_null($tabInfo) ? null : array());
                $boolCreate = $this->saveEngine(
                    $tabCreateEntity,
                    true,
                    $objCollection,
                    $tabCreateConfig,
                    $tabCreateInfo
                );
                $tabUpdateConfig = (
                (
                    (!is_null($tabConfig)) &&
                    array_key_exists(
                        ConstMultiRepository::TAB_EXEC_CONFIG_KEY_UPDATE_EXECUTION_CONFIG,
                        $tabConfig
                    )
                ) ?
                    $tabConfig[ConstMultiRepository::TAB_EXEC_CONFIG_KEY_UPDATE_EXECUTION_CONFIG] :
                    null
                );
                $tabUpdateInfo = (is_null($tabInfo) ? null : array());
                $boolUpdate = $this->saveEngine(
                    $tabUpdateEntity,
                    false,
                    $objCollection,
                    $tabUpdateConfig,
                    $tabUpdateInfo
                );
                $result = $boolCreate && $boolUpdate;

                // Register info, if required
                if(
                    (!is_null($tabCreateInfo)) &&
                    (count($tabCreateInfo) > 0)
                )
                {
                    $tabInfo[ConstMultiRepository::TAB_INFO_KEY_CREATE_INFO] = $tabCreateInfo;
                }

                if(
                    (!is_null($tabUpdateInfo)) &&
                    (count($tabUpdateInfo) > 0)
                )
                {
                    $tabInfo[ConstMultiRepository::TAB_INFO_KEY_UPDATE_INFO] = $tabUpdateInfo;
                }

                // Update entities, if required
                if($result)
                {
                    // Save entities sub-element, if required
                    $tabSubRepoConfig = (
                    (
                        (!is_null($tabConfig)) &&
                        array_key_exists(
                            ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG,
                            $tabConfig
                        )
                    ) ?
                        $tabConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG] :
                        $tabConfig
                    );
                    $tabSubRepoInfo = (is_null($tabInfo) ? null : array());
                    $result = (!$boolSubRepo) || $this->saveSubRepo($objCollection, $tabSubRepoConfig, $tabSubRepoInfo);

                    // Register sub-repository info, if required
                    if(
                        (!is_null($tabSubRepoInfo)) &&
                        (count($tabSubRepoInfo) > 0)
                    )
                    {
                        $tabInfo[ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO] = $tabSubRepoInfo;
                    }
                }

                // Return result
                return $result;
            },
            function($result) {return $result;}
        );

        // Return result
        return $result;
    }



}