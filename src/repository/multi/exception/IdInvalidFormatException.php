<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\multi\exception;

use liberty_code\model\repository\multi\library\ConstMultiRepository;



class IdInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $id
     */
	public function __construct($id)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstMultiRepository::EXCEPT_MSG_ID_INVALID_FORMAT, strval($id));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

	/**
	 * Check if specified identifier has valid format
	 * 
     * @param mixed $id
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($id)
    {
		// Init var
		$result =
            (!is_null($id)) && (is_string($id) || (is_numeric($id))); // Check valid value

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($id);
		}
		
		// Return result
		return $result;
    }
	
	
	
}