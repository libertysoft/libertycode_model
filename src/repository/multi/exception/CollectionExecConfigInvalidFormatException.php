<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\multi\exception;

use Exception;

use liberty_code\model\repository\multi\library\ConstMultiRepository;



class CollectionExecConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultiRepository::EXCEPT_MSG_COLLECTION_EXEC_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid creation execution configuration
            (
                (!array_key_exists(ConstMultiRepository::TAB_EXEC_CONFIG_KEY_CREATE_EXECUTION_CONFIG, $config)) ||
                is_null($config[ConstMultiRepository::TAB_EXEC_CONFIG_KEY_CREATE_EXECUTION_CONFIG]) ||
                is_array($config[ConstMultiRepository::TAB_EXEC_CONFIG_KEY_CREATE_EXECUTION_CONFIG])
            ) &&

            // Check valid updating execution configuration
            (
                (!array_key_exists(ConstMultiRepository::TAB_EXEC_CONFIG_KEY_UPDATE_EXECUTION_CONFIG, $config)) ||
                is_null($config[ConstMultiRepository::TAB_EXEC_CONFIG_KEY_UPDATE_EXECUTION_CONFIG]) ||
                is_array($config[ConstMultiRepository::TAB_EXEC_CONFIG_KEY_UPDATE_EXECUTION_CONFIG])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}