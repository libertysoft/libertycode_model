<?php
/**
 * Description :
 * This class allows to define fixed multi repository class.
 * Fixed multi repository allows to prepare data for complex save, in persistence,
 * from a specified fixed configuration.
 *
 * Fixed multi repository allows to specify parts of configuration:
 * [
 *     Multi repository configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\multi\fix\model;

use liberty_code\model\repository\multi\model\MultiRepository;

use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\multi\fix\exception\PersistorInvalidFormatException;
use liberty_code\model\repository\multi\fix\exception\ConfigInvalidFormatException;



abstract class FixMultiRepository extends MultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(PersistorInterface $objPersistor = null)
    {
        // Init var
        $tabConfig = $this->getTabFixConfig();

        // Call parent constructor
        parent::__construct($tabConfig, $objPersistor);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_PERSISTOR:
                    $strPersistorClassPath = $this->getStrFixPersistorClassPath();
                    PersistorInvalidFormatException::setCheck($value, $strPersistorClassPath);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_CONFIG:
                    $tabConfig = $this->getTabFixConfig();
                    ConfigInvalidFormatException::setCheck($value, $tabConfig);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed class path of persistor.
     * Null means no specific class path fixed.
     * Overwrite it to implement specific class path.
     *
     * @return null|string
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return null;
    }



    /**
     * Get fixed configuration array.
     *
     * @return array
     */
    abstract protected function getTabFixConfig();



}