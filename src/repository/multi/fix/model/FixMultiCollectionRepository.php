<?php
/**
 * Description :
 * This class allows to define fixed multi collection repository class.
 * Fixed multi collection repository type allows to prepare array of data for complex save, in persistence,
 * from a specified fixed configuration.
 *
 * Fixed multi collection repository allows to specify parts of configuration:
 * [
 *     Multi collection repository configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\multi\fix\model;

use liberty_code\model\repository\multi\model\MultiCollectionRepository;

use liberty_code\model\entity\factory\api\EntityFactoryInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\api\RepositoryInterface;
use liberty_code\model\repository\multi\fix\exception\CollectionEntityFactoryInvalidFormatException;
use liberty_code\model\repository\multi\fix\exception\CollectionRepositoryInvalidFormatException;
use liberty_code\model\repository\multi\fix\exception\CollectionConfigInvalidFormatException;



abstract class FixMultiCollectionRepository extends MultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        EntityFactoryInterface $objEntityFactory = null,
        RepositoryInterface $objRepository = null
    )
    {
        // Init var
        $tabConfig = $this->getTabFixConfig();

        // Call parent constructor
        parent::__construct($objEntityFactory, $objRepository, $tabConfig);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_ENTITY_FACTORY:
                    $strEntityFactoryClassPath = $this->getStrFixEntityFactoryClassPath();
                    CollectionEntityFactoryInvalidFormatException::setCheck($value, $strEntityFactoryClassPath);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY:
                    $strRepositoryClassPath = $this->getStrFixRepositoryClassPath();
                    CollectionRepositoryInvalidFormatException::setCheck($value, $strRepositoryClassPath);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_CONFIG:
                    $tabConfig = $this->getTabFixConfig();
                    CollectionConfigInvalidFormatException::setCheck($value, $tabConfig);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed class path of entity factory.
     * Null means no specific class path fixed.
     * Overwrite it to implement specific class path.
     *
     * @return null|string
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return null;
    }



    /**
     * Get fixed class path of repository.
     * Null means no specific class path fixed.
     * Overwrite it to implement specific class path.
     *
     * @return null|string
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return null;
    }



    /**
     * Get fixed configuration array.
     * Overwrite it to implement specific configuration.
     *
     * @return array
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array();
    }



}