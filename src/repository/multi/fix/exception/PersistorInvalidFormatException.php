<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\multi\fix\exception;

use liberty_code\model\repository\multi\fix\library\ConstFixMultiRepository;



class PersistorInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $persistor
     */
	public function __construct($persistor)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFixMultiRepository::EXCEPT_MSG_PERSISTOR_INVALID_FORMAT,
            mb_strimwidth(strval($persistor), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified persistor has valid format.
	 * 
     * @param mixed $persistor
     * @param string $strFixClassPath = null
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($persistor, $strFixClassPath = null)
    {
        // Init var
        $result =
            // Check specified class path, not required
            is_null($strFixClassPath) ||

            // Check specified class path, required
            ($persistor instanceof $strFixClassPath);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($persistor);
        }
		
		// Return result
		return $result;
    }
	
	
	
}