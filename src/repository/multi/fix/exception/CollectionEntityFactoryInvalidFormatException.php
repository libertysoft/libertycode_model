<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\multi\fix\exception;

use liberty_code\model\repository\multi\fix\library\ConstFixMultiRepository;



class CollectionEntityFactoryInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $factory
     */
	public function __construct($factory)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFixMultiRepository::EXCEPT_MSG_COLLECTION_ENTITY_FACTORY_INVALID_FORMAT,
            mb_strimwidth(strval($factory), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified factory has valid format.
	 * 
     * @param mixed $factory
     * @param string $strFixClassPath = null
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($factory, $strFixClassPath = null)
    {
		// Init var
		$result =
            // Check specified class path, not required
            is_null($strFixClassPath) ||

            // Check specified class path, required
            ($factory instanceof $strFixClassPath);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($factory);
		}
		
		// Return result
		return $result;
    }
	
	
	
}