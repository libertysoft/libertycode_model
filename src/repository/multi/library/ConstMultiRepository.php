<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\multi\library;



class ConstMultiRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID = 'attribute_key_id';

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_CREATE_EXECUTION_CONFIG = 'create_execution_config';
    const TAB_EXEC_CONFIG_KEY_UPDATE_EXECUTION_CONFIG = 'update_execution_config';

    // Information configuration
    const TAB_INFO_KEY_CREATE_INFO = 'create_info';
    const TAB_INFO_KEY_UPDATE_INFO = 'update_info';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the multi repository configuration standard.';
    const EXCEPT_MSG_ID_INVALID_FORMAT = 'Following identifier "%1$s" invalid! The identifier must be a valid value (numeric or string).';
    const EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT = 'Following repository "%1$s" invalid! It must be a multi repository object.';
    const EXCEPT_MSG_COLLECTION_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the multi collection repository execution configuration standard.';



}