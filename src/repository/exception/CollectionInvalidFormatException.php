<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\exception;

use liberty_code\model\repository\library\ConstRepository;



class CollectionInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 *
	 * @param mixed $collection
     */
	public function __construct($collection)
	{
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf(
            ConstRepository::EXCEPT_MSG_COLLECTION_INVALID_FORMAT,
            mb_strimwidth(strval($collection), 0, 50, "...")
        );
	}
	
	
	
}