<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\exception;

use liberty_code\model\repository\library\ConstRepository;



class EntityInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 *
	 * @param mixed $entity
     * @param string $classPath = null
     */
	public function __construct($entity, $classPath = null)
	{
        // Call parent constructor
        parent::__construct();

        // Init var
        $strClassPath =
            (
                (!is_null($classPath)) &&
                is_string($classPath) && (trim($classPath) != '')
            ) ?
                ' (' . trim($classPath) . ')' :
                '';
        $this->message = sprintf(
            ConstRepository::EXCEPT_MSG_ENTITY_INVALID_FORMAT,
            mb_strimwidth(strval($entity), 0, 50, "..."),
            $strClassPath
        );
	}
	
	
	
}