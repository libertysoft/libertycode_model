<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\exception;

use Exception;

use liberty_code\model\repository\library\ConstRepository;



class CollectionConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRepository::EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid client execution configuration
            (
                (!isset($config[ConstRepository::TAB_CONFIG_KEY_ENTITY_FACTORY_EXECUTION_CONFIG])) ||
                is_array($config[ConstRepository::TAB_CONFIG_KEY_ENTITY_FACTORY_EXECUTION_CONFIG])
            ) &&

            // Check valid transaction required option
            (
                (!isset($config[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE]) ||
                    is_int($config[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE]) ||
                    (
                        is_string($config[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE]) &&
                        ctype_digit($config[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}