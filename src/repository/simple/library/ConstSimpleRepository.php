<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\simple\library;



class ConstSimpleRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_CREATE_EXECUTION_CONFIG = 'create_execution_config';
    const TAB_EXEC_CONFIG_KEY_UPDATE_EXECUTION_CONFIG = 'update_execution_config';

    // Information configuration
    const TAB_INFO_KEY_CREATE_INFO = 'create_info';
    const TAB_INFO_KEY_UPDATE_INFO = 'update_info';



    // Exception message constants
    const EXCEPT_MSG_COLLECTION_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the simple collection repository execution configuration standard.';



}