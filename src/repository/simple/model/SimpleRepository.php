<?php
/**
 * Description :
 * This class allows to define simple repository class.
 * Simple repository allows to prepare data for basic save, in persistence.
 *
 * Simple repository allows to specify parts of configuration:
 * [
 *     Sub-repo repository configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\simple\model;

use liberty_code\model\repository\sub_repository\model\SubRepoRepository;

use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\exception\EntityInvalidFormatException;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;



class SimpleRepository extends SubRepoRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods repository
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     persistor_add_config(optional): [
     *         // Optional additional persistor configuration
     *         @see PersistorInterface::getData() configuration array format
     *     ],
     *
     *     sub_repository_execution_require(optional: got true if not found): true / false,
     *
     *     sub_repository_execution_config(optional: got all of current configuration, if not found): [
     *         @see loadSubRepo() configuration array format (for all sub-elements)
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     persistor_info(optional): [
     *         @see PersistorInterface::getData() return information array format
     *     ],
     *
     *     sub_repository_info(optional): [
     *         @see loadSubRepo() return information array format (for all sub-elements)
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws EntityInvalidFormatException
     */
    public function load(
        SaveEntityInterface $objEntity,
        $criteria = null,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check entity
        $this->setCheckValidEntity($objEntity);

        // Init var
        $result = false;
        $boolSubRepo = $this->checkSubRepoExecutionRequired($tabConfig);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $boolLoadEmptyEnable = $this->checkLoadEmptyEnableRequired();
        $objPersistor = $this->getObjPersistor();

        if(!is_null($objPersistor))
        {
            // Get data from persistence
            $tabPersistorInfo = (is_null($tabInfo) ? null : array());
            $tabData = $objPersistor->getData(
                null,
                $this->getTabConfigPersistGet($objEntity, $tabConfig),
                $tabPersistorInfo
            );

            // Register info, if required
            if(
                (!is_null($tabPersistorInfo)) &&
                (count($tabPersistorInfo) > 0)
            )
            {
                $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO] = $tabPersistorInfo;
            }

            if($tabData !== false)
            {
                // Hydrate data to entity, if required
                $boolLoadFound = (is_array($tabData) && (count($tabData) > 0));
                if($boolLoadFound)
                {
                    $this->setData($objEntity, $tabData);
                    $objEntity->setIsNew(false);
                }

                // Load sub-element, if required
                $tabSubRepoConfig = (
                    (
                        (!is_null($tabConfig)) &&
                        array_key_exists(
                            ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG,
                            $tabConfig
                        )
                    ) ?
                        $tabConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG] :
                        $tabConfig
                );
                $tabSubRepoInfo = (is_null($tabInfo) ? null : array());
                $result = (
                    ($boolLoadEmptyEnable || $boolLoadFound) &&
                    (
                        (!$boolSubRepo) ||
                        $this->loadSubRepo(
                            $objEntity,
                            $criteria,
                            true,
                            null,
                            $tabSubRepoConfig,
                            $tabSubRepoInfo
                        )
                    )
                );

                // Register sub-repository info, if required
                if(
                    (!is_null($tabSubRepoInfo)) &&
                    (count($tabSubRepoInfo) > 0)
                )
                {
                    $tabInfo[ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO] = $tabSubRepoInfo;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     persistor_add_config(optional): [
     *         // Optional additional persistor configuration
     *         @see PersistorInterface::createData() configuration array format
     *         OR
     *         @see PersistorInterface::updateData() configuration array format
     *     ],
     *
     *     sub_repository_execution_require(optional: got true if not found): true / false,
     *
     *     sub_repository_execution_config(optional: got all of current configuration, if not found): [
     *         @see saveSubRepo() configuration array format (for all sub-elements)
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     persistor_info(optional): [
     *         @see PersistorInterface::createData() return information array format
     *         OR
     *         @see PersistorInterface::updateData() return information array format
     *     ],
     *
     *     sub_repository_info(optional): [
     *         @see saveSubRepo() return information array format (for all sub-elements)
     *     ]
     * ]
     *
     * @inheritdoc
     * @param boolean $boolSubRepo = true
     * @throws EntityInvalidFormatException
     */
    public function save(
        SaveEntityInterface $objEntity,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $boolSubRepo = $this->checkSubRepoExecutionRequired($tabConfig);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $result = $this->executeTransaction(
            function() use ($objEntity, $boolSubRepo, $tabConfig, &$tabInfo)
            {
                // Set check arguments
                $this->setCheckValidEntity($objEntity);

                // Init var
                $objPersistor = $this->getObjPersistor();

                // Get data from entity
                $tabData = $this->getTabData($objEntity);
                $tabOriginalData = $tabData;

                // Save data
                $id = null;
                $tabPersistorInfo = (is_null($tabInfo) ? null : array());
                $result = (
                    $objEntity->checkIsNew() ?
                        // Create data
                        $objPersistor->createData(
                            $tabData,
                            $this->getTabConfigPersistCreate($objEntity, $tabConfig),
                            $id,
                            $tabPersistorInfo
                        ) :
                        // Update data
                        $objPersistor->updateData(
                            $tabData,
                            $this->getTabConfigPersistUpdate($objEntity, $tabConfig),
                            $tabPersistorInfo
                        )
                );

                // Register info, if required
                if(
                    (!is_null($tabPersistorInfo)) &&
                    (count($tabPersistorInfo) > 0)
                )
                {
                    $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO] = $tabPersistorInfo;
                }

                // Update entity, if required
                if($result)
                {
                    // Set data, if required
                    if($tabData !== $tabOriginalData)
                    {
                        $this->setData($objEntity, $tabData);
                    }

                    $objEntity->setIsNew(false);

                    // Save sub-element, if required
                    $tabSubRepoConfig = (
                    (
                        (!is_null($tabConfig)) &&
                        array_key_exists(
                            ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG,
                            $tabConfig
                        )
                    ) ?
                        $tabConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG] :
                        $tabConfig
                    );
                    $tabSubRepoInfo = (is_null($tabInfo) ? null : array());
                    $result =
                        (!$boolSubRepo) ||
                        $this->saveSubRepo(
                            $objEntity,
                            true,
                            null,
                            $tabSubRepoConfig,
                            $tabSubRepoInfo
                        );

                    // Register sub-repository info, if required
                    if(
                        (!is_null($tabSubRepoInfo)) &&
                        (count($tabSubRepoInfo) > 0)
                    )
                    {
                        $tabInfo[ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO] = $tabSubRepoInfo;
                    }
                }

                // Return result
                return $result;
            },
            function($result) {return $result;}
        );

        // Return result
        return $result;
    }



}