<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\simple\fix\library;



class ConstFixSimpleRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
    const EXCEPT_MSG_PERSISTOR_INVALID_FORMAT = 'Following persistor "%1$s" invalid! It must be a valid persistor object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the fixed simple configuration standard.';
    const EXCEPT_MSG_COLLECTION_ENTITY_FACTORY_INVALID_FORMAT = 'Following factory "%1$s" invalid! It must be a valid entity factory object.';
    const EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT = 'Following repository "%1$s" invalid! It must be a valid repository object.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the fixed simple configuration standard.';
}