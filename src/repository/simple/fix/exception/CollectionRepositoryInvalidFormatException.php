<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\simple\fix\exception;

use liberty_code\model\repository\simple\fix\library\ConstFixSimpleRepository;



class CollectionRepositoryInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $repository
     */
	public function __construct($repository)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(
            ConstFixSimpleRepository::EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT,
            strval($repository)
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified repository has valid format.
	 *
     * @param mixed $repository
     * @param string $strFixClassPath = null
     * @return boolean
     * @throws static
     */
    static public function setCheck($repository, $strFixClassPath = null)
    {
        // Init var
        $result =
            // Check specified class path, not required
            is_null($strFixClassPath) ||

            // Check specified class path, required
            ($repository instanceof $strFixClassPath);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($repository);
        }

        // Return result
        return $result;
    }
	
	
	
}