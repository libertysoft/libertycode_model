<?php
/**
 * Description :
 * This class allows to define fixed simple repository class.
 * Fixed simple repository allows to prepare data for basic save, in persistence,
 * from a specified fixed configuration.
 *
 * Fixed simple repository allows to specify parts of configuration:
 * [
 *     Simple repository configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\simple\fix\model;

use liberty_code\model\repository\simple\model\SimpleRepository;

use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\simple\fix\exception\PersistorInvalidFormatException;
use liberty_code\model\repository\simple\fix\exception\ConfigInvalidFormatException;



abstract class FixSimpleRepository extends SimpleRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(PersistorInterface $objPersistor = null)
    {
        // Init var
        $tabConfig = $this->getTabFixConfig();

        // Call parent constructor
        parent::__construct($tabConfig, $objPersistor);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_PERSISTOR:
                    $strPersistorClassPath = $this->getStrFixPersistorClassPath();
                    PersistorInvalidFormatException::setCheck($value, $strPersistorClassPath);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_CONFIG:
                    $tabConfig = $this->getTabFixConfig();
                    ConfigInvalidFormatException::setCheck($value, $tabConfig);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed class path of persistor.
     * Null means no specific class path fixed.
     * Overwrite it to implement specific class path.
     *
     * @return null|string
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return null;
    }



    /**
     * Get fixed configuration array.
     *
     * @return array
     */
    abstract protected function getTabFixConfig();



}