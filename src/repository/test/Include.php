<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/repository/test/persistence/MemoryPersistor.php');
require_once($strRootAppPath . '/src/repository/test/model/config/Config1Entity.php');
require_once($strRootAppPath . '/src/repository/test/model/config/repository/Config1Repository.php');
require_once($strRootAppPath . '/src/repository/test/model/right/RightEntity.php');
require_once($strRootAppPath . '/src/repository/test/model/right/RightCollection.php');
require_once($strRootAppPath . '/src/repository/test/model/right/RightFactory.php');
require_once($strRootAppPath . '/src/repository/test/model/right/repository/RightRepository.php');
require_once($strRootAppPath . '/src/repository/test/model/right/repository/RightCollectionRepository.php');
require_once($strRootAppPath . '/src/repository/test/model/admin/AdminEntity.php');
require_once($strRootAppPath . '/src/repository/test/model/admin/AdminCollection.php');
require_once($strRootAppPath . '/src/repository/test/model/admin/AdminFactory.php');
require_once($strRootAppPath . '/src/repository/test/model/admin/repository/AdminRepository.php');
require_once($strRootAppPath . '/src/repository/test/model/admin/repository/AdminCollectionRepository.php');
require_once($strRootAppPath . '/src/repository/test/model/user/UserEntity.php');
require_once($strRootAppPath . '/src/repository/test/model/user/UserCollection.php');
require_once($strRootAppPath . '/src/repository/test/model/user/UserFactory.php');
require_once($strRootAppPath . '/src/repository/test/model/user/repository/UserRepository.php');
require_once($strRootAppPath . '/src/repository/test/model/user/repository/UserCollectionRepository.php');

// Use
use liberty_code\data\data\table\path\model\PathTableData;
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\config\config\data\model\DataConfig;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\model\entity\model\DefaultEntityCollection;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\test\persistence\MemoryPersistor;
use liberty_code\model\repository\test\model\config\repository\Config1Repository;
use liberty_code\model\repository\test\model\right\RightCollection;
use liberty_code\model\repository\test\model\right\RightFactory;
use liberty_code\model\repository\test\model\right\repository\RightRepository;
use liberty_code\model\repository\test\model\right\repository\RightCollectionRepository;
use liberty_code\model\repository\test\model\admin\AdminCollection;
use liberty_code\model\repository\test\model\admin\AdminFactory;
use liberty_code\model\repository\test\model\admin\repository\AdminRepository;
use liberty_code\model\repository\test\model\admin\repository\AdminCollectionRepository;
use liberty_code\model\repository\test\model\user\UserFactory;
use liberty_code\model\repository\test\model\user\repository\UserRepository;
use liberty_code\model\repository\test\model\user\repository\UserCollectionRepository;



// Init DI provider
$objData = new PathTableData();
$objConfigExt = new DataConfig($objData);
$objRegister = new MemoryRegister();
$objDepCollection = new DefaultDependencyCollection($objRegister, $objConfigExt);
$objDefaultProvider = new DefaultProvider($objDepCollection);

// Init var
$objPersistor = new MemoryPersistor();
$objConfig1Repo = new Config1Repository($objPersistor);
$objRightCollection = new RightCollection();
$objRightRepo = new RightRepository($objPersistor);
$objRightFacto = new RightFactory($objDefaultProvider);
$objRightCollectionRepo = new RightCollectionRepository($objRightFacto, $objRightRepo);
$objAdminCollection = new AdminCollection();
$objAdminRepo = new AdminRepository($objPersistor, $objRightCollectionRepo);
$objAdminFacto = new AdminFactory($objDefaultProvider);
$objAdminCollectionRepo = new AdminCollectionRepository($objAdminFacto, $objAdminRepo, $objRightCollectionRepo, $objRightCollection);
$objUserRepo = new UserRepository($objPersistor, $objAdminRepo);
$objUserFacto = new UserFactory($objDefaultProvider);
$objUserCollectionRepo = new UserCollectionRepository($objUserFacto, $objUserRepo, $objAdminCollectionRepo, $objAdminCollection);



// Utility
function getTabEntityValue(SaveEntityInterface $objEntity)
{
    // Init var
    $result = array('isNew' => ($objEntity->checkIsNew() ? 1 : 0));

    // Run all attributes
    foreach($objEntity->getTabAttributeKey() as $strKey)
    {
        $result[$strKey] = $objEntity->getAttributeValue($strKey, true);

        if($result[$strKey] instanceof SaveEntityInterface)
        {
            $result[$strKey] = getTabEntityValue($result[$strKey]);
        }
        else if($result[$strKey] instanceof DefaultEntityCollection)
        {
            $result[$strKey] = getTabCollectionValue($result[$strKey]);
        }
    }

    // Return result
    return $result;
}



function getTabCollectionValue(DefaultEntityCollection $objCollection)
{
    // Init var
    $result = array();

    // Run all attributes
    foreach($objCollection->getTabKey() as $strKey)
    {
        $objEntity = $objCollection->getItem($strKey);
        $tabEntityValue = getTabEntityValue($objEntity);
        $result[$strKey] = $tabEntityValue;
    }

    // Return result
    return $result;
}


