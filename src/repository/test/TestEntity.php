<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/repository/test/Include.php');

// Use
use liberty_code\model\repository\test\model\config\Config1Entity;
use liberty_code\model\repository\test\model\right\RightEntity;
use liberty_code\model\repository\test\model\right\RightCollection;
use liberty_code\model\repository\test\model\admin\AdminEntity;
use liberty_code\model\repository\test\model\user\UserEntity;



// Test create user entity
$objUser = new UserEntity(array(
    'login' => 'usr_1',
    'pw' => 'pw_1',
    'nmFirst' => 'Fnm 1',
    'nm' => 'Nm 1',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 1',
        'rightCollection' => new RightCollection()
    ))
));
$objUser->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 1 - Rgt 1',
    'right' => true
)));
$objUser->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 1 - Rgt 2',
    'right' => false
)));
$objUser->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 1 - Rgt 3',
    'right' => true
)));

echo('Test user create entity: <br />');
echo('Entity: <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');

echo('<br /><br /><br />');



// Test save user entity (create)
$tabConfig = array();
$tabInfo = array();
$boolSave = $objUserRepo->save($objUser, $tabConfig, $tabInfo);
$strId = $objUser->id;
$strAdmId = $objUser->admin->id;
$tabRghtId = array();
foreach($objUser->admin->rightCollection->getTabKey() as $strKey)
{
    $tabRghtId[] = $objUser->admin->rightCollection->getItem($strKey)->id;
}
$criteriaAdm = array(
    'criteria' => $strAdmId,
    'sub_entity_criteria' => [
        'rightCollection' => $tabRghtId
    ]
);
$criteriaUser = array(
    'criteria' => $strId,
    'sub_entity_criteria' => [
        'admin' => $criteriaAdm
    ]
);

echo('Test save user entity (create): <br />');
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');
echo('Db data: <pre>');var_dump($objPersistor->getTabDbData());echo('</pre>');
echo('Db config: <pre>');var_dump($objPersistor->getTabDbConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test load user entity, without sub-repo
echo('Test load user entity(without sub-repo): <br />');

$objUser = new UserEntity();

echo('Entity (previous): <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');

$tabConfig = array(
    'sub_repository_execution_require' => false,
    'sub_repository_execution_config' => [
        'admin' => [
            'sub_repository_execution_require' => false
        ]
    ]
);
$tabInfo = array();
$boolLoad = $objUserRepo->load($objUser, $criteriaUser, $tabConfig,$tabInfo);

echo('Load: <pre>');var_dump($boolLoad);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');

echo('<br /><br /><br />');



// Test load user entity, without right sub-repo
echo('Test load user entity(without right sub-repo): <br />');

$objUser = new UserEntity();

echo('Entity (previous): <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');

$tabConfig = array(
    'sub_repository_execution_config' => [
        'admin' => [
            'sub_repository_execution_require' => false
        ]
    ]
);
$tabInfo = array();
$boolLoad = $objUserRepo->load($objUser, $criteriaUser, $tabConfig,$tabInfo);

echo('Load: <pre>');var_dump($boolLoad);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');

echo('<br /><br /><br />');



// Test load user entity
echo('Test load user entity: <br />');

$objUser = new UserEntity();

echo('Entity (previous): <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');

$tabConfig = null;
$tabInfo = array();
$boolLoad = $objUserRepo->load($objUser, $criteriaUser, $tabConfig,$tabInfo);

echo('Load: <pre>');var_dump($boolLoad);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');

echo('<br /><br /><br />');



// Test save user entity (update)
$objUser->login = 'user_1';
$objUser->nmFirst = 'First Name 1';
$objUser->nm = 'Name 1';
$objUser->admin->nm = 'Admin 1';
$objUser->admin->rightCollection->getItem('entity_' . $tabRghtId[0])->nm = 'Admin 1 - Right 1';
$objUser->admin->rightCollection->getItem('entity_' . $tabRghtId[1])->nm = 'Admin 1 - Right 2';
$objUser->admin->rightCollection->getItem('entity_' . $tabRghtId[2])->nm = 'Admin 1 - Right 3';

$tabInfo = null;
$boolSave = $objUserRepo->save($objUser, null, $tabInfo);

echo('Test save user entity (update): <br />');
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');
echo('Info: <pre>');var_dump($tabInfo);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');
echo('Db data: <pre>');var_dump($objPersistor->getTabDbData());echo('</pre>');
echo('Db config: <pre>');var_dump($objPersistor->getTabDbConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test remove user entity
//$tabConfig = null;
$tabConfig = array(
    'sub_repository_execution_config' => [
        'admin' => [
            'sub_repository_execution_require' => false
        ]
    ]
);
$tabInfo = array();
$boolRemove = $objUserRepo->remove($objUser, $tabConfig, $tabInfo);

echo('Test remove user entity: <br />');
echo('Remove: <pre>');var_dump($boolRemove);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objUser));echo('</pre>');
echo('Db data: <pre>');var_dump($objPersistor->getTabDbData());echo('</pre>');
echo('Db config: <pre>');var_dump($objPersistor->getTabDbConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test create config 1 entity
$objConfig1 = new Config1Entity();

echo('Test config create entity: <br />');
echo('Entity: <pre>');print_r(getTabEntityValue($objConfig1));echo('</pre>');

echo('<br /><br /><br />');



// Test save config 1 entity
$objConfig1 = new Config1Entity();
$objConfig1->prm_1 = 'Param 1';
$objConfig1->prm_2 = 7;
$objConfig1->prm_3 = true;
$boolSave = $objConfig1Repo->save($objConfig1);

echo('Test save config 1 entity: <br />');
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objConfig1));echo('</pre>');
echo('Db data: <pre>');var_dump($objPersistor->getTabDbData());echo('</pre>');
echo('Db config: <pre>');var_dump($objPersistor->getTabDbConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test load config 1 entity
echo('Test load config 1 entity: <br />');

$objConfig1 = new Config1Entity();

echo('Entity (previous): <pre>');print_r(getTabEntityValue($objConfig1));echo('</pre>');

$tabInfo = array();
$boolLoad = $objConfig1Repo->load($objConfig1, null, array(), $tabInfo);

echo('Load: <pre>');var_dump($boolLoad);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objConfig1));echo('</pre>');

echo('<br /><br /><br />');



// Test remove config 1 entity
$tabInfo = array();
$boolRemove = $objConfig1Repo->remove($objConfig1, null, $tabInfo);

echo('Test remove config 1 entity: <br />');
echo('remove: <pre>');var_dump($boolRemove);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Entity: <pre>');print_r(getTabEntityValue($objConfig1));echo('</pre>');
echo('Db data: <pre>');var_dump($objPersistor->getTabDbData());echo('</pre>');
echo('Db config: <pre>');var_dump($objPersistor->getTabDbConfig());echo('</pre>');

echo('<br /><br /><br />');


