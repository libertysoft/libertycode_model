<?php

namespace liberty_code\model\repository\test\persistence;

use liberty_code\model\persistence\api\PersistorInterface;

use DateTime;



class MemoryPersistor implements PersistorInterface
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /** @var array */
    protected $tabData;



    /** @var array */
    protected $tabConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * Constructor
     */
    public function __construct()
    {
        // Init data
        $this->tabData = array(
            'user' => [],
            'admin' => [],
            'right' => [],
        );

        // Init setting
        $this->tabConfig = array(
            'config_1' => [
                'prm_1_1' => 'Value 1 1',
                'prm_1_2' => 2.2,
                'prm_1_3' => 1
            ],
            'config_2' => [
                'prm_2_1' => 'Value 2 1',
                'prm_2_2' => 2.3,
                'prm_2_3' => 7,
                'prm_2_4' => true
            ]
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get array of data.
     *
     * @return array
     */
    public function getTabDbData()
    {
        // Return result
        return $this->tabData;
    }



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabDbConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * @inheritdoc
     */
    public function getData(
        $id = null,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;

        // Case data
        if(isset($tabConfig['data']))
        {
            $result = null;

            $strData = $tabConfig['data'];
            $strIdNm = $tabConfig['id_nm'];
            if(isset($this->tabData[$strData][$id]))
            {
                $result = array_merge(
                    [$strIdNm => $id],
                    $this->tabData[$strData][$id]
                );
            }
        }
        // Case config
        else if(isset($tabConfig['config']))
        {
            $result = null;

            $strConfig = $tabConfig['config'];
            if(isset($this->tabConfig[$strConfig]))
            {
                $result = $this->tabConfig[$strConfig];
            }
        }

        if(!is_null($tabInfo))
        {
            $tabInfo['action'] = 'get';
            $tabInfo['id'] = $id;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabData(
        array $tabId = array(),
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = array();

        // Run all ids
        foreach($tabId as $id)
        {
            $data = $this->getData($id, $tabConfig);

            if(is_array($data))
            {
                $result[] = $data;
            }
            else if($data === false)
            {
                $result = false;
                break;
            }
        }

        if(!is_null($tabInfo))
        {
            $tabInfo['action'] = 'get-multi';
            $tabInfo['id'] = $tabId;
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of criteria data,
     * from specified index array of ids.
     *
     * @param string $strData
     * @param array $tabId
     * @return array
     */
    public function getTabSearchCriteriaData($strData, $tabId)
    {
        // Init var
        $result = array();

        // Case data
        if(isset($this->tabData[$strData]))
        {
            // Run all ids
            for($intCpt = 0; $intCpt < count($tabId); $intCpt++)
            {
                // Check id exists
                $strId = $tabId[$intCpt];
                if(array_key_exists($strId, $this->tabData[$strData]))
                {
                    // Get info about data required
                    $colNm = null;
                    $strSubData = null;
                    $tabSubId = array();
                    switch($strData)
                    {
                        case 'user':
                            $colNm = 'usr_id';
                            $strSubData = 'admin';
                            $tabSubId = array();
                            $tabSubIdAll = array_keys($this->tabData['admin']);
                            if(isset($tabSubIdAll[$intCpt]))
                            {
                                $tabSubId[] = $tabSubIdAll[$intCpt];
                            }

                            break;

                        case 'admin':
                            $colNm = 'adm_id';
                            $strSubData = 'right';
                            $tabSubId = array();

                            foreach($this->tabData['right'] as $strSubId => $subData)
                            {
                                if($subData['rgt_adm_id'] == $strId)
                                {
                                    $tabSubId[] = $strSubId;
                                }
                            }

                            break;

                        case 'right':
                            $colNm = 'rgt_id';
                            $strSubData = null;
                            $tabSubId = array();

                            break;
                    }

                    // Process id, if required
                    if(!is_null($colNm))
                    {
                        // Get array of sub criteria data, if required
                        $tabSubCrit = array();
                        if((!is_null($strSubData)) && (count($tabSubId) > 0))
                        {
                            $tabSubCrit = $this->getTabSearchCriteriaData($strSubData, $tabSubId);
                        }

                        // Case sub criteria found
                        if(count($tabSubCrit) > 0)
                        {
                            foreach($tabSubCrit as $subCrit)
                            {
                                $data = array(
                                    $colNm => $strId
                                );

                                foreach($subCrit as $strKey => $value)
                                {
                                    $data[$strKey] = $value;
                                }

                                $result[] = $data;
                            }
                        }
                        // Case else: sub criteria not found
                        else
                        {
                            $data = array(
                                $colNm => $strId
                            );

                            switch($strData)
                            {
                                case 'user':
                                    $data['adm_id'] = null;
                                    $data['rgt_id'] = null;
                                    break;

                                case 'admin':
                                    $data['rgt_id'] = null;
                                    break;
                            }

                            $result[] = $data;
                        }
                    }
                }
            }

        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabSearchData(
        $query,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = array();

        if(is_array($query))
        {
            // Case data
            if(isset($tabConfig['data']))
            {
                $result = null;
                $tabId = array();
                $strData = $tabConfig['data'];
                $strIdNm = $tabConfig['id_nm'];
                $strColNm = $query['key'];
                $strColVal = $query['value'];
                $boolCriteria = $query['criteria'];

                foreach($this->tabData[$strData] as $id => $data)
                {
                    if(
                        isset($data[$strColNm]) &&
                        $data[$strColNm] == $strColVal)
                    {
                        $tabId[] = $id;

                        if(is_null($result)) $result = array();

                        $result[] = array_merge(
                            [$strIdNm => $id],
                            $data
                        );
                    }
                }

                // Get array of criteria data, if required
                if((count($tabId) > 0) && ($boolCriteria))
                {
                    $result = $this->getTabSearchCriteriaData($strData, $tabId);
                }
            }
            // Case config
            else if(isset($tabConfig['config']))
            {
                $result = $this->getTabData($query, $tabConfig);
            }
        }

        if(!is_null($tabInfo))
        {
            $tabInfo['action'] = 'search';
            $tabInfo['query'] = $query;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified formatted data,
     * with specified or current date.
     *
     * @param array $data
     * @param string $strData = null
     * @param null|DateTime $objDate = null
     * @param boolean $boolCreate = true
     * @param boolean $boolUpdate = true
     * @return array
     */
    protected function getFormatDataWithDate(
        array $data,
        $strData = null,
        DateTime $objDate = null,
        $boolCreate = true,
        $boolUpdate = true
    )
    {
        // Init var
        $result = $data;
        $strData = (is_string($strData) ? $strData : '');
        $objDate = (is_null($objDate) ? new DateTime() : $objDate);
        $strDate = $objDate->format('Y-m-d H:i:s');
        $strDataPrefix = (
            ($strData == 'user') ?
                'usr_' :
                (
                    ($strData == 'admin') ?
                        'adm_' :
                        (
                            ($strData == 'right') ?
                                'rgt_' :
                                ''
                        )
                )
        );

        // Set date creation, if required
        if(($strDataPrefix != '') && $boolCreate)
        {
            $result[$strDataPrefix . 'dt_create'] = $strDate;
        }

        // Set date updating, if required
        if($boolUpdate)
        {
            $result[$strDataPrefix . 'dt_update'] = $strDate;
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Save specified data.
     * Configuration can be send, to resolve action.
     * Return true if success, false if an error occurs.
     *
     * @param array $data
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function saveData(array $data, array $tabConfig = null)
    {
        // Init var
        $result = false;

        // Case config
        if(isset($tabConfig['config']))
        {
            $strConfig = $tabConfig['config'];

            if(isset($this->tabConfig[$strConfig]))
            {
                $this->tabConfig[$strConfig] = $data;
                $result = true;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Save specified index array of data.
     * Configuration can be send, to resolve action.
     * Return true if success, false if an error occurs.
     *
     * @param array $tabData
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function saveTabData(array $tabData, array $tabConfig = null)
    {
        // Init var
        $result = false;

        // Run all data
        foreach($tabData as $data)
        {
            $result = $this->saveData($data, $tabConfig) && $result;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function createData(
        array &$data,
        array $tabConfig = null,
        &$id = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;

        // Case data
        if(isset($tabConfig['data']))
        {
            $strData = $tabConfig['data'];
            $strIdNm = $tabConfig['id_nm'];
            $id = uniqid();

            // Update data
            $data = $this->getFormatDataWithDate($data, $strData);

            if(!isset($this->tabData[$strData][$id]))
            {
                unset($data[$strIdNm]);
                $this->tabData[$strData][$id] = $data;
            }

            $result = true;
        }
        // Case config
        else if(isset($tabConfig['config']))
        {
            // Update data
            $data = $this->getFormatDataWithDate($data);

            $result = $this->saveData($data, $tabConfig);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function createTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabId = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = true;
        $tabId = array();

        // Run all data
        foreach($tabData as $key => $data)
        {
            $result = $this->createData($data, $tabConfig, $id) && $result;
            $tabData[$key] = $data;
            $tabId[] = $id;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function updateData(
        array &$data,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;

        // Case data
        if(isset($tabConfig['data']))
        {
            $strData = $tabConfig['data'];
            $strIdNm = $tabConfig['id_nm'];
            $strId = $data[$strIdNm];

            // Update data
            $data = $this->getFormatDataWithDate($data, $strData, null, false);

            if(isset($this->tabData[$strData][$strId]))
            {
                unset($data[$strIdNm]);
                $this->tabData[$strData][$strId] = $data;
            }

            $result = true;
        }
        // Case config
        else if(isset($tabConfig['config']))
        {
            // Update data
            $data = $this->getFormatDataWithDate($data, null, null, false);

            $result = $this->saveData($data, $tabConfig);
        }

        // Set info, if required
        if(!is_null($tabInfo))
        {
            $tabInfo['action'] = 'update';
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function updateTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = true;

        // Run all data
        foreach($tabData as $key => $data)
        {
            $result = $this->updateData($data, $tabConfig) && $result;
            $tabData[$key] = $data;
        }

        // Set info, if required
        if(!is_null($tabInfo))
        {
            $tabInfo['action'] = 'update-multi';
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function deleteData(
        array &$data,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;

        // Case data
        if(isset($tabConfig['data']))
        {
            $strData = $tabConfig['data'];
            $strIdNm = $tabConfig['id_nm'];
            $strId = $data[$strIdNm];

            if(isset($this->tabData[$strData][$strId]))
            {
                unset($this->tabData[$strData][$strId]);
            }

            $result = true;
        }
        // Case config
        else if(isset($tabConfig['config']))
        {
            $strConfig = $tabConfig['config'];

            if(isset($this->tabConfig[$strConfig]))
            {
                $this->tabConfig[$strConfig] = array();
                $result = true;
            }
        }

        // Set info, if required
        if(!is_null($tabInfo))
        {
            $tabInfo['action'] = 'delete';
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function deleteTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = true;

        // Run all data
        foreach($tabData as $key => $data)
        {
            $result = $this->deleteData($data, $tabConfig) && $result;
            $tabData[$key] = $data;
        }

        // Set info, if required
        if(!is_null($tabInfo))
        {
            $tabInfo['action'] = 'delete-multi';
        }

        // Return result
        return $result;
    }





    // Methods transaction
    // ******************************************************************************

    /**
     * Check if process are in current transaction:
     * transaction already start and not closed.
     *
     * @return boolean
     */
    public function checkInTransaction()
    {
        // Return result
        return false;
    }



    /**
     * Start transaction.
     *
     * @return boolean
     */
    public function transactionStart()
    {
        // Return result
        return false;
    }



    /**
     * End transaction and commit.
     *
     * @return boolean
     */
    public function transactionEndCommit()
    {
        // Return result
        return false;
    }



    /**
     * End transaction and cancel.
     *
     * @return boolean
     */
    public function transactionEndRollback()
    {
        // Return result
        return false;
    }



}