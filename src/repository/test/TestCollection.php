<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strRootAppPath . '/src/repository/test/Include.php');

// Use
use liberty_code\model\repository\test\model\right\RightEntity;
use liberty_code\model\repository\test\model\right\RightCollection;
use liberty_code\model\repository\test\model\admin\AdminEntity;
use liberty_code\model\repository\test\model\user\UserEntity;
use liberty_code\model\repository\test\model\user\UserCollection;



// Test create user collection
$objUser1 = new UserEntity(array(
    'login' => 'usr_1',
    'pw' => 'pw_1',
    'nmFirst' => 'Fnm 1',
    'nm' => 'Nm 1 2',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 1',
        'rightCollection' => new RightCollection()
    ))
));
$objUser1->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 1 - Rgt 1',
    'right' => true
)));
$objUser1->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 1 - Rgt 2',
    'right' => false
)));
$objUser1->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 1 - Rgt 3',
    'right' => true
)));

$objUser2 = new UserEntity(array(
    'login' => 'usr_2',
    'pw' => 'pw_2',
    'nmFirst' => 'Fnm 2',
    'nm' => 'Nm 1 2',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 2',
        'rightCollection' => new RightCollection()
    ))
));

$objUser3 = new UserEntity(array(
    'login' => 'usr_3',
    'pw' => 'pw_3',
    'nmFirst' => 'Fnm 3',
    'nm' => 'Nm 3',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 3',
        'rightCollection' => new RightCollection()
    ))
));
$objUser3->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 3 - Rgt 1',
    'right' => true
)));
$objUser3->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 3 - Rgt 2',
    'right' => false
)));
$objUser3->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 3 - Rgt 3',
    'right' => true
)));

$objUser4 = new UserEntity(array(
    'login' => 'usr_4',
    'pw' => 'pw_4',
    'nmFirst' => 'Fnm 4',
    'nm' => 'Nm 4',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 4',
        'rightCollection' => new RightCollection()
    ))
));

$objUser5 = new UserEntity(array(
    'login' => 'usr_5',
    'pw' => 'pw_5',
    'nmFirst' => 'Fnm 5',
    'nm' => 'Nm 5',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 5',
        'rightCollection' => new RightCollection()
    ))
));
$objUser5->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 5 - Rgt 1',
    'right' => true
)));
$objUser5->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 5 - Rgt 2',
    'right' => false
)));
$objUser5->admin->rightCollection->setItem(new RightEntity(array(
    'nm' => 'Adm 5 - Rgt 3',
    'right' => true
)));

$objUserCollection = new UserCollection();
$objUserCollection->setItem($objUser1);
$objUserCollection->setItem($objUser2);
$objUserCollection->setItem($objUser3);
$objUserCollection->setItem($objUser4);
$objUserCollection->setItem($objUser5);

echo('Test user create collection: <br />');
echo('Collection: <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

echo('<br /><br /><br />');



// Test save user collection (create)
$tabInfo = array();
$boolSave = $objUserCollectionRepo->save($objUserCollection, null, $tabInfo);
$tabId = array();
foreach($objUserCollection->getTabKey() as $strKey)
{
    $objUser = $objUserCollection->getItem($strKey);
    $strId = $objUser->id;
    $strAdmId = $objUser->admin->id;
    $tabRghtId = array();
    foreach($objUser->admin->rightCollection->getTabKey() as $strRgtKey)
    {
        $tabRghtId[] = $objUser->admin->rightCollection->getItem($strRgtKey)->id;
    }
    $criteriaAdm = array(
        'criteria' => $strAdmId,
        'sub_entity_criteria' => [
            'rightCollection' => $tabRghtId
        ]
    );
    $criteriaUser = array(
        'criteria' => $strId,
        'sub_entity_criteria' => [
            'admin' => $criteriaAdm
        ]
    );

    $tabId[] = $criteriaUser;
}

echo('Test save user collection (create): <br />');
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Collection: <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');
echo('Db data: <pre>');var_dump($objPersistor->getTabDbData());echo('</pre>');
echo('Db config: <pre>');var_dump($objPersistor->getTabDbConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test search user collection, without sub-repo
echo('Test search user collection (without sub-repo): <br />');

$objUserCollection = new UserCollection();

echo('Collection (previous): <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

$query = array(
    'key' => 'usr_nm',
    'value' => 'Nm 1 2',
    'criteria' => false
);
$tabConfig = array(
    'sub_repository_execution_require' => false,
    'load_execution_config' => [
        'sub_repository_execution_require' => true,
        'sub_repository_execution_config' => [
            'admin' => [
                'sub_repository_execution_require' => false
            ]
        ]
    ]
);
$boolSearch = $objUserCollectionRepo->search($objUserCollection, $query, $tabConfig);

echo('Search: <pre>');var_dump($boolSearch);echo('</pre>');
echo('Collection: <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

echo('<br /><br /><br />');



// Test search user collection, without right sub-repo
echo('Test search user collection (without right sub-repo): <br />');

$objUserCollection = new UserCollection();

echo('Collection (previous): <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

$query = array(
    'key' => 'usr_nm',
    'value' => 'Nm 1 2',
    'criteria' => true
);
$tabConfig = array(
    'sub_repository_execution_require' => true,
    'load_execution_config' => [
        'sub_repository_execution_require' => true,
        'sub_repository_execution_config' => [
            'admin' => [
                'sub_repository_execution_require' => false
            ]
        ]
    ]
);
$tabInfo = array();
$boolSearch = $objUserCollectionRepo->search($objUserCollection, $query, $tabConfig, $tabInfo);

echo('Search: <pre>');var_dump($boolSearch);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Collection: <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

echo('<br /><br /><br />');



// Test search user collection
echo('Test search user collection: <br />');

$objUserCollection = new UserCollection();

echo('Collection (previous): <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

$query = array(
    'key' => 'usr_nm',
    'value' => 'Nm 1 2',
    'criteria' => true
);
$tabConfig = array();
$tabInfo = array();
$boolSearch = $objUserCollectionRepo->search($objUserCollection, $query, $tabConfig, $tabInfo);

echo('Search: <pre>');var_dump($boolSearch);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Collection: <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

echo('<br /><br /><br />');



// Test load user collection
echo('Test load user collection: <br />');

$objUserCollection = new UserCollection();

echo('Collection (previous): <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

$tabInfo = array();
$boolLoad = $objUserCollectionRepo->load($objUserCollection, $tabId, null, $tabInfo);

echo('Load: <pre>');var_dump($boolLoad);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Collection: <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');

echo('<br /><br /><br />');



// Test save user collection (update)
$intCpt = 1;
foreach($objUserCollection->getTabKey() as $strKey)
{
    $objUser = $objUserCollection->getItem($strKey);

    $objUser->login = 'user_' . $intCpt;
    $objUser->nmFirst = 'First Name ' . $intCpt;
    $objUser->nm = 'Name ' . $intCpt;
    $objUser->admin->nm = 'Admin ' . $intCpt;

    $intCpt2 = 1;
    foreach($objUser->admin->rightCollection->getTabKey() as $strRgtKey)
    {
        $objUser->admin->rightCollection->getItem($strRgtKey)->nm = 'Admin ' . $intCpt . ' - Right ' . $intCpt2;
        $intCpt2++;
    }

    $intCpt++;
}

$tabInfo = null;
$boolSave = $objUserCollectionRepo->save($objUserCollection, null, $tabInfo);

echo('Test save user collection (update): <br />');
echo('Save: <pre>');var_dump($boolSave);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Collection: <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');
echo('Db data: <pre>');var_dump($objPersistor->getTabDbData());echo('</pre>');
echo('Db config: <pre>');var_dump($objPersistor->getTabDbConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test remove user collection
//$tabConfig = null;
$tabConfig = array(
    'sub_repository_execution_config' => [
        'admin' => [
            'sub_repository_execution_require' => false
        ]
    ]
);
$tabInfo = array();
$boolRemove = $objUserCollectionRepo->remove($objUserCollection, $tabConfig, $tabInfo);

echo('Test remove user collection: <br />');
echo('Remove: <pre>');var_dump($boolRemove);echo('</pre>');
echo('Info: <pre>');print_r($tabInfo);echo('</pre>');
echo('Collection: <pre>');print_r(getTabCollectionValue($objUserCollection));echo('</pre>');
echo('Db data: <pre>');var_dump($objPersistor->getTabDbData());echo('</pre>');
echo('Db config: <pre>');var_dump($objPersistor->getTabDbConfig());echo('</pre>');

echo('<br /><br /><br />');


