<?php

namespace liberty_code\model\repository\test\model\user;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\repository\test\model\user\UserEntity;



class UserFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => UserEntity::class
        );
    }



}