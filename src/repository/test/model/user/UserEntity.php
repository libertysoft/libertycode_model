<?php

namespace liberty_code\model\repository\test\model\user;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\repository\test\model\admin\AdminEntity;



/**
 * @property string $id
 * @property string $login
 * @property string $pw
 * @property string $nmFirst
 * @property string $nm
 * @property null|AdminEntity $admin
 */
class UserEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabValue = array())
    {
        // Call parent constructor
        parent::__construct($tabValue);
    }





    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $result = $this->getAttributeValue('id');
        if(is_null($result))
        {
            $result = parent::getStrBaseKey();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'id',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'identifier',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'usr_id',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Identifier',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute date creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'dt_create',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'date_create',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'usr_dt_create',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Date create',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute date updating
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'dt_update',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'date_update',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'usr_dt_update',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Date update',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute login
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'login',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'login',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'usr_lg',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Login',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'lg_N'
            ],

            // Attribute password
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'pw',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'password',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'usr_pw',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Password',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'pw_N'
            ],

            // Attribute first name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'nmFirst',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'FirstName',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'usr_fnm',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'First name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'Fnm_N'
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'nm',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Name',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'usr_nm',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Last name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'NM_N'
            ],

            // Attribute admin entity
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'admin',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Admin',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => false,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Administrator',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => new AdminEntity()
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Init var
        $result = false;
        $tabError = array();
        $strAlias = $this->getAttributeAlias($strKey);
        $strNm = (
            is_null($strAlias) ?
                $this->getAttributeName($strKey) :
                $strAlias
        );

        // Check by attribute
        switch($strKey)
        {
            case 'id':
                $result =
                    is_null($value) ||
                    (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'dt_create':
            case 'dt_update':
            case 'nmFirst':
            case 'nm':
                $result =
                    is_null($value) ||
                    (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'login':
            case 'pw':
                $result = is_string($value) && (trim($value) != '');

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'admin':
                $result =
                    is_null($value) ||
                    ($value instanceof AdminEntity);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or an admin entity.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $result = parent::getAttributeValueFormatGet($strKey, $value);

        // Format by attribute
        switch($strKey)
        {
            case 'nmFirst':
                $result = (is_string($value) ? ucfirst($value): $value);
                break;

            case 'nm':
                $result = (is_string($value) ? strtoupper($value): $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Return result
        return parent::getAttributeValueFormatSet($strKey, $value);
    }



}