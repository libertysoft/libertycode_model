<?php

namespace liberty_code\model\repository\test\model\user\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiRepository;

use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\model\repository\test\persistence\MemoryPersistor;
use liberty_code\model\repository\test\model\user\UserEntity;
use liberty_code\model\repository\test\model\admin\repository\AdminRepository;



class UserRepository extends FixMultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Admin repository instance.
     * @var AdminRepository
     */
    protected $objAdminRepo;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PersistorInterface $objPersistor
     * @param AdminRepository $objAdminRepo
     */
    public function __construct(
        PersistorInterface $objPersistor,
        AdminRepository $objAdminRepo
    )
    {
        // Init var
        $this->objAdminRepo = $objAdminRepo;

        // Call parent constructor
        parent::__construct($objPersistor);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return MemoryPersistor::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => UserEntity::class,
            // ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,
            ConstMultiRepository::TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID => 'id',
            'data' => 'user',
            'id_nm' => 'usr_id',
            ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO => [
                'admin' => [
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY => $this->objAdminRepo
                ]
            ],
            ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY => 'usr_id'
        );
    }



}


