<?php

namespace liberty_code\model\repository\test\model\user\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiCollectionRepository;

use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\test\model\admin\AdminCollection;
use liberty_code\model\repository\test\model\admin\repository\AdminCollectionRepository;
use liberty_code\model\repository\test\model\user\UserFactory;
use liberty_code\model\repository\test\model\user\repository\UserRepository;



/**
 * @method null|UserRepository getObjRepository() @inheritdoc .
 */
class UserCollectionRepository extends FixMultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Admin collection instance.
     * @var AdminCollection
     */
    protected $objAdminCollection;



    /**
     * DI: Admin collection repository instance.
     * @var AdminCollectionRepository
     */
    protected $objAdminCollectionRepo;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param UserFactory $objEntityFactory
     * @param UserRepository $objRepository
     * @param AdminCollectionRepository $objAdminCollectionRepo
     * @param AdminCollection $objAdminCollection
     */
    public function __construct(
        UserFactory $objEntityFactory,
        UserRepository $objRepository,
        AdminCollectionRepository $objAdminCollectionRepo,
        AdminCollection $objAdminCollection
    )
    {
        // Init var
        $this->objAdminCollection = $objAdminCollection;
        $this->objAdminCollectionRepo = $objAdminCollectionRepo;

        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return UserFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return UserRepository::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO => [
                'admin' => [
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION_REPOSITORY => $this->objAdminCollectionRepo,
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION => $this->objAdminCollection
                ]
            ],
            ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION => [
                ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE => false
            ]
        );
    }



}