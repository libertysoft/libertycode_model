<?php

namespace liberty_code\model\repository\test\model\user;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use liberty_code\model\repository\test\model\user\UserEntity;



class UserCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return UserEntity::class;
    }



}