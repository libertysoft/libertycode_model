<?php

namespace liberty_code\model\repository\test\model\config\repository;

use liberty_code\model\repository\simple\fix\model\FixSimpleRepository;

use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\test\model\config\Config1Entity;



class Config1Repository extends FixSimpleRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => Config1Entity::class,
            'config' => 'config_1'
        );
    }



}


