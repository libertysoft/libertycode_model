<?php

namespace liberty_code\model\repository\test\model\config;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;



/**
 * @property string $prm_1
 * @property integer $prm_2
 * @property boolean $prm_3
 */
class Config1Entity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabValue = array())
    {
        // Call parent constructor
        parent::__construct($tabValue);
    }





    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Return result
        return 'config_1';
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array(
            // Attribute date updating
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'dt_update',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'date_update',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_dt_update',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Date update',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute parameter 1
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'prm_1',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'parameter_1_1',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_1_1',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Parameter 1 1',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => ''
            ],

            // Attribute parameter 2
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'prm_2',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'parameter_1_2',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_1_2',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Parameter 1 2',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 0
            ],

            // Attribute parameter 3
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'prm_3',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'parameter_1_3',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_1_3',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Parameter 1 3',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => false
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Init var
        $result = false;
        $tabError = array();
        $strAlias = $this->getAttributeAlias($strKey);
        $strNm = (
            is_null($strAlias) ?
                $this->getAttributeName($strKey) :
                $strAlias
        );

        // Check by attribute
        switch($strKey)
        {
            case 'dt_update':
                $result =
                    is_null($value) ||
                    (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case'prm_1':
                $result = is_string($value);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'prm_2':
                $result = is_int($value) && ($value >= 0);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a positive integer.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'prm_3':
                $result = is_bool($value);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a boolean (0 - 1).',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $result = parent::getAttributeValueFormatGet($strKey, $value);

        // Format by attribute
        switch($strKey)
        {
            case'prm_3':
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $result = parent::getAttributeValueFormatSet($strKey, $value);

        // Format by attribute
        switch($strKey)
        {
            case'prm_3':
                $result = (
                    // Case boolean
                    is_bool($value) ? $value : (
                        // Case numeric
                        is_numeric($value) ? (intval($value) != 0) : (
                                // Case else: format impossible
                            $value
                        )
                    )
                );
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case'prm_3':
                $result = (is_bool($value) ? ($value ? 'TR' : 'FL') : $value);
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case'prm_3':
                $result = (($value == 'TR') ? true : (($value == 'FL') ? false : $value));
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



}