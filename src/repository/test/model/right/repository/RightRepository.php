<?php

namespace liberty_code\model\repository\test\model\right\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiRepository;

use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\model\repository\test\persistence\MemoryPersistor;
use liberty_code\model\repository\test\model\right\RightEntity;



class RightRepository extends FixMultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return MemoryPersistor::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => RightEntity::class,
            // ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,
            ConstMultiRepository::TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID => 'id',
            'data' => 'right',
            'id_nm' => 'rgt_id',
            ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY => 'rgt_id'
        );
    }



}


