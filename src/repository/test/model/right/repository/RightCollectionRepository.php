<?php

namespace liberty_code\model\repository\test\model\right\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiCollectionRepository;

use liberty_code\model\repository\test\model\right\RightFactory;
use liberty_code\model\repository\test\model\right\repository\RightRepository;



/**
 * @method null|RightRepository getObjRepository() @inheritdoc .
 */
class RightCollectionRepository extends FixMultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RightFactory $objEntityFactory
     * @param RightRepository $objRepository
     */
    public function __construct(
        RightFactory $objEntityFactory,
        RightRepository $objRepository
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return RightFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return RightRepository::class;
    }



}