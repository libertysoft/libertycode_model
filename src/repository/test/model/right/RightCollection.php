<?php

namespace liberty_code\model\repository\test\model\right;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use liberty_code\model\repository\test\model\right\RightEntity;



class RightCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return RightEntity::class;
    }



}