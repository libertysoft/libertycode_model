<?php

namespace liberty_code\model\repository\test\model\right;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;



/**
 * @property string $id
 * @property string $admId
 * @property string $nm
 * @property boolean $right
 */
class RightEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabValue = array())
    {
        // Call parent constructor
        parent::__construct($tabValue);
    }





    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $result = $this->getAttributeValue('id');
        if(is_null($result))
        {
            $result = parent::getStrBaseKey();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'id',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'identifier',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'rgt_id',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Identifier',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute date creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'dt_create',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'date_create',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'rgt_dt_create',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Date create',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute date updating
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'dt_update',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'date_update',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'rgt_dt_update',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Date update',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute admin id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'admId',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'admin identifier',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'rgt_adm_id',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Admin identifier',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'nm',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Name',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'rgt_nm',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'right'
            ],

            // Attribute right
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'right',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Right',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'rgt_right',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Right',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => true
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Init var
        $result = false;
        $tabError = array();
        $strAlias = $this->getAttributeAlias($strKey);
        $strNm = (
            is_null($strAlias) ?
                $this->getAttributeName($strKey) :
                $strAlias
        );

        // Check by attribute
        switch($strKey)
        {
            case 'id':
            case 'admId':
                $result =
                    is_null($value) ||
                    (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'dt_create':
            case 'dt_update':
                $result =
                    is_null($value) ||
                    (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'nm':
                $result = (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'right':
                $result = is_bool($value);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a boolean.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $result = parent::getAttributeValueFormatGet($strKey, $value);

        // Format by attribute
        switch($strKey)
        {
            case 'nm':
                $result = (is_string($value) ? strtoupper($value): $value);
                break;

            case 'right':
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $result = parent::getAttributeValueFormatSet($strKey, $value);

        // Format by attribute
        switch($strKey)
        {
            case 'right':
                $result = (
                    // Case boolean
                    is_bool($value) ? $value : (
                        // Case numeric
                        is_numeric($value) ? intval($value) != 0 : (
                            // Case else: format impossible
                            $value
                        )
                    )
                );

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'right':
                $result = (is_bool($value) ? ($value ? 'TR' : 'FL') : $value);
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'right':
                $result = (($value == 'TR') ? true : (($value == 'FL') ? false : $value));
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



}