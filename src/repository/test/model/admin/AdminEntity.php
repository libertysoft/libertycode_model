<?php

namespace liberty_code\model\repository\test\model\admin;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\repository\test\model\right\RightCollection;



/**
 * @property string $id
 * @property string $nm
 * @property RightCollection $rightCollection
 */
class AdminEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(array $tabValue = array())
    {
        // Call parent constructor
        parent::__construct($tabValue);
    }





    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $result = $this->getAttributeValue('id');
        if(is_null($result))
        {
            $result = parent::getStrBaseKey();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'id',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'identifier',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'adm_id',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => function(SaveConfigEntity $objEntity, $strKey) {
                    echo('Test admin entity save callable: <br />');
                    echo('Class: <pre>');var_dump(get_class($objEntity));echo('</pre>');
                    echo('key: <pre>');var_dump($strKey);echo('</pre>');
                    echo('<br />');

                    return true;
                },
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Identifier',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute date creation
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'dt_create',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'date_create',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'adm_dt_create',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Date create',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute date updating
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'dt_update',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'date_update',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'adm_dt_update',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => function(){return false;},
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => function(){return true;},
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Date update',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'nm',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Name',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'adm_nm',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Name',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'admin'
            ],

            // Attribute right collection
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'rightCollection',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME => 'Rights',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => function(){return false;},
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS => 'Rights collection',
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => new RightCollection()
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Init var
        $result = false;
        $tabError = array();
        $strAlias = $this->getAttributeAlias($strKey);
        $strNm = (
            is_null($strAlias) ?
                $this->getAttributeName($strKey) :
                $strAlias
        );

        // Check by attribute
        switch($strKey)
        {
            case 'id':
                $result =
                    is_null($value) ||
                    (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'dt_create':
            case 'dt_update':
                $result =
                    is_null($value) ||
                    (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'nm':
                $result = (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a valid string.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'rightCollection':
                $result =
                    is_null($value) ||
                    ($value instanceof RightCollection);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be null or a right collection.',
                        $strNm,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $result = parent::getAttributeValueFormatGet($strKey, $value);

        // Format by attribute
        switch($strKey)
        {
            case 'nm':
                $result = (is_string($value) ? strtoupper($value): $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Return result
        return parent::getAttributeValueFormatSet($strKey, $value);
    }



}