<?php

namespace liberty_code\model\repository\test\model\admin;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use liberty_code\model\repository\test\model\admin\AdminEntity;



class AdminCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AdminEntity::class;
    }



}