<?php

namespace liberty_code\model\repository\test\model\admin;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\model\entity\factory\library\ConstEntityFactory;
use liberty_code\model\repository\test\model\admin\AdminEntity;



class AdminFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AdminEntity::class
        );
    }



}