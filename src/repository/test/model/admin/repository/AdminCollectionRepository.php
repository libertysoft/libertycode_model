<?php

namespace liberty_code\model\repository\test\model\admin\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiCollectionRepository;

use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\test\model\right\RightCollection;
use liberty_code\model\repository\test\model\right\repository\RightCollectionRepository;
use liberty_code\model\repository\test\model\admin\AdminFactory;
use liberty_code\model\repository\test\model\admin\repository\AdminRepository;



/**
 * @method null|AdminRepository getObjRepository() @inheritdoc .
 */
class AdminCollectionRepository extends FixMultiCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();


    /**
     * DI: Right collection instance.
     * @var RightCollection
     */
    protected $objRightCollection;



    /**
     * DI: Right collection repository instance.
     * @var RightCollectionRepository
     */
    protected $objRightCollectionRepo;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AdminFactory $objEntityFactory
     * @param AdminRepository $objRepository
     * @param RightCollectionRepository $objRightCollectionRepo
     * @param RightCollection $objRightCollection
     */
    public function __construct(
        AdminFactory $objEntityFactory,
        AdminRepository $objRepository,
        RightCollectionRepository $objRightCollectionRepo,
        RightCollection $objRightCollection
    )
    {
        // Init var
        $this->objRightCollection = $objRightCollection;
        $this->objRightCollectionRepo = $objRightCollectionRepo;

        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return AdminFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return AdminRepository::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            //*
            ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO => [
                'rightCollection' => [
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION_REPOSITORY => $this->objRightCollectionRepo,
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION => $this->objRightCollection
                ]
            ],
            //*/
            //*
            ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION => [
                ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE => false
            ]
            //*/
        );
    }



}