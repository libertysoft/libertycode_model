<?php

namespace liberty_code\model\repository\test\model\admin\repository;

use liberty_code\model\repository\multi\fix\model\FixMultiRepository;

use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\multi\library\ConstMultiRepository;
use liberty_code\model\repository\test\persistence\MemoryPersistor;
use liberty_code\model\repository\test\model\admin\AdminEntity;
use liberty_code\model\repository\test\model\right\repository\RightCollectionRepository;



class AdminRepository extends FixMultiRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Right collection repository instance.
     * @var RightCollectionRepository
     */
    protected $objRightCollectionRepo;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param PersistorInterface $objPersistor
     * @param RightCollectionRepository $objRightCollectionRepo
     */
    public function __construct(
        PersistorInterface $objPersistor,
        RightCollectionRepository $objRightCollectionRepo
    )
    {
        // Init var
        $this->objRightCollectionRepo = $objRightCollectionRepo;

        // Call parent constructor
        parent::__construct($objPersistor);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixPersistorClassPath()
    {
        // Return result
        return MemoryPersistor::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AdminEntity::class,
            // ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,
            ConstMultiRepository::TAB_CONFIG_KEY_ATTRIBUTE_KEY_ID => 'id',
            'data' => 'admin',
            'id_nm' => 'adm_id',
            ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO => [
                'rightCollection' => [
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY => $this->objRightCollectionRepo,
                    ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING => [
                        'admId' => 'id'
                    ]
                ]
            ],
            ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY => 'adm_id'
        );
    }



}


