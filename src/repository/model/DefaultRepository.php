<?php
/**
 * Description :
 * This class allows to define default repository class.
 * Can be consider is base of all repository type.
 * Default repository allows to specify parts of configuration (entity type):
 * [
 *     entity_class_path(required):"class path",
 *
 *     load_empty_enable_require(optional: got false if not found): true / false,
 *
 *     transaction_require(optional: got true if not found): true / false
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\model\repository\api\RepositoryInterface;

use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\persistence\library\ToolBoxPersistor;
use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\exception\PersistorInvalidFormatException;
use liberty_code\model\repository\exception\ConfigInvalidFormatException;
use liberty_code\model\repository\exception\EntityInvalidFormatException;
use liberty_code\model\repository\exception\ExecConfigInvalidFormatException;



/**
 * @method void setTabConfig(array $tabConfig) Set config array.
 */
abstract class DefaultRepository extends FixBean implements RepositoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig = null
     * @param PersistorInterface $objPersistor = null
     */
    public function __construct(array $tabConfig = null, PersistorInterface $objPersistor = null)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init persistor if required
        if(!is_null($objPersistor))
        {
            $this->setPersistor($objPersistor);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
		if(!$this->beanExists(ConstRepository::DATA_KEY_DEFAULT_PERSISTOR))
        {
            $this->__beanTabData[ConstRepository::DATA_KEY_DEFAULT_PERSISTOR] = null;
        }

        if(!$this->beanExists(ConstRepository::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstRepository::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRepository::DATA_KEY_DEFAULT_PERSISTOR,
            ConstRepository::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_PERSISTOR:
                    PersistorInvalidFormatException::setCheck($value);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkValidEntity($entity)
    {
        // Init var
        $strEntityClassPath = $this->getStrEntityClassPath();
        $result =
            is_object($entity) &&
            ($entity instanceof SaveEntityInterface) &&
            ($entity instanceof $strEntityClassPath);

        // Return result
        return $result;
    }



    /**
     * Set check specified entity is valid.
     *
     * @param mixed $entity
     * @throws EntityInvalidFormatException
     */
    protected function setCheckValidEntity($entity)
    {
        if(!$this->checkValidEntity($entity))
        {
            throw new EntityInvalidFormatException($entity, $this->getStrEntityClassPath());
        }
    }



    /**
     * Check load empty enable required.
     *
     * @return boolean
     */
    public function checkLoadEmptyEnableRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstRepository::TAB_CONFIG_KEY_LOAD_EMPTY_ENABLE_REQUIRE, $tabConfig) &&
            (intval($tabConfig[ConstRepository::TAB_CONFIG_KEY_LOAD_EMPTY_ENABLE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check transaction required.
     *
     * @return boolean
     */
    public function checkTransactionRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $objPersistor = $this->getObjPersistor();
        $result = (
            (!is_null($objPersistor)) &&
            (
                (!isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE])) ||
                (intval($tabConfig[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

	/**
     * @inheritdoc
     */
    public function getObjPersistor()
    {
        // Return result
        return $this->beanGet(ConstRepository::DATA_KEY_DEFAULT_PERSISTOR);
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstRepository::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * @inheritdoc
     */
    public function getStrEntityClassPath()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $tabConfig[ConstRepository::TAB_CONFIG_KEY_ENTITY_CLASS_PATH];

        // Return result
        return $result;
    }



    /**
     * Overwrite it to set specific feature.
     *
     * @inheritdoc
     * @throws EntityInvalidFormatException
     */
    public function getTabData(SaveEntityInterface $objEntity)
    {
        // Set check entity
        $this->setCheckValidEntity($objEntity);

        // Return result
        return $objEntity->getTabDataSave();
    }





    // Methods setters
    // ******************************************************************************
	
	/**
	 * @inheritdoc
	 */
    public function setPersistor(PersistorInterface $objPersistor)
	{
		$this->beanSet(ConstRepository::DATA_KEY_DEFAULT_PERSISTOR, $objPersistor);
	}



    /**
     * Overwrite it to set specific feature.
     *
     * @inheritdoc
     */
    public function setData(SaveEntityInterface $objEntity, array $tabData)
    {
        $objEntity->hydrateSave($tabData, true);
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     persistor_add_config(optional): [
     *         // Optional additional persistor configuration
     *         @see PersistorInterface::deleteData() configuration array format
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     persistor_info(optional): [
     *         @see PersistorInterface::deleteData() return information array format
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws EntityInvalidFormatException
     * @throws ExecConfigInvalidFormatException
     */
    public function remove(
        SaveEntityInterface $objEntity,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $result = $this->executeTransaction(
            function() use ($objEntity, $tabConfig, &$tabInfo)
            {
                // Set check arguments
                $this->setCheckValidEntity($objEntity);

                // Init var
                $objPersistor = $this->getObjPersistor();

                // Get data from entity
                $tabData = $this->getTabData($objEntity);
                $tabOriginalData = $tabData;

                // Remove data
                $tabPersistorInfo = (is_null($tabInfo) ? null : array());
                $result = $objPersistor->deleteData(
                    $tabData,
                    $this->getTabConfigPersistDelete($objEntity, $tabConfig),
                    $tabPersistorInfo
                );

                // Register info, if required
                if(
                    (!is_null($tabPersistorInfo)) &&
                    (count($tabPersistorInfo) > 0)
                )
                {
                    $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO] = $tabPersistorInfo;
                }

                // Update entity, if required
                if($result)
                {
                    // Set data, if required
                    if($tabData !== $tabOriginalData)
                    {
                        $this->setData($objEntity, $tabData);
                    }

                    $objEntity->setIsNew(true);
                }

                // Return result
                return $result;
            },
            function($result) {return $result;}
        );

        // Return result
        return $result;
    }





    // Methods persistence
    // ******************************************************************************

    /**
     * Get configuration array for persistence action Get.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see load() configuration array format.
     *
     * @param SaveEntityInterface $objEntity = null
     * @param array $tabConfig = null
     * @return array
     * @throws ExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistGet(
        SaveEntityInterface $objEntity = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration array for persistence action Create.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see save() configuration array format.
     *
     * @param SaveEntityInterface $objEntity = null
     * @param array $tabConfig = null
     * @return array
     * @throws ExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistCreate(
        SaveEntityInterface $objEntity = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration array for persistence action Update.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see save() configuration array format.
     *
     * @param SaveEntityInterface $objEntity = null
     * @param array $tabConfig = null
     * @return array
     * @throws ExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistUpdate(
        SaveEntityInterface $objEntity = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration array for persistence action delete.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see remove() configuration array format.
     *
     * @param SaveEntityInterface $objEntity = null
     * @param array $tabConfig = null
     * @return array
     * @throws ExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistDelete(
        SaveEntityInterface $objEntity = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified transaction process,
     * in transaction, if possible,
     * and return result.
     *
     * Process callback format:
     * @see ToolBoxPersistor::executeTransaction() process callback format.
     *
     * Check result is valid callback format:
     * @see ToolBoxPersistor::executeTransaction() check result is valid callback format.
     *
     * @param callable $callableProcess
     * @param callable $callableCheckResultIsValid = null
     * @return false|mixed
     */
    protected function executeTransaction(
        callable $callableProcess,
        callable $callableCheckResultIsValid = null
    )
    {
        // Init var
        $objPersistor = $this->getObjPersistor();
        $result = (
            ($this->checkTransactionRequired()) ?
                ToolBoxPersistor::executeTransaction(
                    $this->getObjPersistor(),
                    $callableProcess,
                    $callableCheckResultIsValid
                ) :
                (
                    (!is_null($objPersistor)) ?
                        $callableProcess() :
                        false
                )
        );

        // Return result
        return $result;
    }



}