<?php
/**
 * Description :
 * This class allows to define default collection repository class.
 * Can be consider is base of all collection repository type.
 *
 * Default collection repository allows to specify parts of configuration:
 * [
 *     entity_factory_execution_config(optional): [
 *         @see EntityFactoryInterface::getObjEntity() configuration array format
 *     ],
 *
 *     transaction_require(optional: got true if not found): true / false
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\model\repository\api\CollectionRepositoryInterface;

use liberty_code\library\table\library\ConstTable;
use liberty_code\library\table\library\ToolBoxTable;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\entity\factory\api\EntityFactoryInterface;
use liberty_code\model\persistence\library\ToolBoxPersistor;
use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\library\ToolBoxEntityRepository;
use liberty_code\model\repository\api\RepositoryInterface;
use liberty_code\model\repository\exception\EntityInvalidFormatException;
use liberty_code\model\repository\exception\CollectionEntityFactoryInvalidFormatException;
use liberty_code\model\repository\exception\CollectionRepositoryInvalidFormatException;
use liberty_code\model\repository\exception\CollectionConfigInvalidFormatException;
use liberty_code\model\repository\exception\CollectionInvalidFormatException;
use liberty_code\model\repository\exception\CollectionExecConfigInvalidFormatException;



/**
 * @method void setTabConfig(array $tabConfig) Set config array.
 */
abstract class DefaultCollectionRepository extends FixBean implements CollectionRepositoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param EntityFactoryInterface $objEntityFactory = null
     * @param RepositoryInterface $objRepository = null
     * @param array $tabConfig = null
     */
    public function __construct(
        EntityFactoryInterface $objEntityFactory = null,
        RepositoryInterface $objRepository = null,
        array $tabConfig = null
    )
    {
        // Call parent constructor
        parent::__construct();

        // Init entity factory, if required
        if(!is_null($objEntityFactory))
        {
            $this->setEntityFactory($objEntityFactory);
        }

        // Init repository, if required
        if(!is_null($objRepository))
        {
            $this->setRepository($objRepository);
        }

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRepository::DATA_KEY_DEFAULT_COLLECTION_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstRepository::DATA_KEY_DEFAULT_COLLECTION_ENTITY_FACTORY] = null;
        }

		if(!$this->beanExists(ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY))
        {
            $this->__beanTabData[ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY] = null;
        }

        if(!$this->beanExists(ConstRepository::DATA_KEY_DEFAULT_COLLECTION_CONFIG))
        {
            $this->__beanTabData[ConstRepository::DATA_KEY_DEFAULT_COLLECTION_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRepository::DATA_KEY_DEFAULT_COLLECTION_ENTITY_FACTORY,
            ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY,
            ConstRepository::DATA_KEY_DEFAULT_COLLECTION_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_ENTITY_FACTORY:
                    CollectionEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY:
                    CollectionRepositoryInvalidFormatException::setCheck($value);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_CONFIG:
                    CollectionConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkValidCollection($collection)
    {
        // Init var
        $result = false;
        $objRepository = $this->getObjRepository();

        // Check info valid
        if(
            (!is_null($objRepository)) &&
            ($collection instanceof EntityCollectionInterface)
        )
        {
            /** @var EntityCollectionInterface $collection */

            // Run all entities
            $result = true;
            $tabKey = $collection->getTabKey();
            for($intCpt = 0; $result && ($intCpt < count($tabKey)); $intCpt++)
            {
                // Get info
                $strKey = $tabKey[$intCpt];
                $objEntity = $collection->getItem($strKey);

                // Check entity
                $boolValidEntity = $objRepository->checkValidEntity($objEntity);

                // Set result
                $result = $result && $boolValidEntity;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Set check specified entity collection is valid.
     *
     * @param mixed $collection
     * @throws CollectionInvalidFormatException
     */
    protected function setCheckValidCollection($collection)
    {
        if(!$this->checkValidCollection($collection))
        {
            throw new CollectionInvalidFormatException($collection);
        }
    }



    /**
     * Check transaction required.
     *
     * @return boolean
     */
    public function checkTransactionRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $objPersistor = $this->getObjPersistor();
        $result = (
            (!is_null($objPersistor)) &&
            (
                (!isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE])) ||
                (intval($tabConfig[ConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjEntityFactory()
    {
        // Return result
        return $this->beanGet(ConstRepository::DATA_KEY_DEFAULT_COLLECTION_ENTITY_FACTORY);
    }



	/**
     * @inheritdoc
     */
    public function getObjRepository()
    {
        // Return result
        return $this->beanGet(ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY);
    }



    /**
     * Get persistor object.
     *
     * @return null|PersistorInterface
     */
    protected function getObjPersistor()
    {
        // Init var
        $result = null;
        $objRepository = $this->getObjRepository();

        // Register persistor, if required
        if(!is_null($objRepository))
        {
            $result = $objRepository->getObjPersistor();
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig($boolMerge = true)
    {
        // Init var
        $result = $this->beanGet(ConstRepository::DATA_KEY_DEFAULT_COLLECTION_CONFIG);
        $objRepository = $this->getObjRepository();
        $boolMerge = (is_bool($boolMerge) ? $boolMerge : true);

        // Merge configuration repository, if required
        if(($boolMerge) && (!is_null($objRepository)))
        {
            $result = ToolBoxTable::getTabMerge($objRepository->getTabConfig(), $result, ConstTable::OPTION_MERGE_OVERWRITE);
        }

        // Return result
        return $result;
    }



    /**
     * Get entity factory execution configuration array.
     *
     * @return null|array
     */
    protected function getTabEntityFactoryExecConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_CONFIG_KEY_ENTITY_FACTORY_EXECUTION_CONFIG]) ?
                $tabConfig[ConstRepository::TAB_CONFIG_KEY_ENTITY_FACTORY_EXECUTION_CONFIG] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get new entity object engine,
     * from specified data.
     * Overwrite it to implement specific feature.
     *
     * Data array format:
     * @see getObjEntityNew() data array format.
     *
     *  @param array $tabData
     * @return SaveEntityInterface
     */
    protected function getObjEntityNewEngine(array $tabData)
    {
        // Init var
        $objEntityFactory = $this->getObjEntityFactory();
        $tabConfig = $this->getTabEntityFactoryExecConfig();
        $result = $objEntityFactory->getObjEntity(array(), $tabConfig);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjEntityNew(array $tabData)
    {
        // Init var
        $result = null;
        $objEntityFactory = $this->getObjEntityFactory();
        $objRepository = $this->getObjRepository();

        // Get new instance, if required
        if(
            (!is_null($objEntityFactory)) &&
            (!is_null($objRepository))
        )
        {
            $result = $this->getObjEntityNewEngine($tabData);

            // Check entity is valid
            if(!$objRepository->checkValidEntity($result))
            {
                throw new CollectionEntityFactoryInvalidFormatException($objEntityFactory);
            }

            /** @var SaveEntityInterface $result */
            $objRepository->setData($result, $tabData);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws CollectionInvalidFormatException
     * @throws EntityInvalidFormatException
     */
    public function getTabData(EntityCollectionInterface $objCollection)
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);

        // Init var
        $objRepository = $this->getObjRepository();
        $result = ToolBoxEntityRepository::getTabEntityCollectionDataSave(
            $objCollection,
            function(SaveEntityInterface $objEntity) use ($objRepository)
            {
                // Get entity data
                return $objRepository->getTabData($objEntity);
            }
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setEntityFactory(EntityFactoryInterface $objEntityFactory)
    {
        $this->beanSet(ConstRepository::DATA_KEY_DEFAULT_COLLECTION_ENTITY_FACTORY, $objEntityFactory);
    }



	/**
	 * @inheritdoc
	 */
    public function setRepository(RepositoryInterface $objRepository)
	{
		$this->beanSet(ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY, $objRepository);
	}



    /**
     * @inheritdoc
     */
    public function setTabData(
        EntityCollectionInterface $objCollection,
        array $tabData,
        $boolIsNew = false,
        $callableEntity = null
    )
    {
        // Init var
        $boolIsNew = (is_bool($boolIsNew) ? $boolIsNew : false);
        $callableEntity = (is_callable($callableEntity) ? $callableEntity : null);

        // Hydrate collection
        ToolBoxEntityRepository::hydrateSaveEntityCollection(
            $objCollection,
            function(array $tabData) use ($boolIsNew, $callableEntity)
            {
                // Get new hydrated entity
                $result = $this->getObjEntityNew($tabData);

                // Format entity, if required
                if(!is_null($result))
                {
                    $result->setIsNew($boolIsNew);

                    if(!is_null($callableEntity))
                    {
                        $callableEntity($result);
                    }
                }

                // Return result
                return $result;
            },
            $tabData
        );
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     persistor_add_config(optional): [
     *         // Optional additional persistor configuration
     *         @see PersistorInterface::getTabSearchData() configuration array format
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     persistor_info(optional): [
     *         @see PersistorInterface::getTabSearchData() return information array format
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws CollectionInvalidFormatException
     * @throws CollectionExecConfigInvalidFormatException
     */
    public function search(
        EntityCollectionInterface $objCollection,
        $query,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Set check arguments
        $this->setCheckValidCollection($objCollection);

        // Init var
        $result = false;
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $objPersistor = $this->getObjPersistor();

        if(!is_null($objPersistor))
        {
            // Get data from persistence
            $tabPersistorInfo = (is_null($tabInfo) ? null : array());
            $tabData = $objPersistor->getTabSearchData(
                $query,
                $this->getTabConfigPersistSearch($objCollection, $tabConfig),
                $tabPersistorInfo
            );

            // Register info, if required
            if(
                (!is_null($tabPersistorInfo)) &&
                (count($tabPersistorInfo) > 0)
            )
            {
                $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO] = $tabPersistorInfo;
            }

            // Check data valid
            if(is_array($tabData))
            {
                $result = true;

                // Set entities with data
                $this->setTabData($objCollection, $tabData, false);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     persistor_add_config(optional): [
     *         // Optional additional persistor configuration
     *         @see PersistorInterface::deleteTabData() configuration array format
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     persistor_info(optional): [
     *         @see PersistorInterface::deleteTabData() return information array format
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws CollectionInvalidFormatException
     * @throws CollectionExecConfigInvalidFormatException
     */
    public function remove(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $result = $this->executeTransaction(
            function() use ($objCollection, $tabConfig, &$tabInfo)
            {
                // Set check arguments
                $this->setCheckValidCollection($objCollection);

                // Init var
                $result = false;
                $objPersistor = $this->getObjPersistor();
                $objRepository = $this->getObjRepository();
                if(!is_null($objRepository))
                {
                    // Get data from entity
                    $tabData = $this->getTabData($objCollection);
                    $tabOriginalData = $tabData;

                    // Remove data
                    $tabPersistorInfo = (is_null($tabInfo) ? null : array());
                    $result = $objPersistor->deleteTabData(
                        $tabData,
                        $this->getTabConfigPersistDelete($objCollection, $tabConfig),
                        $tabPersistorInfo
                    );

                    // Register info, if required
                    if(
                        (!is_null($tabPersistorInfo)) &&
                        (count($tabPersistorInfo) > 0)
                    )
                    {
                        $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO] = $tabPersistorInfo;
                    }

                    // Update entities, if required
                    if($result)
                    {
                        // Run each entity and update it, if required
                        $tabEntityKey = $objCollection->getTabKey();
                        $tabData = array_filter(
                            array_values($tabData),
                            function($data) {return is_array($data);}
                        );
                        $boolTabDataValid = (count($tabEntityKey) == count($tabData));
                        for($intCpt = 0; ($intCpt < count($tabEntityKey)); $intCpt++)
                        {
                            // Get info
                            $strKey = $tabEntityKey[$intCpt];
                            /** @var SaveEntityInterface $objEntity */
                            $objEntity = $objCollection->getItem($strKey);

                            // Set data, if required
                            if($boolTabDataValid)
                            {
                                $data = $tabData[$intCpt];
                                $originalData = $tabOriginalData[$intCpt];
                                if($data !== $originalData)
                                {
                                    $objRepository->setData($objEntity, $data);
                                }
                            }

                            $objEntity->setIsNew(true);
                        }
                    }
                }

                // Return result
                return $result;
            },
            function($result) {return $result;}
        );

        // Return result
        return $result;
    }





    // Methods persistence
    // ******************************************************************************

    /**
     * Get configuration array for persistence action Get.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see load() configuration array format.
     *
     * @param EntityCollectionInterface $objCollection = null
     * @param array $tabConfig = null
     * @return array
     * @throws CollectionExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistGet(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        CollectionExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration array for persistence action Search.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see search() configuration array format.
     *
     * @param EntityCollectionInterface $objCollection = null
     * @param array $tabConfig = null
     * @return array
     * @throws CollectionExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistSearch(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        CollectionExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration array for persistence action Create.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see save() configuration array format.
     *
     * @param EntityCollectionInterface $objCollection = null
     * @param array $tabConfig = null
     * @return array
     * @throws CollectionExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistCreate(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        CollectionExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration array for persistence action Update.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see save() configuration array format.
     *
     * @param EntityCollectionInterface $objCollection = null
     * @param array $tabConfig = null
     * @return array
     * @throws CollectionExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistUpdate(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        CollectionExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get configuration array for persistence action delete.
     * Overwrite it to implement specific configuration,
     * to send to this persistence action.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see remove() configuration array format.
     *
     * @param EntityCollectionInterface $objCollection = null
     * @param array $tabConfig = null
     * @return array
     * @throws CollectionExecConfigInvalidFormatException
     */
    protected function getTabConfigPersistDelete(
        EntityCollectionInterface $objCollection = null,
        array $tabConfig = null
    )
    {
        // Set check arguments
        CollectionExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG]) ?
                array_merge(
                    $tabConfig[ConstRepository::TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG],
                    $result
                ) :
                $result
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified transaction process,
     * in transaction, if possible,
     * and return result.
     *
     * Process callback format:
     * @see ToolBoxPersistor::executeTransaction() process callback format.
     *
     * Check result is valid callback format:
     * @see ToolBoxPersistor::executeTransaction() check result is valid callback format.
     *
     * @param callable $callableProcess
     * @param callable $callableCheckResultIsValid = null
     * @return false|mixed
     */
    protected function executeTransaction(
        callable $callableProcess,
        callable $callableCheckResultIsValid = null
    )
    {
        // Init var
        $objPersistor = $this->getObjPersistor();
        $result = (
            ($this->checkTransactionRequired()) ?
                ToolBoxPersistor::executeTransaction(
                    $this->getObjPersistor(),
                    $callableProcess,
                    $callableCheckResultIsValid
                ) :
                (
                    (!is_null($objPersistor)) ?
                        $callableProcess() :
                        false
                )
        );

        // Return result
        return $result;
    }



}