<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\entity\api\ItemEntityInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\api\CollectionRepositoryInterface;



class ToolBoxEntityRepository extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified entity collection,
     * from specified index array of saved data array.
     *
     * Data array format:
     * @see CollectionRepositoryInterface::setTabData() data array format.
     *
     * Get new entity callable format:
     * mixed|SaveEntityInterface function(array $tabData):
     * Get new entity,
     * from specified saved data array.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param callable $callableGetObjEntityNew
     * @param array $tabData
     * @return boolean
     */
    public static function hydrateSaveEntityCollection(
        EntityCollectionInterface $objEntityCollection,
        callable $callableGetObjEntityNew,
        array $tabData
    )
    {
        // Init var
        $result = true;

        // Run each saved data
        foreach($tabData as $data)
        {
            // Get entity and register it, if required
            $boolHydrate = false;
            if(is_array($data) && (count($data) > 0))
            {
                // Get new hydrated entity
                $objEntity = $callableGetObjEntityNew($data);

                // Register hydrated entity, if required
                if(
                    is_object($objEntity) &&
                    ($objEntity instanceof ItemEntityInterface) &&
                    ($objEntity instanceof SaveEntityInterface)
                )
                {
                    // Register entity
                    $objEntityCollection->setItem($objEntity);
                    $boolHydrate = true;
                }
            }

            // Check if hydrated
            $result = $result && $boolHydrate;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of data array to save,
     * from specified entity collection.
     *
     * Get data array callable format:
     * mixed|array function(SaveEntityInterface $objEntity):
     * Get specified data array to save,
     * from specified entity.
     *
     * Return format:
     * @see CollectionRepositoryInterface::getTabData() return array format.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param callable $callableGetTabData
     * @return array
     */
    public static function getTabEntityCollectionDataSave(
        EntityCollectionInterface $objEntityCollection,
        callable $callableGetTabData
    )
    {
        // Init var
        $result = array();

        // Run each entity
        foreach($objEntityCollection->getTabKey() as $strKey)
        {
            // Get data to save
            $objEntity = $objEntityCollection->getItem($strKey);
            $tabData = (
                (
                    is_object($objEntity) &&
                    ($objEntity instanceof SaveEntityInterface)
                ) ?
                    $callableGetTabData($objEntity) :
                    null
            );

            // Register data to save, if required
            if(is_array($tabData))
            {
                $result[] = $tabData;
            }
        }

        // Return result
        return $result;
    }



}