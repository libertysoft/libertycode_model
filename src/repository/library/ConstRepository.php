<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\library;



class ConstRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PERSISTOR = 'objPersistor';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_COLLECTION_ENTITY_FACTORY = 'objEntityFactory';
    const DATA_KEY_DEFAULT_COLLECTION_REPOSITORY = 'objRepository';
    const DATA_KEY_DEFAULT_COLLECTION_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_ENTITY_CLASS_PATH = 'entity_class_path';
    const TAB_CONFIG_KEY_LOAD_EMPTY_ENABLE_REQUIRE = 'load_empty_enable_require';
    const TAB_CONFIG_KEY_TRANSACTION_REQUIRE = 'transaction_require';
    const TAB_CONFIG_KEY_ENTITY_FACTORY_EXECUTION_CONFIG = 'entity_factory_execution_config';

    // Execution configuration
    const TAB_EXEC_CONFIG_KEY_PERSISTOR_ADD_CONFIG = 'persistor_add_config';

    // Information configuration
    const TAB_INFO_KEY_PERSISTOR_INFO = 'persistor_info';


	
    // Exception message constants
    const EXCEPT_MSG_PERSISTOR_INVALID_FORMAT = 'Following persistor "%1$s" invalid! It must be a persistor object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the default repository configuration standard.';
    const EXCEPT_MSG_ENTITY_INVALID_FORMAT = 'Following entity "%1$s" invalid! The entity must have a valid type%2$s.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the default repository execution configuration standard.';
    const EXCEPT_MSG_COLLECTION_ENTITY_FACTORY_INVALID_FORMAT = 'Following factory "%1$s" invalid! It must be a valid entity factory object.';
    const EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT = 'Following repository "%1$s" invalid! It must be a repository object.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the default collection repository configuration standard.';
    const EXCEPT_MSG_COLLECTION_INVALID_FORMAT = 'Following collection "%1$s" invalid! The collection must contain valid entities.';
    const EXCEPT_MSG_COLLECTION_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the default collection repository execution configuration standard.';



}