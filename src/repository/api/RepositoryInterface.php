<?php
/**
 * Description :
 * This class allows to describe behavior of repository class.
 * Repository is item, containing all information to prepare data from entity, to save in persistence.
 * Repository is specific for one entity type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\api;

use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;



interface RepositoryInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified entity is valid.
     *
     * @param mixed $entity
     * @return boolean
     */
    public function checkValidEntity($entity);





    // Methods getters
    // ******************************************************************************

    /**
     * Get persistor object.
     *
     * @return null|PersistorInterface
     */
    public function getObjPersistor();



    /**
     * Get config array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get class path of items.
     *
     * @return string
     */
    public function getStrEntityClassPath();



    /**
     * Get data from specified entity.
     *
     * @param SaveEntityInterface $objEntity
     * @return array
     */
    public function getTabData(SaveEntityInterface $objEntity);





    // Methods setters
    // ******************************************************************************

    /**
     * Set persistor object.
     *
     * @param PersistorInterface $objPersistor
     */
    public function setPersistor(PersistorInterface $objPersistor);



    /**
     * Set specified data to specified entity.
     *
     * @param SaveEntityInterface $objEntity
     * @param array $tabData
     */
    public function setData(SaveEntityInterface $objEntity, array $tabData);





    // Methods repository
    // ******************************************************************************

    /**
     * Load data on specified entity,
     * from specified criteria, if required.
     * A criteria allows to retrieve one specific entity.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param SaveEntityInterface $objEntity
     * @param mixed $criteria = null
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function load(
        SaveEntityInterface $objEntity,
        $criteria = null,
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Save specified entity.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param SaveEntityInterface $objEntity
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function save(
        SaveEntityInterface $objEntity,
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Remove specified entity.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param SaveEntityInterface $objEntity
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function remove(
        SaveEntityInterface $objEntity,
        array $tabConfig = null,
        array &$tabInfo = null
    );
}