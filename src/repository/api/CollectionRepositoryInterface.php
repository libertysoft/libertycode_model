<?php
/**
 * Description :
 * This class allows to describe behavior of collection repository class.
 * Collection repository is item, containing all information to prepare data from entity collection, to save in persistence.
 * Collection repository is specific for one entity type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\api;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\entity\factory\api\EntityFactoryInterface;
use liberty_code\model\repository\api\RepositoryInterface;



interface CollectionRepositoryInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified entity collection is valid.
     *
     * @param mixed $collection
     * @return boolean
     */
    public function checkValidCollection($collection);





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity factory object.
     *
     * @return null|EntityFactoryInterface
     */
    public function getObjEntityFactory();



    /**
     * Get repository object.
     *
     * @return null|RepositoryInterface
     */
    public function getObjRepository();



    /**
     * Get config array.
     * Option merge allows to specify if result array is
     * collection repository configuration merged with repository configuration.
     *
     * @param boolean $boolMerge = true
     * @return array
     */
    public function getTabConfig($boolMerge = true);



    /**
     * Get new entity object, from specified data.
     *
     * @param array $tabData
     * @return SaveEntityInterface
     */
    public function getObjEntityNew(array $tabData);



    /**
     * Get index array of data, from specified entity collection.
     *
     * @param EntityCollectionInterface $objCollection
     * @return array
     */
    public function getTabData(EntityCollectionInterface $objCollection);





    // Methods setters
    // ******************************************************************************

    /**
     * Set entity factory object.
     *
     * @param EntityFactoryInterface $objEntityFactory
     */
    public function setEntityFactory(EntityFactoryInterface $objEntityFactory);



    /**
     * Set repository object.
     *
     * @param RepositoryInterface $objRepository
     */
    public function setRepository(RepositoryInterface $objRepository);



    /**
     * Set new entity objects,
     * from specified index array of data,
     * to specified entity collection.
     * Entity callable format: void function(SaveEntityInterface $objEntity):
     * Allows to handle entity before set on collection.
     *
     * @param EntityCollectionInterface $objCollection
     * @param array $tabData
     * @param boolean $boolIsNew = false
     * @param callable $callableEntity = null
     */
    public function setTabData(
        EntityCollectionInterface $objCollection,
        array $tabData,
        $boolIsNew = false,
        $callableEntity = null
    );





    // Methods repository
    // ******************************************************************************

    /**
     * Search and load data on specified entities,
     * from specified selection query.
     * Return true if all entities success, false if an error occurs on at least one entity.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param EntityCollectionInterface $objCollection
     * @param mixed $query
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function search(
        EntityCollectionInterface $objCollection,
        $query,
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Load data on specified entities,
     * from specified index array of criteria, if required.
     * Each criteria allows to retrieve one specific entity.
     * Return true if all entities success, false if an error occurs on at least one entity.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param EntityCollectionInterface $objCollection
     * @param array $tabCriteria = array()
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function load(
        EntityCollectionInterface $objCollection,
        array $tabCriteria = array(),
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Save specified entities.
     * Return true if all entities success, false if an error occurs on at least one entity.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param EntityCollectionInterface $objCollection
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function save(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Remove specified entities.
     * Return true if all entities success, false if an error occurs on at least one entity.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param EntityCollectionInterface $objCollection
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function remove(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    );
}