<?php
/**
 * Description :
 * This class allows to define sub-repo repository class.
 * Sub-repo repository type allows to configure sub-repositories, to save entity and sub-entities linked.
 * Can be consider is base of all repository type, especially when entities have sub-entities attributes.
 *
 * Sub-repo repository allows to specify parts of configuration:
 * [
 *     Default repository configuration,
 *
 *     sub_repository(optional): [
 *         "entity attribute key 1": [
 *             repository(required): "object implements repository or collection repository interface",
 *             entity_mapping(optional): [
 *                 "entity attribute key 1": "sub-repository entity attribute key 1",
 *                 ...,
 *                 "entity attribute key N": "sub-repository entity attribute key N"
 *             ]
 *         ],
 *         ...,
 *         "entity attribute key N": ...
 *     ],
 *
 *     search_criteria_attribute_key(optional): "string criteria attribute key, in search data result"
 *     // If search_criteria_null_allowed = true, null criteria registered in search data result.
 *     search_criteria_null_allowed(optional: got true if not found): true / false
 * ]
 *
 * Sub-repo repository allows to load entity, from following criteria configuration:
 *     mixed entity criteria (in case without sub-entities),
 *
 *     OR
 *
 *     [
 *         criteria(required): mixed entity criteria,
 *         sub_entity_criteria(required):[
 *             "entity attribute key 1": mixed sub-entity or sub-entity collection criteria 1,
 *             ...,
 *             "entity attribute key N": mixed sub-entity or sub-entity collection criteria N
 *         ]
 *     ] (in case with sub-entities)
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\model;

use liberty_code\model\repository\model\DefaultRepository;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\api\RepositoryInterface;
use liberty_code\model\repository\api\CollectionRepositoryInterface;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\sub_repository\library\ToolBoxSubRepo;
use liberty_code\model\repository\sub_repository\exception\ConfigInvalidFormatException;
use liberty_code\model\repository\sub_repository\exception\ExecConfigInvalidFormatException;
use liberty_code\model\repository\sub_repository\exception\SubEntityInvalidFormatException;
use liberty_code\model\repository\sub_repository\exception\CriteriaInvalidFormatException;



abstract class SubRepoRepository extends DefaultRepository
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check sub-repository execution option required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see load() (or save(), remove()) configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws ExecConfigInvalidFormatException
     */
    protected function checkSubRepoExecutionRequired(array $tabConfig = null)
    {
        // Set check argument
        ExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $result = (
            (!isset($tabExecConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_REQUIRE])) ||
            (intval($tabExecConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check if sub-repository configuration,
     * from specified entity attribute key,
     * is for entity.
     *
     * @param mixed $strAttributeKey
     * @return boolean
     */
    public function checkSubRepoConfigIsEntity($strAttributeKey)
    {
        // Init var
        $tabSubRepoConfig = $this->getTabSubRepoConfig($strAttributeKey);

        // Check if sub-repository configuration is for entity, if exists
        $result = (
            (!is_null($tabSubRepoConfig)) &&
            ($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY] instanceof RepositoryInterface)
        );

        // Return result
        return $result;
    }



    /**
     * Check if sub-repository configuration,
     * from specified entity attribute key,
     * is for collection.
     *
     * @param string $strAttributeKey
     * @return boolean
     */
    public function checkSubRepoConfigIsCollection($strAttributeKey)
    {
        // Init var
        $tabSubRepoConfig = $this->getTabSubRepoConfig($strAttributeKey);

        // Check if sub-repository configuration is for collection, if exists
        $result = (
                (!is_null($tabSubRepoConfig)) &&
                ($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY] instanceof CollectionRepositoryInterface)
            );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of attribute keys,
     * in sub-repository configuration.
     *
     * @return array
     */
    public function getTabSubRepoAttributeKey()
    {
        // Init var
        $result = array();
        $tabConfig = $this->getTabConfig();

        // Check attributes sub-repositories configuration, if required
        if(array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO, $tabConfig))
        {
            $result = array_keys($tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO]);
        }

        // Return result
        return $result;
    }



    /**
     * Get sub-repository configuration, from specified entity attribute key.
     *
     * @param string $strAttributeKey
     * @return null|array
     */
    public function getTabSubRepoConfig($strAttributeKey)
    {
        // Init var
        $result = null;
        $tabConfig = $this->getTabConfig();

        // Check attributes sub-repositories configuration, if required
        if(
            array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO, $tabConfig) &&
            array_key_exists($strAttributeKey, $tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO])
        )
        {
            $result = $tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO][$strAttributeKey];
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set entity mapping,
     * from specified entity attribute key or all entity attribute keys and specified entity,
     * to sub-element (entity or collection),
     * if required.
     *
     * @param SaveEntityInterface $objEntity
     * @param string $strAttributeKey = null
     * @return boolean
     * @throws ConfigInvalidFormatException
     * @throws SubEntityInvalidFormatException
     */
    public function setEntityMap(SaveEntityInterface $objEntity, $strAttributeKey = null)
    {
        // Init var
        $result = true;

        // Run all attributes, if required
        if(is_null($strAttributeKey))
        {
            // Run all attributes
            $tabAttributeKey = $this->getTabSubRepoAttributeKey();
            foreach($tabAttributeKey as $strAttributeKey)
            {
                $result = $this->setEntityMap($objEntity, $strAttributeKey) && $result;
            }
        }
        // Else: Run specific attribute, if required
        else
        {
            // Init var
            $tabSubRepoConfig = $this->getTabSubRepoConfig($strAttributeKey);

            // Check mapping required
            if(
                (!is_null($tabSubRepoConfig)) &&
                array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING, $tabSubRepoConfig)
            )
            {
                // Get infos
                $result = false;
                $objSubEntity = $objEntity->getAttributeValue($strAttributeKey);
                $tabMapConfig = $tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING];

                // Run all mapping configuration
                foreach($tabMapConfig as $strMapSubAttributeKey => $strMapAttributeKey)
                {
                    // Check configuration entity attribute key valid
                    if(!in_array($strMapAttributeKey, $objEntity->getTabAttributeKey()))
                    {
                        throw new ConfigInvalidFormatException($tabMapConfig);
                    }

                    // Set mapping for sub-entity
                    if($objSubEntity instanceof SaveEntityInterface)
                    {
                        /** @var SaveEntityInterface $objSubEntity */

                        // Check configuration sub-entity attribute key valid
                        if(!in_array($strMapSubAttributeKey, $objSubEntity->getTabAttributeKey()))
                        {
                            throw new ConfigInvalidFormatException($tabMapConfig);
                        }

                        $objSubEntity->setAttributeValue($strMapSubAttributeKey, $objEntity->getAttributeValue($strMapAttributeKey));
                        $result = true;
                    }
                    // Set mapping for sub-entities collection
                    else if($objSubEntity instanceof EntityCollectionInterface)
                    {
                        /** @var EntityCollectionInterface $objSubEntity */
                        foreach($objSubEntity->getTabKey() as $strSubEntityKey)
                        {
                            $objSubItemEntity = $objSubEntity->getItem($strSubEntityKey);

                            // Check configuration sub-entity attribute key valid
                            if(!in_array($strMapSubAttributeKey, $objSubItemEntity->getTabAttributeKey()))
                            {
                                throw new ConfigInvalidFormatException($tabMapConfig);
                            }

                            $objSubItemEntity->setAttributeValue($strMapSubAttributeKey, $objEntity->getAttributeValue($strMapAttributeKey));
                            $result = true;
                        }
                    }
                    // Else: throw sub-entity invalid exception
                    else
                    {
                        throw new SubEntityInvalidFormatException($strAttributeKey, get_class($objEntity));
                    }
                }
            }
        }

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load sub-element (entity or collection),
     * from specified entity,
     * from specified criteria, if required.
     * If sub-element specified, load only this,
     * else load all sub-elements.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure loading execution.
     * Null means no specific execution configuration required.
     * - For all sub-elements:
     * [
     *     "entity attribute key 1": [
     *         @see loadSubRepo() configuration array format (for specified sub-element)
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     *
     *     OR
     *
     *     @see loadSubRepo() configuration array format (for specified sub-element)
     * ]
     * - For specified sub-element:
     * [
     *     @see RepositoryInterface::load() return information array format
     *
     *     OR
     *
     *     @see CollectionRepositoryInterface::load() configuration array format
     * ]
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about loading execution.
     * Null means return information not required.
     * - For all sub-elements:
     * [
     *     "entity attribute key 1": [
     *         @see loadSubRepo() return information array format (for specified sub-element)
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     * ]
     * - For specified sub-element:
     * [
     *     @see RepositoryInterface::load() return information array format
     *
     *     OR
     *
     *     @see CollectionRepositoryInterface::load() return information array format
     * ]
     *
     * @param SaveEntityInterface $objEntity
     * @param $criteria
     * @param $boolEntityMap = true
     * @param string $strAttributeKey = null
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     * @throws ConfigInvalidFormatException
     * @throws SubEntityInvalidFormatException
     * @throws CriteriaInvalidFormatException
     */
    public function loadSubRepo(
        SaveEntityInterface $objEntity,
        $criteria,
        $boolEntityMap = true,
        $strAttributeKey = null,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = true;
        $boolEntityMap = (is_bool($boolEntityMap) ? $boolEntityMap : true);
        $strAttributeKey = (is_string($strAttributeKey) ? $strAttributeKey : null);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());

        // Run all sub-criteria attributes, if required
        if(is_null($strAttributeKey))
        {
            // Run all attributes
            $tabAttributeKey = ToolBoxSubRepo::getTabCriteriaAttributeKey($criteria);
            foreach($tabAttributeKey as $strAttributeKey)
            {
                $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                $tabAttrInfo = (is_null($tabInfo) ? null : array());
                $result =
                    $this->loadSubRepo(
                        $objEntity,
                        $criteria,
                        $boolEntityMap,
                        $strAttributeKey,
                        $tabAttrConfig,
                        $tabAttrInfo
                    ) &&
                    $result;

                // Register info, if required
                if(
                    (!is_null($tabAttrInfo)) &&
                    (count($tabAttrInfo) > 0)
                )
                {
                    $tabInfo[$strAttributeKey] = $tabAttrInfo;
                }
            }
        }
        // Else: Run specific sub-criteria attribute, if required
        else
        {
            // Check configuration entity attribute key valid
            if(!in_array($strAttributeKey, $objEntity->getTabAttributeKey()))
            {
                throw new CriteriaInvalidFormatException($strAttributeKey);
            }

            // Load sub-element, if required
            if(ToolBoxSubRepo::checkSubCriteriaExists($criteria, $strAttributeKey))
            {
                // Get info
                $subCriteria = ToolBoxSubRepo::getSubCriteria($criteria, $strAttributeKey);

                // Load sub-element
                $result = $this->executeSubRepoAction(
                    $strAttributeKey,
                    $objEntity,
                    function($objSubRepository, $objSubEntity) use ($subCriteria, $tabConfig, &$tabInfo) {
                        $result = false;

                        if($objSubRepository instanceof RepositoryInterface)
                        {
                            /** @var SaveEntityInterface $objSubEntity */
                            $result = $objSubRepository->load(
                                $objSubEntity,
                                $subCriteria,
                                $tabConfig,
                                $tabInfo
                            );
                        }
                        else if($objSubRepository instanceof CollectionRepositoryInterface)
                        {
                            /** @var EntityCollectionInterface $objSubEntity */
                            $result = $objSubRepository->load(
                                $objSubEntity,
                                $subCriteria,
                                $tabConfig,
                                $tabInfo
                            );
                        }

                        return $result;
                    }
                );
            }

            // Set entity mapping, if required
            if($boolEntityMap)
            {
                $this->setEntityMap($objEntity, $strAttributeKey);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Save sub-element (entity or collection),
     * from specified entity.
     * If sub-element specified, save only this,
     * else save all sub-elements.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure saving execution.
     * Null means no specific execution configuration required.
     * - For all sub-elements:
     * [
     *     "entity attribute key 1": [
     *         @see saveSubRepo() configuration array format (for specified sub-element)
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     *
     *     OR
     *
     *     @see saveSubRepo() configuration array format (for specified sub-element)
     * ]
     * - For specified sub-element:
     * [
     *     @see RepositoryInterface::save() configuration array format
     *
     *     OR
     *
     *     @see CollectionRepositoryInterface::save() configuration array format
     * ]
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about saving execution.
     * Null means return information not required.
     * - For all sub-elements:
     * [
     *     "entity attribute key 1": [
     *         @see saveSubRepo() return information array format (for specified sub-element)
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     * ]
     * - For specified sub-element:
     * [
     *     @see RepositoryInterface::save() return information array format
     *
     *     OR
     *
     *     @see CollectionRepositoryInterface::save() return information array format
     * ]
     *
     * @param SaveEntityInterface $objEntity
     * @param $boolEntityMap = true
     * @param string $strAttributeKey = null
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     * @throws ConfigInvalidFormatException
     * @throws SubEntityInvalidFormatException
     */
    public function saveSubRepo(
        SaveEntityInterface $objEntity,
        $boolEntityMap = true,
        $strAttributeKey = null,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = true;
        $boolEntityMap = (is_bool($boolEntityMap) ? $boolEntityMap : true);
        $strAttributeKey = (is_string($strAttributeKey) ? $strAttributeKey : null);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());

        // Run all attributes, if required
        if(is_null($strAttributeKey))
        {
            // Run all attributes
            $tabAttributeKey = $this->getTabSubRepoAttributeKey();
            foreach($tabAttributeKey as $strAttributeKey)
            {
                $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                $tabAttrInfo = (is_null($tabInfo) ? null : array());
                $result =
                    $this->saveSubRepo(
                        $objEntity,
                        $boolEntityMap,
                        $strAttributeKey,
                        $tabAttrConfig,
                        $tabAttrInfo
                    ) &&
                    $result;

                // Register info, if required
                if(
                    (!is_null($tabAttrInfo)) &&
                    (count($tabAttrInfo) > 0)
                )
                {
                    $tabInfo[$strAttributeKey] = $tabAttrInfo;
                }
            }
        }
        // Else: Run specific attribute, if required
        else
        {
            // Set entity mapping, if required
            if($boolEntityMap)
            {
                $this->setEntityMap($objEntity, $strAttributeKey);
            }

            // Save sub-element, if required
            $result = $this->executeSubRepoAction(
                    $strAttributeKey,
                    $objEntity,
                    function($objSubRepository, $objSubEntity) use($tabConfig, &$tabInfo) {
                        $result = false;

                        if($objSubRepository instanceof RepositoryInterface)
                        {
                            /** @var SaveEntityInterface $objSubEntity */
                            $result = $objSubRepository->save($objSubEntity, $tabConfig, $tabInfo);
                        }
                        else if($objSubRepository instanceof CollectionRepositoryInterface)
                        {
                            /** @var EntityCollectionInterface $objSubEntity */
                            $result = $objSubRepository->save($objSubEntity, $tabConfig, $tabInfo);
                        }

                        return $result;
                    }
                ) && $result;
        }

        // Return result
        return $result;
    }



    /**
     * Remove sub-element (entity or collection),
     * from specified entity.
     * If sub-element specified, remove only this,
     * else remove all sub-elements.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure removing execution.
     * Null means no specific execution configuration required.
     * - For all sub-elements:
     * [
     *     "entity attribute key 1": [
     *         @see removeSubRepo() configuration array format (for specified sub-element)
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     *
     *     OR
     *
     *     @see removeSubRepo() configuration array format (for specified sub-element)
     * ]
     * - For specified sub-element:
     * [
     *     @see RepositoryInterface::remove() configuration array format
     *
     *     OR
     *
     *     @see CollectionRepositoryInterface::remove() configuration array format
     * ]
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about removing execution.
     * Null means return information not required.
     * - For all sub-elements:
     * [
     *     "entity attribute key 1": [
     *         @see removeSubRepo() return information array format (for specified sub-element)
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     * ]
     * - For specified sub-element:
     * [
     *     @see RepositoryInterface::remove() return information array format
     *
     *     OR
     *
     *     @see CollectionRepositoryInterface::remove() return information array format
     * ]
     *
     * @param SaveEntityInterface $objEntity
     * @param $boolEntityMap = true
     * @param string $strAttributeKey = null
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     * @throws ConfigInvalidFormatException
     * @throws SubEntityInvalidFormatException
     */
    public function removeSubRepo(
        SaveEntityInterface $objEntity,
        $boolEntityMap = true,
        $strAttributeKey = null,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = true;
        $boolEntityMap = (is_bool($boolEntityMap) ? $boolEntityMap : true);
        $strAttributeKey = (is_string($strAttributeKey) ? $strAttributeKey : null);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());

        // Run all attributes, if required
        if(is_null($strAttributeKey))
        {
            // Run all attributes
            $tabAttributeKey = $this->getTabSubRepoAttributeKey();
            foreach($tabAttributeKey as $strAttributeKey)
            {
                $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                $tabAttrInfo = (is_null($tabInfo) ? null : array());
                $result =
                    $this->removeSubRepo(
                        $objEntity,
                        $boolEntityMap,
                        $strAttributeKey,
                        $tabAttrConfig,
                        $tabAttrInfo
                    ) &&
                    $result;

                // Register info, if required
                if(
                    (!is_null($tabAttrInfo)) &&
                    (count($tabAttrInfo) > 0)
                )
                {
                    $tabInfo[$strAttributeKey] = $tabAttrInfo;
                }
            }
        }
        // Else: Run specific attribute, if required
        else
        {
            // Set entity mapping, if required
            if($boolEntityMap)
            {
                $this->setEntityMap($objEntity, $strAttributeKey);
            }

            // Remove sub-element, if required
            $result = $this->executeSubRepoAction(
                    $strAttributeKey,
                    $objEntity,
                    function($objSubRepository, $objSubEntity) use ($tabConfig, &$tabInfo) {
                        $result = false;

                        if($objSubRepository instanceof RepositoryInterface)
                        {
                            /** @var SaveEntityInterface $objSubEntity */
                            $result = $objSubRepository->remove($objSubEntity, $tabConfig, $tabInfo);
                        }
                        else if($objSubRepository instanceof CollectionRepositoryInterface)
                        {
                            /** @var EntityCollectionInterface $objSubEntity */
                            $result = $objSubRepository->remove($objSubEntity, $tabConfig, $tabInfo);
                        }

                        return $result;
                    }
                ) && $result;
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see DefaultRepository::remove() configuration array format,
     *
     *     sub_repository_execution_require(optional: got true, if not found): true / false,
     *
     *     sub_repository_execution_config(optional: got all of current configuration, if not found): [
     *         @see removeSubRepo() configuration array format (for all sub-elements)
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     @see DefaultRepository::remove() return information array format,
     *
     *     sub_repository_info(optional): [
     *         @see removeSubRepo() return information array format (for all sub-elements)
     *     ]
     * ]
     *
     * @inheritdoc
     * @param boolean $boolSubRepo = true
     * @throws ConfigInvalidFormatException
     * @throws ExecConfigInvalidFormatException
     * @throws SubEntityInvalidFormatException
     */
    public function remove(
        SaveEntityInterface $objEntity,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $boolSubRepo = $this->checkSubRepoExecutionRequired($tabConfig);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $result = $this->executeTransaction(
            function() use ($objEntity, $boolSubRepo, $tabConfig, &$tabInfo)
            {
                // Init var
                $result = false;
                $tabSubRepoConfig = (
                (
                    (!is_null($tabConfig)) &&
                    array_key_exists(
                        ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG,
                        $tabConfig
                    )
                ) ?
                    $tabConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG] :
                    $tabConfig
                );
                $tabSubRepoInfo = (is_null($tabInfo) ? null : array());

                if(
                    (!$boolSubRepo) ||
                    $this->removeSubRepo(
                        $objEntity,
                        true,
                        null,
                        $tabSubRepoConfig,
                        $tabSubRepoInfo
                    )
                )
                {
                    $result = parent::remove($objEntity, $tabConfig, $tabInfo);
                }

                // Register sub-repository info, if required
                if(
                    (!is_null($tabSubRepoInfo)) &&
                    (count($tabSubRepoInfo) > 0)
                )
                {
                    $tabInfo[ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO] = $tabSubRepoInfo;
                }

                // Return result
                return $result;
            },
            function($result) {return $result;}
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified sub-repository action,
     * from specified entity attribute key and specified entity,
     * to sub-element (entity or collection).
     * Callable format: boolean function(
     *     RepositoryInterface|CollectionRepositoryInterface $objSubRepository,
     *     SaveEntityInterface|EntityCollectionInterface $objSubEntity
     * ): Allows to defined action.
     *
     * @param string $strAttributeKey
     * @param SaveEntityInterface $objEntity
     * @param callable $callable
     * @return boolean
     * @throws SubEntityInvalidFormatException
     */
    protected function executeSubRepoAction($strAttributeKey, SaveEntityInterface $objEntity, callable $callable)
    {
        // Init var
        $result = true;
        $tabSubRepoConfig = $this->getTabSubRepoConfig($strAttributeKey);

        // Check sub-action required
        if(!is_null($tabSubRepoConfig))
        {
            // Get info
            $objSubEntity = $objEntity->getAttributeValue($strAttributeKey);
            $objSubRepository = $tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY];

            // Check info valid
            if(
                (
                    ($objSubEntity instanceof SaveEntityInterface) &&
                    ($objSubRepository instanceof RepositoryInterface)
                ) ||
                (
                    ($objSubEntity instanceof EntityCollectionInterface) &&
                    ($objSubRepository instanceof CollectionRepositoryInterface)
                )
            )
            {
                // Execute repository action
                $result = $callable($objSubRepository, $objSubEntity);
            }
            // Else: throw sub-entity invalid exception
            else
            {
                throw new SubEntityInvalidFormatException($strAttributeKey, get_class($objEntity));
            }
        }

        // Return result
        return $result;
    }



}