<?php
/**
 * Description :
 * This class allows to define sub-repo collection repository class.
 * Sub-repo collection repository type allows to configure sub-repositories, to save entities and sub-entities linked.
 * Can be consider is base of all collection repository type, especially when entities have sub-entities attributes.
 *
 * Sub-repo collection repository allows to specify parts of configuration:
 * [
 *     Default collection repository configuration,
 *
 *     sub_repository(optional: allows to specify collection repository used to load, save, remove, instead of configuration of repository property): [
 *         "entity attribute key 1": [
 *             collection_repository(required): "object implements collection repository interface",
 *             collection(required): "object implements collection interface"
 *         ],
 *         ...,
 *         "entity attribute key N": ...
 *     ],
 *
 *     sub_repository_option(optional): [
 *         // In case where collection_repository provided for sub-repository loading,
 *         // complete loading with repository, if required.
 *         repository_complete(optional: got true if not found): true / false
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\model;

use liberty_code\model\repository\model\DefaultCollectionRepository;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\repository\library\ConstRepository;
use liberty_code\model\repository\api\CollectionRepositoryInterface;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\sub_repository\library\ToolBoxSubRepo;
use liberty_code\model\repository\sub_repository\model\SubRepoRepository;
use liberty_code\model\repository\sub_repository\exception\CollectionRepositoryInvalidFormatException;
use liberty_code\model\repository\sub_repository\exception\CollectionConfigInvalidFormatException;
use liberty_code\model\repository\sub_repository\exception\CollectionExecConfigInvalidFormatException;
use liberty_code\model\repository\sub_repository\exception\SubEntityInvalidFormatException;



/**
 * @method null|SubRepoRepository getObjRepository() @inheritdoc .
 */
abstract class SubRepoCollectionRepository extends DefaultCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_REPOSITORY:
                    CollectionRepositoryInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                case ConstRepository::DATA_KEY_DEFAULT_COLLECTION_CONFIG:
                    CollectionConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if option repository complete required.
     *
     * @return boolean
     */
    protected function checkSubRepoOptRepositoryComplete()
    {
        // Init var
        $result = true;
        $tabConfig = $this->getTabConfig(false);

        // Check repository complete required, if option found
        if(isset($tabConfig
            [ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION]
            [ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE]
        ))
        {
            $repoComplete = $tabConfig
            [ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION]
            [ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE];

            $result = (intval($repoComplete) !== 0);
        }

        // Return result
        return $result;
    }



    /**
     * Check if repository complete required.
     *
     * @return boolean
     */
    protected function checkSubRepoRepositoryCompleteRequire()
    {
        // Init var
        $result = false;
        $objRepository = $this->getObjRepository();

        if(!is_null($objRepository))
        {
            // Get info
            $tabCollRepoAttributeKey = $this->getTabSubRepoAttributeKey(false);
            $tabRepoAttributeKey = $objRepository->getTabSubRepoAttributeKey();

            // Run all sub-repository configuration attribute key
            for($intCpt = 0; ($intCpt < count($tabRepoAttributeKey)) && (!$result); $intCpt++)
            {
                // Check attribute key require repository sub-action: not found in collection sub-configuration
                $strRepoAttributeKey = $tabRepoAttributeKey[$intCpt];
                $result = (!in_array($strRepoAttributeKey, $tabCollRepoAttributeKey));
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check sub-repository execution option required.
     *
     * Configuration array format:
     * Execution configuration can be provided.
     * @see load() (or save(), remove()) configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws CollectionExecConfigInvalidFormatException
     */
    protected function checkSubRepoExecutionRequired(array $tabConfig = null)
    {
        // Set check argument
        CollectionExecConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabExecConfig = $tabConfig;
        $result = (
            (!isset($tabExecConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_REQUIRE])) ||
            (intval($tabExecConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of attribute keys,
     * in collection sub-repository configuration.
     * Option merge allows to specify if scope of configuration include
     * collection repository configuration merged with repository configuration or
     * only collection repository configuration.
     *
     * @param boolean $boolMerge = true
     * @return array
     */
    protected function getTabSubRepoAttributeKey($boolMerge = true)
    {
        // Init var
        $result = array();
        $tabConfig = $this->getTabConfig($boolMerge);

        // Check attributes sub-repositories configuration, if required
        if(array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO, $tabConfig))
        {
            $result = array_keys($tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO]);
        }

        // Return result
        return $result;
    }



    /**
     * Get collection sub-repository configuration, from specified entity attribute key.
     * Option merge allows to specify if scope of configuration include
     * collection repository configuration merged with repository configuration or
     * only collection repository configuration.
     *
     * @param string $strAttributeKey
     * @param boolean $boolMerge = true
     * @return null|array
     */
    protected function getTabSubRepoConfig($strAttributeKey, $boolMerge = true)
    {
        // Init var
        $result = null;
        $tabConfig = $this->getTabConfig($boolMerge);

        // Check attributes sub-repositories configuration, if required
        if(
            array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO, $tabConfig) &&
            array_key_exists($strAttributeKey, $tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO])
        )
        {
            $result = $tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO][$strAttributeKey];
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of criteria,
     * from specified index array of data.
     * Overwrite it to set specific feature.
     *
     * @param array $tabData
     * @return array
     */
    protected function getTabCriteria(array $tabData)
    {
        // Return result
        return ToolBoxSubRepo::getTabCriteriaFromData(
            $tabData,
            $this->getObjRepository()
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set entity mapping,
     * for specified entities collection.
     *
     * @param EntityCollectionInterface $objCollection
     * @return boolean
     */
    protected function setEntityMap(EntityCollectionInterface $objCollection)
    {
        // Init var
        $result = false;
        $objRepository = $this->getObjRepository();

        if(!is_null($objRepository))
        {
            // Run all entities
            $result = true;
            foreach($objCollection->getTabKey() as $strKey)
            {
                // Get info
                /** @var SaveEntityInterface $objEntity */
                $objEntity = $objCollection->getItem($strKey);

                // Set entity mapping
                $result = $objRepository->setEntityMap($objEntity) && $result;
            }
        }

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load entities sub-element (entity or collection),
     * from specified collection,
     * from specified index array of criteria, if required.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure loading execution.
     * Null means no specific execution configuration required.
     * [
     *     @see SubRepoRepository::loadSubRepo() configuration array format (for all sub-elements)
     * ]
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about loading execution.
     * Null means return information not required.
     * [
     *     "entity attribute key 1": [
     *         @see CollectionRepositoryInterface::load() return information array format,
     *
     *         entity(optional): [
     *             "entity key 1": [
     *                 @see RepositoryInterface::loadSubRepo() return information array format (for specified sub-element)
     *             ],
     *             ...,
     *             "entity key N": ...
     *         ]
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     * ]
     *
     * @param EntityCollectionInterface $objCollection
     * @param array $tabCriteria = array()
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    protected function loadSubRepo(
        EntityCollectionInterface $objCollection,
        array $tabCriteria = array(),
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;
        $objRepository = $this->getObjRepository();
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());

        if(!is_null($objRepository))
        {
            // Init var
            $result = true;
            $tabAttributeKey = $this->getTabSubRepoAttributeKey(false);
            $strRepoAttributeKey = $objRepository->getTabSubRepoAttributeKey();

            // Case process by entity attribute
            if(count($tabAttributeKey) > 0)
            {
                // Run all collection configuration attributes
                foreach($tabAttributeKey as $strAttributeKey)
                {
                    // Get sub-criteria array
                    $tabSubCriteria = array();
                    $tabCollectionCount = array();
                    foreach($tabCriteria as $criteria)
                    {
                        $subCriteria = ToolBoxSubRepo::getSubCriteria($criteria, $strAttributeKey);
                        $intCount = 0;

                        // Register sub criteria, if required
                        if(ToolBoxSubRepo::checkSubCriteriaExists($criteria, $strAttributeKey))
                        {
                            // Case sub-criteria is for entity (depending on repository sub configuration)
                            if($objRepository->checkSubRepoConfigIsEntity($strAttributeKey))
                            {
                                $tabSubCriteria[] = $subCriteria;
                                $intCount = 1;
                            }
                            // Case sub-criteria is for collection (depending on repository sub configuration)
                            else if(
                                $objRepository->checkSubRepoConfigIsCollection($strAttributeKey) &&
                                is_array($subCriteria)
                            )
                            {
                                foreach($subCriteria as $subCriteriaItem)
                                {
                                    $tabSubCriteria[] = $subCriteriaItem;
                                }

                                $intCount = count($subCriteria);
                            }
                        }

                        $tabCollectionCount[] = $intCount;
                    }

                    // Load sub-collection
                    $result = $this->executeSubRepoAction(
                        $strAttributeKey,
                        $objCollection,
                        function(CollectionRepositoryInterface $objSubCollectionRepo, EntityCollectionInterface $objSubCollection)
                        use ($tabSubCriteria, $objCollection, $strAttributeKey, $objRepository, $tabCollectionCount, $tabConfig, &$tabInfo) {
                            // Load in sub-collection
                            $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                            $tabAttrInfo = (is_null($tabInfo) ? null : array());
                            $result = $objSubCollectionRepo->load($objSubCollection, $tabSubCriteria, $tabAttrConfig, $tabAttrInfo);

                            // Register attribute info, if required
                            if(
                                (!is_null($tabAttrInfo)) &&
                                (count($tabAttrInfo) > 0)
                            )
                            {
                                $tabInfo[$strAttributeKey] = $tabAttrInfo;
                            }

                            // Run all entities
                            $tabKey = $objCollection->getTabKey();
                            $tabSubKey = $objSubCollection->getTabKey();
                            for(
                                $intCpt = 0, $intSubCpt = 0;
                                ($intCpt < count($tabKey)) && ($intCpt < count($tabCollectionCount)) && ($intSubCpt < count($tabSubKey));
                                $intCpt++
                            )
                            {
                                // Get info
                                $strKey = $tabKey[$intCpt];
                                $intCount = $intSubCpt + $tabCollectionCount[$intCpt];
                                $objEntity = $objCollection->getItem($strKey);

                                // Run all sub-entities of entity
                                while($intSubCpt < $intCount)
                                {
                                    // Get info
                                    $strSubKey = $tabSubKey[$intSubCpt];
                                    $objSubEntity = $objSubCollection->getItem($strSubKey);

                                    // set sub-entity in collection
                                    if($objRepository->checkSubRepoConfigIsEntity($strAttributeKey))
                                    {
                                        $objEntity->setAttributeValue($strAttributeKey, $objSubEntity);
                                    }
                                    else if($objRepository->checkSubRepoConfigIsCollection($strAttributeKey))
                                    {
                                        /** @var EntityCollectionInterface $objSubCollection */
                                        $objEntitySubCollection = $objEntity->getAttributeValue($strAttributeKey);
                                        $objEntitySubCollection->setItem($objSubEntity);
                                    }

                                    $intSubCpt++;
                                }
                            }

                            return $result;
                        },
                        false
                    ) && $result;
                }

                // Check repository complete required
                if(
                    (count($strRepoAttributeKey) > 0) &&
                    $this->checkSubRepoOptRepositoryComplete() &&
                    $this->checkSubRepoRepositoryCompleteRequire()
                )
                {
                    // Run all entities
                    $tabKey = $objCollection->getTabKey();
                    for($intCpt = 0; ($intCpt < count($tabKey)) && ($intCpt < count($tabCriteria)); $intCpt++)
                    {
                        // Get info
                        $strKey = $tabKey[$intCpt];
                        $criteria = $tabCriteria[$intCpt];
                        /** @var SaveEntityInterface $objEntity */
                        $objEntity = $objCollection->getItem($strKey);

                        // Run all entity attributes sub-configured
                        foreach($strRepoAttributeKey as $strAttributeKey)
                        {
                            // Check attribute not already process
                            if(!in_array($strAttributeKey, $tabAttributeKey))
                            {
                                // Load entity sub-element
                                $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                                $tabAttrInfo = (is_null($tabInfo) ? null : array());
                                $result =
                                    $objRepository->loadSubRepo(
                                        $objEntity,
                                        $criteria,
                                        false,
                                        $strAttributeKey,
                                        $tabAttrConfig,
                                        $tabAttrInfo
                                    ) &&
                                    $result;

                                // Register attribute info, if required
                                if(
                                    (!is_null($tabAttrInfo)) &&
                                    (count($tabAttrInfo) > 0)
                                )
                                {
                                    if(!array_key_exists($strAttributeKey, $tabInfo))
                                    {
                                        $tabInfo[$strAttributeKey] = array();
                                    }
                                    if(!array_key_exists(ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY, $tabInfo[$strAttributeKey]))
                                    {
                                        $tabInfo[$strAttributeKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY] = array();
                                    }
                                    $tabInfo[$strAttributeKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY][$strKey] = $tabAttrInfo;
                                }
                            }
                        }
                    }
                }
            }
            // Case else: process by entity, if required
            else if(count($strRepoAttributeKey) > 0)
            {
                // Run all entities
                $tabKey = $objCollection->getTabKey();
                for($intCpt = 0; ($intCpt < count($tabKey)) && ($intCpt < count($tabCriteria)); $intCpt++)
                {
                    // Get info
                    $strKey = $tabKey[$intCpt];
                    $criteria = $tabCriteria[$intCpt];
                    /** @var SaveEntityInterface $objEntity */
                    $objEntity = $objCollection->getItem($strKey);
                    $tabSubRepoInfo = (is_null($tabInfo) ? null : array());

                    // Load entity sub-element
                    $result =
                        $objRepository->loadSubRepo(
                            $objEntity,
                            $criteria,
                            false,
                            null,
                            $tabConfig,
                            $tabSubRepoInfo
                        ) &&
                        $result;

                    // Register sub-repository info, if required
                    if(
                        (!is_null($tabSubRepoInfo)) &&
                        (count($tabSubRepoInfo) > 0)
                    )
                    {
                        foreach($tabSubRepoInfo as $strAttrKey => $tabAttrInfo)
                        {
                            if(!array_key_exists($strAttrKey, $tabInfo))
                            {
                                $tabInfo[$strAttrKey] = array(
                                    ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY => array()
                                );
                            }
                            $tabInfo[$strAttrKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY][$strKey] = $tabAttrInfo;
                        }
                    }
                }
            }

            // Set entity mapping, if required
            $this->setEntityMap($objCollection);
        }

        // Return result
        return $result;
    }



    /**
     * Save entities sub-element (entity or collection),
     * from specified collection.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure saving execution.
     * Null means no specific execution configuration required.
     * [
     *     @see SubRepoRepository::saveSubRepo() configuration array format (for all sub-elements)
     * ]
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about saving execution.
     * Null means return information not required.
     * [
     *     "entity attribute key 1": [
     *         @see CollectionRepositoryInterface::save() return information array format,
     *
     *         entity(optional): [
     *             "entity key 1": [
     *                 @see RepositoryInterface::saveSubRepo() return information array format (for specified sub-element)
     *             ],
     *             ...,
     *             "entity key N": ...
     *         ]
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     * ]
     *
     * @param EntityCollectionInterface $objCollection
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    protected function saveSubRepo(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;
        $objRepository = $this->getObjRepository();
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());

        if(!is_null($objRepository))
        {
            // Init var
            $result = true;
            $tabAttributeKey = $this->getTabSubRepoAttributeKey(false);
            $strRepoAttributeKey = $objRepository->getTabSubRepoAttributeKey();

            // Set entity mapping, if required
            $this->setEntityMap($objCollection);

            // Case process by entity attribute
            if(count($tabAttributeKey) > 0)
            {
                // Run all collection configuration attributes
                foreach($tabAttributeKey as $strAttributeKey)
                {
                    // Save sub-collection
                    $result = $this->executeSubRepoAction(
                            $strAttributeKey,
                            $objCollection,
                            function(
                                CollectionRepositoryInterface $objSubCollectionRepo,
                                EntityCollectionInterface $objSubCollection
                            )
                            use($strAttributeKey, $tabConfig, &$tabInfo)
                            {
                                $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                                $tabAttrInfo = (is_null($tabInfo) ? null : array());
                                $result =  $objSubCollectionRepo->save($objSubCollection, $tabAttrConfig, $tabAttrInfo);

                                // Register attribute info, if required
                                if(
                                    (!is_null($tabAttrInfo)) &&
                                    (count($tabAttrInfo) > 0)
                                )
                                {
                                    $tabInfo[$strAttributeKey] = $tabAttrInfo;
                                }

                                return $result;
                            },
                            true
                        ) && $result;
                }

                // Check repository complete required
                if(
                    (count($strRepoAttributeKey) > 0) &&
                    $this->checkSubRepoOptRepositoryComplete() &&
                    $this->checkSubRepoRepositoryCompleteRequire()
                )
                {
                    // Run all entities
                    foreach($objCollection->getTabKey() as $strKey)
                    {
                        // Get info
                        /** @var SaveEntityInterface $objEntity */
                        $objEntity = $objCollection->getItem($strKey);

                        // Run all entity attributes sub-configured
                        foreach($strRepoAttributeKey as $strAttributeKey)
                        {
                            // Check attribute not already process
                            if(!in_array($strAttributeKey, $tabAttributeKey))
                            {
                                // Save entity sub-element
                                $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                                $tabAttrInfo = (is_null($tabInfo) ? null : array());
                                $result =
                                    $objRepository->saveSubRepo(
                                        $objEntity,
                                        false,
                                        $strAttributeKey,
                                        $tabAttrConfig,
                                        $tabAttrInfo
                                    ) &&
                                    $result;

                                // Register attribute info, if required
                                if(
                                    (!is_null($tabAttrInfo)) &&
                                    (count($tabAttrInfo) > 0)
                                )
                                {
                                    if(!array_key_exists($strAttributeKey, $tabInfo))
                                    {
                                        $tabInfo[$strAttributeKey] = array();
                                    }
                                    if(!array_key_exists(ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY, $tabInfo[$strAttributeKey]))
                                    {
                                        $tabInfo[$strAttributeKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY] = array();
                                    }
                                    $tabInfo[$strAttributeKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY][$strKey] = $tabAttrInfo;
                                }
                            }
                        }
                    }
                }
            }
            // Case else: process by entity, if required
            else if(count($strRepoAttributeKey) > 0)
            {
                // Run all entities
                foreach($objCollection->getTabKey() as $strKey)
                {
                    // Get info
                    /** @var SaveEntityInterface $objEntity */
                    $objEntity = $objCollection->getItem($strKey);
                    $tabSubRepoInfo = (is_null($tabInfo) ? null : array());

                    // Save entity sub-element
                    $result =
                        $objRepository->saveSubRepo(
                            $objEntity,
                            false,
                            null,
                            $tabConfig,
                            $tabSubRepoInfo
                        ) &&
                        $result;

                    // Register sub-repository info, if required
                    if(
                        (!is_null($tabSubRepoInfo)) &&
                        (count($tabSubRepoInfo) > 0)
                    )
                    {
                        foreach($tabSubRepoInfo as $strAttrKey => $tabAttrInfo)
                        {
                            if(!array_key_exists($strAttrKey, $tabInfo))
                            {
                                $tabInfo[$strAttrKey] = array(
                                    ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY => array()
                                );
                            }
                            $tabInfo[$strAttrKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY][$strKey] = $tabAttrInfo;
                        }
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Remove entities sub-element (entity or collection),
     * from specified collection.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure removing execution.
     * Null means no specific execution configuration required.
     * [
     *     @see SubRepoRepository::removeSubRepo() configuration array format (for all sub-elements)
     * ]
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about removing execution.
     * Null means return information not required.
     * [
     *     "entity attribute key 1": [
     *         @see CollectionRepositoryInterface::remove() return information array format,
     *
     *         entity(optional): [
     *             "entity key 1": [
     *                 @see RepositoryInterface::removeSubRepo() return information array format (for specified sub-element)
     *             ],
     *             ...,
     *             "entity key N": ...
     *         ]
     *     ],
     *     ...,
     *     "entity attribute key N": ...
     * ]
     *
     * @param EntityCollectionInterface $objCollection
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    protected function removeSubRepo(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;
        $objRepository = $this->getObjRepository();
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());

        if(!is_null($objRepository))
        {
            // Init var
            $result = true;
            $tabAttributeKey = $this->getTabSubRepoAttributeKey(false);
            $strRepoAttributeKey = $objRepository->getTabSubRepoAttributeKey();

            // Set entity mapping, if required
            $this->setEntityMap($objCollection);

            // Case process by entity attribute
            if(count($tabAttributeKey) > 0)
            {
                // Run all collection configuration attributes
                foreach($tabAttributeKey as $strAttributeKey)
                {
                    // Remove sub-collection
                    $result = $this->executeSubRepoAction(
                            $strAttributeKey,
                            $objCollection,
                            function(
                                CollectionRepositoryInterface $objSubCollectionRepo,
                                EntityCollectionInterface $objSubCollection
                            )
                            use($strAttributeKey, $tabConfig, &$tabInfo)
                            {
                                $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                                $tabAttrInfo = (is_null($tabInfo) ? null : array());
                                $result = $objSubCollectionRepo->remove($objSubCollection, $tabAttrConfig, $tabAttrInfo);

                                // Register attribute info, if required
                                if(
                                    (!is_null($tabAttrInfo)) &&
                                    (count($tabAttrInfo) > 0)
                                )
                                {
                                    $tabInfo[$strAttributeKey] = $tabAttrInfo;
                                }

                                return $result;
                            },
                            true
                        ) && $result;
                }

                // Check repository complete required
                if(
                    (count($strRepoAttributeKey) > 0) &&
                    $this->checkSubRepoOptRepositoryComplete() &&
                    $this->checkSubRepoRepositoryCompleteRequire()
                )
                {
                    // Run all entities
                    foreach($objCollection->getTabKey() as $strKey)
                    {
                        // Get info
                        /** @var SaveEntityInterface $objEntity */
                        $objEntity = $objCollection->getItem($strKey);

                        // Run all entity attributes sub-configured
                        foreach($strRepoAttributeKey as $strAttributeKey)
                        {
                            // Check attribute not already process
                            if(!in_array($strAttributeKey, $tabAttributeKey))
                            {
                                // Remove entity sub-element
                                $tabAttrConfig = (isset($tabConfig[$strAttributeKey]) ? $tabConfig[$strAttributeKey] : $tabConfig);
                                $tabAttrInfo = (is_null($tabInfo) ? null : array());
                                $result =
                                    $objRepository->removeSubRepo(
                                        $objEntity,
                                        false,
                                        $strAttributeKey,
                                        $tabAttrConfig,
                                        $tabAttrInfo
                                    ) &&
                                    $result;

                                // Register attribute info, if required
                                if(
                                    (!is_null($tabAttrInfo)) &&
                                    (count($tabAttrInfo) > 0)
                                )
                                {
                                    if(!array_key_exists($strAttributeKey, $tabInfo))
                                    {
                                        $tabInfo[$strAttributeKey] = array();
                                    }
                                    if(!array_key_exists(ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY, $tabInfo[$strAttributeKey]))
                                    {
                                        $tabInfo[$strAttributeKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY] = array();
                                    }
                                    $tabInfo[$strAttributeKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY][$strKey] = $tabAttrInfo;
                                }
                            }
                        }
                    }
                }
            }
            // Case else: process by entity, if required
            else if(count($strRepoAttributeKey) > 0)
            {
                // Run all entities
                foreach($objCollection->getTabKey() as $strKey)
                {
                    // Get info
                    /** @var SaveEntityInterface $objEntity */
                    $objEntity = $objCollection->getItem($strKey);
                    $tabSubRepoInfo = (is_null($tabInfo) ? null : array());

                    // Remove entity sub-element
                    $result =
                        $objRepository->removeSubRepo(
                            $objEntity,
                            false,
                            null,
                            $tabConfig,
                            $tabSubRepoInfo
                        ) &&
                        $result;

                    // Register sub-repository info, if required
                    if(
                        (!is_null($tabSubRepoInfo)) &&
                        (count($tabSubRepoInfo) > 0)
                    )
                    {
                        foreach($tabSubRepoInfo as $strAttrKey => $tabAttrInfo)
                        {
                            if(!array_key_exists($strAttrKey, $tabInfo))
                            {
                                $tabInfo[$strAttrKey] = array(
                                    ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY => array()
                                );
                            }
                            $tabInfo[$strAttrKey][ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO_ENTITY][$strKey] = $tabAttrInfo;
                        }
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see DefaultCollectionRepository::search() configuration array format,
     *
     *     load_execution_config(optional: got all of current configuration, if not found): [
     *         @see load() configuration array format
     *     ],
     *
     *     sub_repository_execution_require(optional: got true if not found): true / false
     * ]
     *
     * Return information array format:
     * [
     *     @see DefaultCollectionRepository::search() return information array format,
     *
     *     load_info(optional): [
     *         @see load() return information array format
     *     ]
     * ]
     *
     * Note:
     * -> Configuration sub_repository_execution_require:
     *     If required,
     *     the specified configured query must return array of criteria data (instead of entity data),
     *     understandable by @see getTabCriteria().
     *
     * @inheritdoc
     */
    public function search(
        EntityCollectionInterface $objCollection,
        $query,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $result = false;
        $boolSubRepo = $this->checkSubRepoExecutionRequired($tabConfig);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $tabRepoConfig = $this->getObjRepository()->getTabConfig();

        // Case sub-repository loading required and possible
        if(
            $boolSubRepo &&
            isset($tabRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY])
        )
        {
            $objPersistor = $this->getObjPersistor();
            if(!is_null($objPersistor))
            {
                // Get index array of criteria, from persistence data
                $tabPersistorInfo = (is_null($tabInfo) ? null : array());
                $tabData = $objPersistor->getTabSearchData(
                    $query,
                    $this->getTabConfigPersistSearch($objCollection, $tabConfig),
                    $tabPersistorInfo
                );
                $tabCriteria = $this->getTabCriteria($tabData);

                // Load collection from criteria
                $tabLoadConfig = (
                    (
                        (!is_null($tabConfig)) &&
                        array_key_exists(
                            ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_LOAD_EXECUTION_CONFIG,
                            $tabConfig
                        )
                    ) ?
                        $tabConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_LOAD_EXECUTION_CONFIG] :
                        $tabConfig
                );
                $tabLoadInfo = (is_null($tabInfo) ? null : array());
                $result = $this->load($objCollection, $tabCriteria, $tabLoadConfig, $tabLoadInfo);

                // Register info, if required
                if(
                    (!is_null($tabPersistorInfo)) &&
                    (count($tabPersistorInfo) > 0)
                )
                {
                    $tabInfo[ConstRepository::TAB_INFO_KEY_PERSISTOR_INFO] = $tabPersistorInfo;
                }

                if(
                    (!is_null($tabLoadInfo)) &&
                    (count($tabLoadInfo) > 0)
                )
                {
                    $tabInfo[ConstSubRepoRepository::TAB_INFO_KEY_LOAD_INFO] = $tabLoadInfo;
                }
            }
        }
        // Case else: simple collection loading required
        else
        {
            $result = parent::search($objCollection, $query, $tabConfig, $tabInfo);
        }

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see DefaultCollectionRepository::remove() configuration array format,
     *
     *     sub_repository_execution_require(optional: got true if not found): true / false,
     *
     *     sub_repository_execution_config(optional: got all of current configuration, if not found): [
     *         @see removeSubRepo() configuration array format
     *     ]
     * ]
     *
     * Return information array format:
     * [
     *     @see DefaultCollectionRepository::remove() return information array format,
     *
     *     sub_repository_info(optional): [
     *         @see removeSubRepo() return information array format
     *     ]
     * ]
     *
     * @inheritdoc
     */
    public function remove(
        EntityCollectionInterface $objCollection,
        array $tabConfig = null,
        array &$tabInfo = null
    )
    {
        // Init var
        $boolSubRepo = $this->checkSubRepoExecutionRequired($tabConfig);
        $tabInfo = (is_null($tabInfo) ? $tabInfo : array());
        $result = $this->executeTransaction(
            function() use ($objCollection, $boolSubRepo, $tabConfig, &$tabInfo)
            {
                // Init var
                $result = false;
                $boolSubRepo = (is_bool($boolSubRepo) ? $boolSubRepo : true);
                $tabSubRepoConfig = (
                    (
                        (!is_null($tabConfig)) &&
                        array_key_exists(
                            ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG,
                            $tabConfig
                        )
                    ) ?
                        $tabConfig[ConstSubRepoRepository::TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG] :
                        $tabConfig
                );
                $tabSubRepoInfo = (is_null($tabInfo) ? null : array());

                if(
                    (!$boolSubRepo) ||
                    $this->removeSubRepo($objCollection, $tabSubRepoConfig, $tabSubRepoInfo)
                )
                {
                    $result = parent::remove($objCollection, $tabConfig, $tabInfo);
                }

                // Register sub-repository info, if required
                if(
                    (!is_null($tabSubRepoInfo)) &&
                    (count($tabSubRepoInfo) > 0)
                )
                {
                    $tabInfo[ConstSubRepoRepository::TAB_INFO_KEY_SUB_REPO_INFO] = $tabSubRepoInfo;
                }

                // Return result
                return $result;
            },
            function($result) {return $result;}
        );

        // Return result
        return $result;
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified collection sub-repository action,
     * from specified entity attribute key and specified entities collection,
     * to entities sub-element (entity or collection).
     * Callable format: boolean function(
     *     CollectionRepositoryInterface $objSubCollectionRepo,
     *     EntityCollectionInterface $objSubCollection
     * ): Allows to defined action.
     *
     * @param string $strAttributeKey
     * @param EntityCollectionInterface $objCollection
     * @param callable $callable
     * @param boolean $boolSubCollectionLoad = true
     * @return boolean
     * @throws SubEntityInvalidFormatException
     */
    protected function executeSubRepoAction($strAttributeKey, EntityCollectionInterface $objCollection, callable $callable, $boolSubCollectionLoad = true)
    {
        // Init var
        $result = true;
        $tabSubRepoConfig = $this->getTabSubRepoConfig($strAttributeKey, false);

        // Check sub-action required
        if(!is_null($tabSubRepoConfig))
        {
            // Get info
            /** @var EntityCollectionInterface $objSubCollection */
            $objSubCollection = $tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION];
            /** @var CollectionRepositoryInterface $objSubCollectionRepo */
            $objSubCollectionRepo = $tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION_REPOSITORY];

            // Reset sub-collection
            $objSubCollection->removeItemAll();

            // Build sub-collection: Run all entities
            foreach($objCollection->getTabKey() as $strKey)
            {
                // Get info
                /** @var SaveEntityInterface $objEntity */
                $objEntity = $objCollection->getItem($strKey);
                $objSubEntity = $objEntity->getAttributeValue($strAttributeKey);

                // Add sub-entity in sub-collection
                if($objSubEntity instanceof SaveEntityInterface)
                {
                    if($boolSubCollectionLoad) $objSubCollection->setItem($objSubEntity);
                }
                else if($objSubEntity instanceof EntityCollectionInterface)
                {
                    if($boolSubCollectionLoad) $objSubCollection->setTabItem($objSubEntity);
                }
                // Else: throw sub-entity invalid exception
                else
                {
                    throw new SubEntityInvalidFormatException($strAttributeKey, get_class($objEntity));
                }
            }

            // Execute repository action
            $result = $callable($objSubCollectionRepo, $objSubCollection);

            // Reset sub-collection
            $objSubCollection->removeItemAll();
        }

        // Return result
        return $result;
    }



}