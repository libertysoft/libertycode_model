<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\exception;

use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;



class SubEntityInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 *
	 * @param string $strAttributeKey
     * @param string $strEntityClassPath
     */
	public function __construct($strAttributeKey, $strEntityClassPath)
	{
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf(
            ConstSubRepoRepository::EXCEPT_MSG_SUB_ENTITY_INVALID_FORMAT,
            strval($strAttributeKey),
            strval($strEntityClassPath)
        );
	}
	
	
	
}