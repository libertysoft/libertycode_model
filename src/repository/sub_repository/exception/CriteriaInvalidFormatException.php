<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\exception;

use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;



class CriteriaInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $criteria
     */
	public function __construct($criteria)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstSubRepoRepository::EXCEPT_MSG_CRITERIA_INVALID_FORMAT, strval($criteria));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified criteria has valid format.
     *
     * @param mixed $criteria
     * @return boolean
     */
    protected static function checkCriteriaIsValid($criteria)
    {
        // Init var
        $result =
            (!array_key_exists(ConstSubRepoRepository::TAB_CRITERIA_KEY_CRITERIA, $criteria)) &&
            (!array_key_exists(ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA, $criteria));

        // Check criteria with sub-criteria, if required
        if(!$result)
        {
            $result =
                // Check criteria valid format
                array_key_exists(ConstSubRepoRepository::TAB_CRITERIA_KEY_CRITERIA, $criteria) &&
                // (!is_null($criteria[ConstSubRepoRepository::TAB_CRITERIA_KEY_CRITERIA])) &&

                // Check sub-entities criteria valid format
                array_key_exists(ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA, $criteria) &&
                is_array($criteria[ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]);

            // Check sub-entities criteria, if required
            if($result)
            {
                // Run all sub-entities criteria
                $tabAttributeKey = array_keys($criteria[ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]);
                for($intCpt = 0; ($intCpt < count($tabAttributeKey)) && $result; $intCpt++)
                {
                    // Get info
                    $strAttributeKey = $tabAttributeKey[$intCpt];
                    //$subCriteria = $criteria[ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA][$strAttributeKey];

                    // Check sub-entity criteria
                    $result =
                        // Check attribute key valid format
                        is_string($strAttributeKey) && (trim($strAttributeKey) != ''); // &&

                        // Check sub-criteria valid format
                        //((!is_null($subCriteria)) || is_array($subCriteria));

                    // Check sub-criteria, if required
                    /*
                    if($result && is_array($subCriteria))
                    {
                        for($intCpt2 = 0; ($intCpt2 < count($subCriteria)) && $result; $intCpt2++)
                        {
                            // Check sub-criteria
                            $result = (!is_null($subCriteria));
                        }
                    }
                    */
                }
            }
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified criteria has valid format.
	 * 
     * @param mixed $criteria
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($criteria)
    {
		// Init var
		$result =
            (!is_array($criteria)) || static::checkCriteriaIsValid($criteria); // Check valid criteria

		// Throw exception if check not pass
		if(!$result)
		{
            throw new static((is_array($criteria) ? serialize($criteria) : $criteria));
		}
		
		// Return result
		return $result;
    }
	
	
	
}