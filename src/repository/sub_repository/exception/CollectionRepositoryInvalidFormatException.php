<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\exception;

use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\sub_repository\model\SubRepoRepository;



class CollectionRepositoryInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $repository
     */
	public function __construct($repository)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstSubRepoRepository::EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT, strval($repository));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified repository has valid format.
	 *
     * @param mixed $repository
     * @return boolean
     * @throws static
     */
    static public function setCheck($repository)
    {
        // Init var
        $result = (
            (is_null($repository)) ||
            ($repository instanceof SubRepoRepository)
        );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($repository);
        }

        // Return result
        return $result;
    }
	
	
	
}