<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\exception;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\repository\api\CollectionRepositoryInterface;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;



class CollectionConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstSubRepoRepository::EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result = (!array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO, $config));

        // Check attributes sub-repositories configuration, if required
        if(
            array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO, $config) &&
            is_array($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO])
        )
        {
            // Run all attributes sub-repositories configuration
            $result = true;
            $tabAttributeKey = array_keys($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO]);
            for($intCpt = 0; ($intCpt < count($tabAttributeKey)) && $result; $intCpt++)
            {
                // Get info
                $strAttributeKey = $tabAttributeKey[$intCpt];
                $tabSubRepoConfig = $config[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO][$strAttributeKey];

                // Check attribute sub-repository configuration
                $result =
                    // Check attribute key valid format
                    is_string($strAttributeKey) && (trim($strAttributeKey) != '') &&

                    // Check sub-repository configuration valid format
                    is_array($tabSubRepoConfig) &&

                    // Check sub-repository configuration has valid collection repository
                    isset($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION_REPOSITORY]) &&
                    ($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION_REPOSITORY] instanceof CollectionRepositoryInterface) &&

                    // Check sub-repository configuration has valid collection (work collection used for save, remove)
                    isset($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION]) &&
                    ($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION] instanceof EntityCollectionInterface);

                // Check collection valid, if required
                if($result)
                {
                    /** @var CollectionRepositoryInterface $objCollectionRepo */
                    $objCollectionRepo = $tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION_REPOSITORY];
                    /** @var EntityCollectionInterface $objCollection */
                    $objCollection = $tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_COLLECTION];

                    $result = $objCollectionRepo->checkValidCollection($objCollection);
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified option config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkOptConfigIsValid($config)
    {
        // Init var
        $result = (!array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION, $config));

        // Check attributes sub-repositories configuration, if required
        if(
            array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION, $config) &&
            is_array($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION])
        )
        {
            // Init var
            $tabSubRepoOptConfig = $config[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION];
            $result =
                // Check valid option repository complete
                (
                    (!isset($tabSubRepoOptConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE])) ||
                    (
                        // Check is boolean or numeric (0 = false, 1 = true)
                        is_bool($tabSubRepoOptConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE]) ||
                        is_int($tabSubRepoOptConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE]) ||
                        (
                            is_string($tabSubRepoOptConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE]) &&
                            ctype_digit($tabSubRepoOptConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE])
                        )
                    )
                );
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config) &&

            // Check valid option config
            static::checkOptConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}