<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\exception;

use liberty_code\model\repository\api\RepositoryInterface;
use liberty_code\model\repository\api\CollectionRepositoryInterface;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstSubRepoRepository::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result = (!array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO, $config));

        // Check attributes sub-repositories configuration, if required
        if(
            array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO, $config) &&
            is_array($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO])
        )
        {
            // Run all attributes sub-repositories configuration
            $result = true;
            $tabAttributeKey = array_keys($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO]);
            for($intCpt = 0; ($intCpt < count($tabAttributeKey)) && $result; $intCpt++)
            {
                // Get info
                $strAttributeKey = $tabAttributeKey[$intCpt];
                $tabSubRepoConfig = $config[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO][$strAttributeKey];

                // Check attribute sub-repository configuration
                $result =
                    // Check attribute key valid format
                    is_string($strAttributeKey) && (trim($strAttributeKey) != '') &&

                    // Check sub-repository configuration valid format
                    is_array($tabSubRepoConfig) &&

                    // Check sub-repository configuration has valid repository
                    isset($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY]) &&
                    (
                        ($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY] instanceof RepositoryInterface) ||
                        ($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY] instanceof CollectionRepositoryInterface)
                    ) &&

                    // Check sub-repository configuration has valid mapping
                    (
                        (!array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING, $tabSubRepoConfig)) ||
                        is_array($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING])
                    );

                // Check mapping, if required
                if(
                    $result &&
                    array_key_exists(ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING, $tabSubRepoConfig)
                )
                {
                    // Run all mapping configuration
                    $tabMapSubAttributeKey = array_keys($tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING]);
                    for($intCpt2 = 0; ($intCpt2 < count($tabMapSubAttributeKey)) && $result; $intCpt2++)
                    {
                        // Get info
                        $strMapSubAttributeKey = $tabMapSubAttributeKey[$intCpt];
                        $strMapAttributeKey = $tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING][$strMapSubAttributeKey];

                        // Check mapping
                        $result =
                            // Check mapping attribute key valid format
                            is_string($strMapAttributeKey) && (trim($strMapAttributeKey) != '') &&

                            // Check mapping sub-attribute key valid format
                            is_string($strMapSubAttributeKey) && (trim($strMapSubAttributeKey) != '');
                    }
                }
            }
        }

        // Init var
        $result =
            // Check valid previous checks
            $result &&

            // Check valid search criteria attribute key
            (
                (!isset($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY])) ||
                (
                    is_string($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY]) &&
                    (trim($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY]) != '')
                )
            ) &&

            // Check valid option search criteria null allowed
            (
                (!isset($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_NULL_ALLOWED])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_NULL_ALLOWED]) ||
                    is_int($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_NULL_ALLOWED]) ||
                    (
                        is_string($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_NULL_ALLOWED]) &&
                        ctype_digit($config[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_NULL_ALLOWED])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}