<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\library;



class ConstSubRepoRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_SUB_REPO = 'sub_repository';
    const TAB_CONFIG_KEY_SUB_REPO_REPOSITORY = 'repository';
    const TAB_CONFIG_KEY_SUB_REPO_ENTITY_MAPPING = 'entity_mapping';
    const TAB_CONFIG_KEY_SUB_REPO_COLLECTION_REPOSITORY = 'collection_repository';
    const TAB_CONFIG_KEY_SUB_REPO_COLLECTION = 'collection';
    const TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY = 'search_criteria_attribute_key';
    const TAB_CONFIG_KEY_SEARCH_CRITERIA_NULL_ALLOWED = 'search_criteria_null_allowed';
    const TAB_CONFIG_KEY_SUB_REPO_OPTION = 'sub_repository_option';
    const TAB_CONFIG_KEY_SUB_REPO_OPTION_COLLECTION_REPOSITORY_COMPLETE = 'collection_repository_complete';

    // Criteria configuration
    const TAB_CRITERIA_KEY_CRITERIA = 'criteria';
    const TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA = 'sub_entity_criteria';

    // Configuration execution
    const TAB_EXEC_CONFIG_KEY_LOAD_EXECUTION_CONFIG = 'load_execution_config';
    const TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_REQUIRE = 'sub_repository_execution_require';
    const TAB_EXEC_CONFIG_KEY_SUB_REPO_EXECUTION_CONFIG = 'sub_repository_execution_config';

    // Information configuration
    const TAB_INFO_KEY_LOAD_INFO = 'load_info';
    const TAB_INFO_KEY_SUB_REPO_INFO = 'sub_repository_info';
    const TAB_INFO_KEY_SUB_REPO_INFO_ENTITY = 'entity';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the sub-repository configuration standard.';
    const EXCEPT_MSG_SUB_ENTITY_INVALID_FORMAT = 'Following sub-entity "%1$s" invalid, in entity "%2$s"! The sub-entity must be entity type or entities collection type, according to sub-repository configuration.';
    const EXCEPT_MSG_CRITERIA_INVALID_FORMAT = 'Following criteria "%1$s" invalid! The criteria must follow the sub-repositories criteria standard.';
    const EXCEPT_MSG_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the sub-repository execution configuration standard.';
    const EXCEPT_MSG_COLLECTION_REPOSITORY_INVALID_FORMAT = 'Following repository "%1$s" invalid! It must be a sub-repository repository object.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, not empty and following the sub-repository collection configuration standard.';
    const EXCEPT_MSG_COLLECTION_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the sub-repository collection execution configuration standard.';



}