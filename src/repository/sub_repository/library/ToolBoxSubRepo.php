<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\repository\sub_repository\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\repository\api\RepositoryInterface;
use liberty_code\model\repository\api\CollectionRepositoryInterface;
use liberty_code\model\repository\sub_repository\library\ConstSubRepoRepository;
use liberty_code\model\repository\sub_repository\model\SubRepoRepository;
use liberty_code\model\repository\sub_repository\exception\CriteriaInvalidFormatException;



class ToolBoxSubRepo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified sub-entity criteria exists,
     * from specified criteria.
     *
     * @param mixed $criteria
     * @param mixed $strAttributeKey
     * @return boolean
     * @throws CriteriaInvalidFormatException
     */
    public static function checkSubCriteriaExists($criteria, $strAttributeKey)
    {
        // Init var
        $tabAttributeKey = static::getTabCriteriaAttributeKey($criteria);
        $result = in_array($strAttributeKey, $tabAttributeKey);

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity criteria, from specified criteria.
     *
     * @param mixed $criteria
     * @return mixed
     * @throws CriteriaInvalidFormatException
     */
    public static function getCriteria($criteria)
    {
        // Set check criteria
        CriteriaInvalidFormatException::setCheck($criteria);

        // Init var
        $result = $criteria;

        // Case criteria is composite, get entity criteria
        if(is_array($criteria) && array_key_exists(ConstSubRepoRepository::TAB_CRITERIA_KEY_CRITERIA, $criteria))
        {
            $result = $criteria[ConstSubRepoRepository::TAB_CRITERIA_KEY_CRITERIA];
        }

        // Return result
        return $result;
    }



    /**
     * Get index array of entity attribute keys,
     * where sub-criteria found,
     * from specified criteria.
     *
     * @param mixed $criteria
     * @return array
     * @throws CriteriaInvalidFormatException
     */
    public static function getTabCriteriaAttributeKey($criteria)
    {
        // Set check criteria
        CriteriaInvalidFormatException::setCheck($criteria);

        // Init var
        $result = array();

        // Get all attribute keys, if required
        if(is_array($criteria) && array_key_exists(ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA, $criteria))
        {
            $result = array_keys($criteria[ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]);
        }

        // Return result
        return $result;
    }



    /**
     * Get sub-entity criteria,
     * from specified criteria and
     * from specified attribute key.
     *
     * @param mixed $criteria
     * @param mixed $strAttributeKey
     * @return null|mixed
     * @throws CriteriaInvalidFormatException
     */
    public static function getSubCriteria($criteria, $strAttributeKey)
    {
        // Init var
        $result = (
            (static::checkSubCriteriaExists($criteria, $strAttributeKey)) ?
                $criteria[ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA][$strAttributeKey] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get index array of criteria,
     * from specified index array of data.
     *
     * @param array $tabData
     * @param RepositoryInterface $objRepository
     * @return array
     */
    public static function getTabCriteriaFromData(
        array $tabData,
        RepositoryInterface $objRepository
    )
    {
        // Init var
        $result = array();

        // Run each data
        foreach($tabData as $data)
        {
            // Set criteria info about current data
            ToolBoxSubRepo::setTabCriteriaFromData(
                $result,
                $data,
                $objRepository
            );
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set index array of criteria,
     * from specified data and
     * repository configuration.
     *
     * @param array &$tabCriteria
     * @param array $data
     * @param RepositoryInterface $objRepository
     */
    public static function setTabCriteriaFromData(
        array &$tabCriteria,
        array $data,
        RepositoryInterface $objRepository
    )
    {
        // Init var
        $tabConfig = $objRepository->getTabConfig();
        $boolCriteriaNull = (
            isset($tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_NULL_ALLOWED]) ?
                (intval($tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_NULL_ALLOWED]) != 0) :
                true
        );

        if(isset($tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY]))
        {
            // Get criteria key
            $strCritAttrKey = $tabConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SEARCH_CRITERIA_ATTRIBUTE_KEY];

            // Get criteria from data
            $dataCriteria = null;
            if(array_key_exists($strCritAttrKey, $data))
            {
                $dataCriteria = $data[$strCritAttrKey];
            }

            // Check criteria valid
            if(
                $boolCriteriaNull ||
                (!is_null($dataCriteria))
            )
            {
                // Try to find criteria
                $boolFind = false;
                for($intCpt = 0; ($intCpt < count($tabCriteria)) && (!$boolFind); $intCpt++)
                {
                    $criteria = static::getCriteria($tabCriteria[$intCpt]);
                    $boolFind = ($criteria == $dataCriteria);
                }

                // Create criteria if not exists
                if(!$boolFind)
                {
                    $tabCriteria[] = $dataCriteria;
                    $intCpt = (count($tabCriteria) - 1);
                }
                else
                {
                    $intCpt--;
                }

                // Process sub-repository, if required
                if($objRepository instanceof SubRepoRepository)
                {
                    // Init reformat criteria function
                    $setSubCriteriaFormat = function (array &$tabCriteria) use ($intCpt)
                    {
                        if(
                            (!is_array($tabCriteria[$intCpt])) ||
                            (!array_key_exists(ConstSubRepoRepository::TAB_CRITERIA_KEY_CRITERIA, $tabCriteria[$intCpt])) ||
                            (!array_key_exists(ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA, $tabCriteria[$intCpt]))
                        )
                        {
                            $tabCriteria[$intCpt] = array(
                                ConstSubRepoRepository::TAB_CRITERIA_KEY_CRITERIA => $tabCriteria[$intCpt],
                                ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA => array()
                            );
                        }
                    };

                    // Run each sub-repository entity attributes
                    $tabSubRepoAttributeKey = $objRepository->getTabSubRepoAttributeKey();
                    foreach($tabSubRepoAttributeKey as $strAttributeKey)
                    {
                        // Get info
                        $tabSubRepoConfig = $objRepository->getTabSubRepoConfig($strAttributeKey);
                        $objSubRepository = $tabSubRepoConfig[ConstSubRepoRepository::TAB_CONFIG_KEY_SUB_REPO_REPOSITORY];

                        // Case sub-repository for entity
                        if($objRepository->checkSubRepoConfigIsEntity($strAttributeKey))
                        {
                            /** @var RepositoryInterface $objSubRepository */

                            // Reformat criteria, if required
                            $setSubCriteriaFormat($tabCriteria);

                            // Get array of sub-repository criteria, if exists
                            $tabSubRepoCriteria = array();
                            if(
                            isset(
                                $tabCriteria[$intCpt]
                                [ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]
                                [$strAttributeKey]
                            )
                            )
                            {
                                $tabSubRepoCriteria[] = $tabCriteria[$intCpt]
                                [ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]
                                [$strAttributeKey];
                            }

                            // Get criteria info about current data
                            static::setTabCriteriaFromData(
                                $tabSubRepoCriteria,
                                $data,
                                $objSubRepository
                            );

                            // Set criteria, if required
                            if(count($tabSubRepoCriteria) > 0)
                            {
                                $tabCriteria[$intCpt]
                                [ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]
                                [$strAttributeKey] = $tabSubRepoCriteria[0];
                            }
                        }
                        // Case else: sub-repository for collection
                        else if($objRepository->checkSubRepoConfigIsCollection($strAttributeKey))
                        {
                            /** @var CollectionRepositoryInterface $objSubRepository */

                            // Reformat criteria, if required
                            $setSubCriteriaFormat($tabCriteria);

                            // Get array of sub-repository criteria, if exists
                            $tabSubRepoCriteria = array();
                            if(
                            isset(
                                $tabCriteria[$intCpt]
                                [ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]
                                [$strAttributeKey]
                            )
                            )
                            {
                                $tabSubRepoCriteria = $tabCriteria[$intCpt]
                                [ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]
                                [$strAttributeKey];
                            }

                            // Get criteria info about current data
                            static::setTabCriteriaFromData(
                                $tabSubRepoCriteria,
                                $data,
                                $objSubRepository->getObjRepository()
                            );

                            // Set criteria
                            $tabCriteria[$intCpt]
                            [ConstSubRepoRepository::TAB_CRITERIA_KEY_SUB_ENTITY_CRITERIA]
                            [$strAttributeKey] = $tabSubRepoCriteria;
                        }
                    }
                }
            }
        }
    }



}