<?php
/**
 * Description :
 * This class allows to describe behavior of persistor class.
 * Persistor allows to design entities data storage engine and
 * implements all storage actions, for specific storage support.
 *
 * Entity data uses following format:
 * [
 *     "attribute name 1": "attribute value 1",
 *     ...,
 *     "attribute name N": "attribute value N"
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\persistence\api;



interface PersistorInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get data from specified identifier, if required.
     * Return null if success but nothing found,
     * data array if success and data found,
     * false if an error occurs.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param mixed $id = null
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean|null|array
     */
    public function getData(
        $id = null,
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Get index array of data,
     * from specified index array of identifiers, if required.
     * Return array of data if success, false if an error occurs.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array $tabId = array()
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean|array
     */
    public function getTabData(
        array $tabId = array(),
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Get index array of data,
     * from specified selection query.
     * Return array of data if success, false if an error occurs.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param mixed $query
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean|array
     */
    public function getTabSearchData(
        $query,
        array $tabConfig = null,
        array &$tabInfo = null
    );





    // Methods setters
    // ******************************************************************************

    /**
     * Create specified data.
     * Return true if success, false if an error occurs.
     *
     * Return data array format:
     * Specified data array is returned, with potential changes if possible,
     * due to execution.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return identifier format:
     * Identifier can be returned, if possible, to retrieve data.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array &$data
     * @param array $tabConfig = null
     * @param mixed &$id = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function createData(
        array &$data,
        array $tabConfig = null,
        &$id = null,
        array &$tabInfo = null
    );



    /**
     * Create specified index array of data.
     * Return true if success, false if an error occurs.
     *
     * Return array of data format:
     * Specified index array of data is returned, with potential changes if possible,
     * due to execution.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return array of identifiers format:
     * Index array of identifiers, if possible.
     * Null means identifiers not required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array &$tabData
     * @param array $tabConfig = null
     * @param array &$tabId = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function createTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabId = null,
        array &$tabInfo = null
    );



    /**
     * Update specified data.
     * Return true if success, false if an error occurs.
     *
     * Return data array format:
     * Specified data array is returned, with potential changes if possible,
     * due to execution.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array &$data
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function updateData(
        array &$data,
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Update specified index array of data.
     * Return true if success, false if an error occurs.
     *
     * Return array of data format:
     * Specified index array of data is returned, with potential changes if possible,
     * due to execution.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array &$tabData
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function updateTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Delete specified data.
     * Return true if success, false if an error occurs.
     *
     * Return data array format:
     * Specified data array is returned, with potential changes if possible,
     * due to execution.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array &$data
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function deleteData(
        array &$data,
        array $tabConfig = null,
        array &$tabInfo = null
    );



    /**
     * Delete specified index array of data.
     * Return true if success, false if an error occurs.
     *
     * Return array of data format:
     * Specified index array of data is returned, with potential changes if possible,
     * due to execution.
     *
     * Configuration array format:
     * Execution configuration can be provided,
     * to configure execution.
     * Null means no specific execution configuration required.
     *
     * Return information array format:
     * Information can be returned, if possible,
     * to retrieve information about execution.
     * Null means return information not required.
     *
     * @param array &$tabData
     * @param array $tabConfig = null
     * @param array &$tabInfo = null
     * @return boolean
     */
    public function deleteTabData(
        array &$tabData,
        array $tabConfig = null,
        array &$tabInfo = null
    );





    // Methods transaction
    // ******************************************************************************

    /**
     * Check if process is in current transaction:
     * transaction already start and not closed.
     *
     * @return boolean
     */
    public function checkInTransaction();



    /**
     * Start transaction.
     *
     * @return boolean
     */
    public function transactionStart();



    /**
     * End transaction and commit.
     *
     * @return boolean
     */
    public function transactionEndCommit();



    /**
     * End transaction and cancel.
     *
     * @return boolean
     */
    public function transactionEndRollback();



}