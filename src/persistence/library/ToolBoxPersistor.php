<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\persistence\library;

use liberty_code\library\instance\model\Multiton;

use Throwable;
use liberty_code\model\persistence\api\PersistorInterface;



class ToolBoxPersistor extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * Execute specified transaction process,
     * in transaction, if possible,
     * and return result.
     *
     * Process callback format:
     * mixed function()
     *
     * Check result is valid callback format:
     * boolean function(mixed $result):
     * Check if result, got from process execution,
     * is valid or not.
     * Null means no specific result validation required,
     * so no rollback done, based on result.
     *
     * @param PersistorInterface $objPersistor
     * @param callable $callableProcess
     * @param callable $callableCheckResultIsValid = null
     * @return mixed
     */
    public static function executeTransaction(
        PersistorInterface $objPersistor,
        callable $callableProcess,
        callable $callableCheckResultIsValid = null
    )
    {
        // Start transaction, if required
        $boolStart = (
            (!$objPersistor->checkInTransaction()) ?
                $objPersistor->transactionStart() :
                false
        );

        // Try execute process
        try
        {
            // Execute process
            $result = $callableProcess();

            // Close transaction, if required
            if($boolStart && $objPersistor->checkInTransaction())
            {
                // Cancel transaction, if result not valid
                if(
                    (!is_null($callableCheckResultIsValid)) &&
                    (!$callableCheckResultIsValid($result))
                )
                {
                    $objPersistor->transactionEndRollback();
                }
                // Else: send transaction
                else
                {
                    $objPersistor->transactionEndCommit();
                }
            }
        }
        catch (Throwable $objException)
        {
            //  Close and cancel transaction, if required
            if($boolStart && $objPersistor->checkInTransaction())
            {
                $objPersistor->transactionEndRollback();
            }

            // Throw exception
            throw $objException;
        }

        // Return result
        return $result;
    }



}