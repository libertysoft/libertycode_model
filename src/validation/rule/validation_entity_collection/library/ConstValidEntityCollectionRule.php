<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\validation_entity_collection\library;



class ConstValidEntityCollectionRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'validation_entity_collection';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must be an entity collection, passing validation.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_COLLECTION_CONFIG = 'collection_config';
    const TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY = 'attribute_key';

    // Error information configuration
    const TAB_ERROR_INFO_KEY_ENTITY_COLLECTION_ERROR = 'entity_collection_error';



    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the validation entity collection rule validation configuration standard.';
}