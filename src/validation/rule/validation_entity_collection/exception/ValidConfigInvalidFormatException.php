<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\validation_entity_collection\exception;

use liberty_code\model\validation\rule\validation_entity_collection\library\ConstValidEntityCollectionRule;



class ValidConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstValidEntityCollectionRule::EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid attribute key
            (
                (!isset($config[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY])) ||
                (
                    is_string($config[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY]) &&
                    (trim($config[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY]) != '')
                ) ||
                (
                    is_array($config[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY]) &&
                    $checkTabStrIsValid($config[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY])
                )
            ) &&

            // Check valid collection configuration
            (
                (!isset($config[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_COLLECTION_CONFIG])) ||
                is_array($config[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_COLLECTION_CONFIG])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}