<?php
/**
 * Description :
 * This class allows to define validation entity collection rule class.
 * Validation entity collection rule allows to check if specified data is entity collection,
 * which successes validation.
 *
 * Validation entity collection rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Validation entity collection rule uses the following specified validation configuration:
 * [
 *     Default rule validation configuration,
 *
 *     collection_config(optional):
 *         @see ValidEntityCollectionInterface::checkValid() configuration array format,
 *
 *     attribute_key(optional):
 *         "string attribute key" OR ["string attribute key 1", ... "string attribute key N"]
 * ]
 *
 * Validation entity collection rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\validation_entity_collection\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\exception\ValidationFailException;
use liberty_code\model\entity\api\ValidEntityCollectionInterface;
use liberty_code\model\validation\rule\validation_entity_collection\library\ConstValidEntityCollectionRule;
use liberty_code\model\validation\rule\validation_entity_collection\exception\ValidConfigInvalidFormatException;



class ValidEntityCollectionRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $collectionConfig = (
            isset($tabConfig[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_COLLECTION_CONFIG]) ?
                $tabConfig[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_COLLECTION_CONFIG] :
                null
        );
        $attrKey = (
            isset($tabConfig[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY]) ?
                $tabConfig[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY] :
                null
        );
        $result = (
            ($value instanceof ValidEntityCollectionInterface) ?
                $value->checkValid($collectionConfig, $attrKey) :
                false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstValidEntityCollectionRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstValidEntityCollectionRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * @inheritdoc
     */
    public function getObjErrorException(
        $strName,
        $value,
        array $tabConfig = null
    )
    {
        // Init var
        $result = null;
        $collectionConfig = (
            isset($tabConfig[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_COLLECTION_CONFIG]) ?
                $tabConfig[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_COLLECTION_CONFIG] :
                null
        );
        $attrKey = (
            isset($tabConfig[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY]) ?
                $tabConfig[ConstValidEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY] :
                null
        );
        $strErrorMsg = $this->getStrErrorMessage($strName, $value, $tabConfig);
        $tabError = null;
        if($value instanceof ValidEntityCollectionInterface)
        {
            $tabError = array();
            $value->checkValid($collectionConfig, $attrKey, $tabError);
        }

        // Get error, if required
        if(
            // Check error message found
            (!is_null($strErrorMsg)) ||

            // Check array of error found
            (!is_null($tabError))
        )
        {
            $tabInfo = (
                (is_array($tabError)) ?
                    array(ConstValidEntityCollectionRule::TAB_ERROR_INFO_KEY_ENTITY_COLLECTION_ERROR => $tabError) :
                    array()
            );
            $result = new ValidationFailException(
                $strErrorMsg,
                $this,
                $strName,
                $value,
                $tabConfig,
                $tabInfo
            );
        }

        // Return result
        return $result;
    }



}