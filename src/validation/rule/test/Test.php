<?php

// Init var
$strTestRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strTestRootAppPath . '/src/validation/rule/test/ValidatorTest.php');
require_once($strTestRootAppPath . '/src/entity/test/ClassTest.php');

// Use
use liberty_code\validation\rule\exception\ValidationFailException;
use liberty_code\model\entity\model\DefaultValidEntityCollection;
use liberty_code\model\validation\rule\library\ToolBoxEntityRule;
use liberty_code\model\entity\test\ClassTest;
use liberty_code\model\repository\test\model\right\RightCollection;
use liberty_code\model\repository\test\model\admin\AdminEntity;
use liberty_code\model\repository\test\model\user\UserEntity;



// Init var
$objEntity1 = new ClassTest(
    array(
        'id' => 1,
        'nmFirst' => 'Fnm_1',
        'nm' => 'NM_1',
        'age' => 18,
        'isDev' => 1
    ),
    $objValidator
);
$objEntity2 = new ClassTest(
    array(
        'id' => 2,
        'nmFirst' => 'Fnm_2',
        'nm' => 'NM_2',
        'age' => 25,
        'isDev' => 0
    ),
    $objValidator
);
$objEntity3 = new ClassTest(
    array(
        'id' => 3,
        'nmFirst' => 'Fnm_3',
        'nm' => 'NM_3',
        'age' => 27,
        'isDev' => true
    ),
    $objValidator
);
$objEntity4 = new ClassTest(
    array(
        'id' => 4,
        'nmFirst' => 'Fnm_4',
        'nm' => 'NM_4',
        'age' => 30,
        'isDev' => false
    ),
    $objValidator
);
$objEntity5 = new ClassTest(
    array(
        'id' => 5,
        'nmFirst' => 'Fnm_5',
        'nm' => 5,
        'age' => 37,
        'isDev' => '1'
    ),
    $objValidator
);
$objEntity6 = new ClassTest(
    array(
        'id' => 6,
        'nmFirst' => 'Fnm_6',
        'nm' => '',
        'age' => 40,
        'isDev' => '0'
    ),
    $objValidator
);

$tabItem = array(
    $objEntity1,
    $objEntity2,
    $objEntity3,
    $objEntity4,
    $objEntity5,
    $objEntity6
);

$objEntityCollection = new DefaultValidEntityCollection(ClassTest::class);
$objEntityCollection->setTabItem($tabItem);

$objEntityCollection2 = new DefaultValidEntityCollection(ClassTest::class);
$objEntityCollection2->setItem($objEntity3);
$objEntityCollection2->setItem($objEntity4);

$objUserEntity1 = new UserEntity(array(
    'login' => 'usr_1',
    'pw' => 'pw_1',
    'nmFirst' => 'Fnm 1',
    'nm' => 'Nm 1',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 1',
        'rightCollection' => new RightCollection()
    ))
));

$objUserEntity2 = new UserEntity(array(
    'login' => 'usr_2',
    'pw' => 'pw_2',
    'nmFirst' => 'Fnm 2',
    'nm' => 'Nm 2',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 2',
        'rightCollection' => new RightCollection()
    ))
));

$objUserEntity3 = new UserEntity(array(
    'login' => 'usr_3',
    'pw' => 'pw_3',
    'nmFirst' => 'Fnm 3',
    'nm' => 'Nm 3',
    'admin' => new AdminEntity(array(
        'nm' => 'Adm 3',
        'rightCollection' => new RightCollection()
    ))
));

$objUserEntityCollection = new DefaultValidEntityCollection(UserEntity::class);
$objUserEntityCollection->setItem($objUserEntity1);
$objUserEntityCollection->setItem($objUserEntity2);
$objUserEntityCollection->setItem($objUserEntity3);



// Test data validation
echo('Test validation : <br />');

$tabInfo = array(
    // Test exist Ko
    [
        // Data configuration
        [
            'nmFirst' => 'Fnm_1'
        ],
        // Rule configuration
        [
            'nmFirst' => [
                'entity_collection_attribute_exist',
                []
            ]
        ]
    ],

    // Test exist 1 Ok/exist 2 Ko
    [
        // Data configuration
        [
            'nmFirst' => 'Fnm_2'
        ],
        // Rule configuration
        [
            'nmFirst' => [
                [
                    'entity_collection_attribute_exist',
                    [
                        'entity_collection' => $objEntityCollection
                    ]
                ],
                [
                    'rule_name' => 'entity_collection_attribute_exist',
                    'rule_config' => [
                        'entity_collection' => $objEntityCollection,
                        'attribute_key' => 'nmFirst',
                        'exclude' => [
                            'id' => 2
                        ],
                        'error_message_pattern' => '%1$s not found (exclude id).'
                    ]
                ]
            ]
        ]
    ],

    // Test exist 1 Ko/exist 2 Ok
    [
        // Data configuration
        [
            'nmFirst' => 'Fnm_3'
        ],
        // Rule configuration
        [
            'nmFirst' => [
                [
                    'entity_collection_attribute_exist',
                    [
                        'entity_collection' => $objEntityCollection,
                        'count_min' => 0,
                        'count_max' => 0,
                        'error_message_pattern' => '%1$s must be unique.'
                    ]
                ],
                [
                    'rule_name' => 'entity_collection_attribute_exist',
                    'rule_config' => [
                        'entity_collection' => $objEntityCollection,
                        'attribute_key' => 'nmFirst',
                        'count_min' => 0,
                        'count_max' => 0,
                        'exclude' => [
                            'id' => 3
                        ],
                        'error_message_pattern' => '%1$s must be unique (exclude id).'
                    ]
                ]
            ]
        ]
    ],

    // Test exist Ok
    [
        // Data configuration
        [
            'nmFirst' => 'Fnm_4'
        ],
        // Rule configuration
        [
            'nmFirst' => [
                [
                    'rule_name' => 'entity_collection_attribute_exist',
                    'rule_config' => [
                        'entity_collection' => $objEntityCollection,
                        'attribute_key' => 'nmFirst',
                        'count_min' => 0,
                        'count_max' => 0,
                        'exclude' => [
                            'id' => 4
                        ],
                        'error_message_pattern' => '%1$s must be unique (exclude id).'
                    ]
                ],
                [
                    'entity_collection_attribute_exist',
                    [
                        'entity_collection' => $objEntityCollection
                    ]
                ]
            ]
        ]
    ],

    // Test valid entity Ko/valid entity collection Ko
    [
        // Data configuration
        [
            'entity' => $objEntity5
        ],
        // Rule configuration
        [
            'entity' => [
                [
                    'validation_entity'
                ],
                [
                    'rule_name' => 'validation_entity_collection'
                ]
            ]
        ]
    ],

    // Test valid entity Ok/valid entity collection Ko
    [
        // Data configuration
        [
            'entity' => $objEntity3
        ],
        // Rule configuration
        [
            'entity' => [
                [
                    'validation_entity'
                ],
                [
                    'rule_name' => 'validation_entity_collection'
                ]
            ]
        ]
    ],

    // Test valid entity Ko/valid entity collection Ko
    [
        // Data configuration
        [
            'entity_collection' => $objEntityCollection
        ],
        // Rule configuration
        [
            'entity_collection' => [
                [
                    'validation_entity'
                ],
                [
                    'rule_name' => 'validation_entity_collection'
                ]
            ]
        ]
    ],

    // Test valid entity Ko/valid entity collection Ok
    [
        // Data configuration
        [
            'entity_collection' => $objEntityCollection2
        ],
        // Rule configuration
        [
            'entity_collection' => [
                [
                    'validation_entity'
                ],
                [
                    'rule_name' => 'validation_entity_collection'
                ]
            ]
        ]
    ],

    // Test new entity Ko
    [
        // Data configuration
        [
            'entity' => $objEntity3
        ],
        // Rule configuration
        [
            'entity' => [
                ['new_entity']
            ]
        ]
    ],

    // Test new entity Ok
    [
        // Data configuration
        [
            'entity' => $objUserEntity1
        ],
        // Rule configuration
        [
            'entity' => [
                ['new_entity']
            ]
        ]
    ],

    // Test new entity Ko
    [
        // Data configuration
        [
            'entity' => $objUserEntity1
        ],
        // Rule configuration
        [
            'entity' => [
                [
                    'new_entity',
                    [
                        'new_require' => 0
                    ]
                ]
            ]
        ]
    ],

    // Test new entity collection Ko
    [
        // Data configuration
        [
            'entity_collection' => $objEntityCollection
        ],
        // Rule configuration
        [
            'entity_collection' => [
                ['new_entity_collection']
            ]
        ]
    ],

    // Test new entity collection Ok
    [
        // Data configuration
        [
            'entity_collection' => $objUserEntityCollection
        ],
        // Rule configuration
        [
            'entity_collection' => [
                ['new_entity_collection']
            ]
        ]
    ],

    // Test new entity collection Ko
    [
        // Data configuration
        [
            'entity_collection' => $objUserEntityCollection
        ],
        // Rule configuration
        [
            'entity_collection' => [
                [
                    'new_entity_collection',
                    [
                        'new_require' => 0
                    ]
                ]
            ]
        ]
    ]
);

foreach($tabInfo as $info)
{
    echo('Test validation info: <br />');

    try{
        // Get info
        $tabData = $info[0];
        $tabRuleConfig = $info[1];

        echo('Data: <pre>');var_dump(array_keys($tabData));echo('</pre>');
        //echo('Rule configurations: <pre>');var_dump($tabRuleConfig);echo('</pre>');

        // Get validation
        $tabErrorMessage = array();
        $tabErrorException = array();
        $boolIsValid = $objValidator->checkTabDataIsValid(
            $tabData,
            $tabRuleConfig,
            $tabErrorMessage,
            $tabErrorException
        );
        $tabFormatErrorException = array_map(
            function($tabErrorException)
            {
                return array_map(
                    function($objException)
                    {
                        return (
                        ($objException instanceof Exception) ?
                            [
                                get_class($objException),
                                $objException->getMessage()
                            ]:
                            null
                        );
                    },
                    $tabErrorException
                );
            },
            $tabErrorException
        );

        echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
        echo('Get error messages: <pre>');var_dump($tabErrorMessage);echo('</pre>');
        echo('Get error exceptions: <pre>');var_dump($tabFormatErrorException);echo('</pre>');

        foreach($tabErrorException as $tabDataErrorException)
        {
            if(is_array($tabDataErrorException))
            {
                foreach($tabDataErrorException as $objErrorException)
                {
                    if($objErrorException instanceof ValidationFailException)
                    {
                        echo('Get array of error exceptions info: <pre>');
                        print_r($objErrorException->getTabInfo());
                        echo('</pre>');
                    }
                }
            }
        }
    //*
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    //*/
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test entity collection validation
echo('Test entity collection validation : <br />');

$tabTabRuleConfig = array(
    // Test exist Ko
    [
        'nmFirst' => [
            'entity_collection_attribute_exist',
            []
        ]
    ],

    // Test exist 1 Ok/exist 2 Ko
    function(ClassTest $objEntity) use ($objEntityCollection)
    {
        return array(
            'nmFirst' => [
                [
                    'entity_collection_attribute_exist',
                    [
                        'entity_collection' => $objEntityCollection
                    ]
                ],
                [
                    'rule_name' => 'entity_collection_attribute_exist',
                    'rule_config' => [
                        'entity_collection' => $objEntityCollection,
                        'attribute_key' => 'nmFirst',
                        'exclude' => [
                            'id' => $objEntity->getAttributeValue('id', false)
                        ],
                        'error_message_pattern' => '%1$s not found (exclude id).'
                    ]
                ]
            ]
        );
    },

    // Test exist 1 Ko/exist 2 Ok
    function(ClassTest $objEntity) use ($objEntityCollection)
    {
        return array(
            'nmFirst' => [
                [
                    'entity_collection_attribute_exist',
                    [
                        'entity_collection' => $objEntityCollection,
                        'count_min' => 0,
                        'count_max' => 0,
                        'error_message_pattern' => '%1$s must be unique.'
                    ]
                ],
                [
                    'rule_name' => 'entity_collection_attribute_exist',
                    'rule_config' => [
                        'entity_collection' => $objEntityCollection,
                        'attribute_key' => 'nmFirst',
                        'count_min' => 0,
                        'count_max' => 0,
                        'exclude' => [
                            'id' => $objEntity->getAttributeValue('id', false)
                        ],
                        'error_message_pattern' => '%1$s must be unique (exclude id).'
                    ]
                ]
            ]
        );
    },

    // Test exist Ok
    function(ClassTest $objEntity) use ($objEntityCollection)
    {
        return array(
            'nmFirst' => [
                [
                    'rule_name' => 'entity_collection_attribute_exist',
                    'rule_config' => [
                        'entity_collection' => $objEntityCollection,
                        'attribute_key' => 'nmFirst',
                        'count_min' => 0,
                        'count_max' => 0,
                        'exclude' => [
                            'id' => $objEntity->getAttributeValue('id', false)
                        ],
                        'error_message_pattern' => '%1$s must be unique (exclude id).'
                    ]
                ],
                [
                    'entity_collection_attribute_exist',
                    [
                        'entity_collection' => $objEntityCollection
                    ]
                ]
            ]
        );
    }
);

foreach($tabTabRuleConfig as $tabRuleConfig)
{
    echo('Test validation info: <br />');
    //echo('Rule configurations: <pre>');var_dump($tabRuleConfig);echo('</pre>');

    try{
        $tabError = array();
        $boolIsValid = ToolBoxEntityRule::checkEntityCollectionIsValid(
            $objEntityCollection,
            $objValidator,
            $tabRuleConfig,
            $tabError
        );

        echo('Is valid: <pre>');var_dump($boolIsValid);echo('</pre>');
        echo('Get array of errors: <pre>');print_r($tabError);echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ': ' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


