<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load library test
require_once($strRootAppPath . '/vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

// Use
use liberty_code\model\validation\rule\entity_collection\attribute_exist\model\AttrExistEntityCollectionRule;
use liberty_code\model\validation\rule\validation_entity\model\ValidEntityRule;
use liberty_code\model\validation\rule\validation_entity_collection\model\ValidEntityCollectionRule;
use liberty_code\model\validation\rule\new_entity\model\NewEntityRule;
use liberty_code\model\validation\rule\new_entity_collection\model\NewEntityCollectionRule;



// Init rules
$objAttrExistEntityCollectionRule = new AttrExistEntityCollectionRule();
$objValidEntityRule = new ValidEntityRule();
$objValidEntityCollectionRule = new ValidEntityCollectionRule();
$objNewEntityRule = new NewEntityRule();
$objNewEntityCollectionRule = new NewEntityCollectionRule();



// Add rule
$tabRule = array(
    $objAttrExistEntityCollectionRule,
    $objValidEntityRule,
    $objValidEntityCollectionRule,
    $objNewEntityRule,
    $objNewEntityCollectionRule
);
$objRuleCollection->setTabRule($tabRule);


