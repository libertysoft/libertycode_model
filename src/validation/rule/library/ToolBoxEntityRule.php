<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ToolBoxValidEntity;
use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;



class ToolBoxEntityRule extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified entity is valid,
     * from specified validator,
     * and specified rule configurations.
     * Errors can be returned.
     *
     * Entity attributes are used as data, on validator:
     * - Data name = attribute key.
     * - Data value = attribute value.
     *
     * Rule configurations array format:
     * @see ValidatorInterface::checkTabDataIsValid() rule configurations array format.
     *
     * Errors array format:
     * @see ToolBoxValidEntity::checkEntityIsValid() errors array format.
     *
     * @param EntityInterface $objEntity
     * @param ValidatorInterface $objValidator
     * @param array $tabRuleConfig
     * @param array &$tabError = array()
     * @param null|string|string[] $attrKey = null
     * @return boolean
     */
    public static function checkEntityIsValid(
        EntityInterface $objEntity,
        ValidatorInterface $objValidator,
        array $tabRuleConfig,
        array &$tabError = array(),
        $attrKey = null
    )
    {
        // Get attribute validations
        $tabData = $objEntity->getTabData(false);
        $tabErrorMessage = array();
        $tabErrorException = array();
        $objValidator->checkTabDataIsValid(
            $tabData,
            $tabRuleConfig,
            $tabErrorMessage,
            $tabErrorException
        );

        // Init check attribute is valid function
        $callCheckAttributeIsValid = function($strKey, array &$tabError) use ($tabErrorMessage, $tabErrorException)
        {
            // Get errors
            $tabError = (
                array_key_exists($strKey, $tabErrorMessage) ?
                    $tabErrorMessage[$strKey] :
                    array()
            );
            $tabError = (
                array_key_exists($strKey, $tabErrorException) ?
                    ToolBoxValidEntity::getTabMergeAttributeError(
                        $tabError,
                        $tabErrorException[$strKey]
                    ) :
                    $tabError
            );

            // Return result
            return (count($tabError) === 0);
        };

        // Init var
        $result = ToolBoxValidEntity::checkEntityIsValid(
            $objEntity,
            $callCheckAttributeIsValid,
            $tabError,
            $attrKey
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified entity collection is valid,
     * from specified validator,
     * and specified rule configurations.
     * Errors can be returned.
     *
     * Rule configurations format:
     * - Rule configurations callable:
     *     array function(EntityInterface $objEntity).
     *     Return array format: @see checkEntityCollectionIsValid() rule configurations format (array).
     * - Rule configurations array:
     *     @see checkEntityIsValid() rule configurations array format.
     *
     * Errors array format:
     * @see ToolBoxValidEntity::checkEntityCollectionIsValid() errors array format.
     *
     * Configuration array format:
     * @see ToolBoxValidEntity::checkEntityCollectionIsValid() configuration array format.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param ValidatorInterface $objValidator
     * @param array|callable $tabRuleConfig
     * @param array &$tabError = array()
     * @param null|array $tabConfig = null
     * @param null|string|string[] $attrKey = null
     * @return boolean
     */
    public static function checkEntityCollectionIsValid(
        EntityCollectionInterface $objEntityCollection,
        ValidatorInterface $objValidator,
        $tabRuleConfig,
        array &$tabError = array(),
        array $tabConfig = null,
        $attrKey = null
    )
    {
        // Init check entity function
        $callCheckEntityIsValid = function(EntityInterface $objEntity, array &$tabError)
        use ($objValidator, $tabRuleConfig, $attrKey)
        {
            // Init var
            $tabEntityRuleConfig = (
                is_array($tabRuleConfig) ?
                    $tabRuleConfig :
                    (is_callable($tabRuleConfig) ? $tabRuleConfig($objEntity) : null)
            );
            $result = (
                (!is_array($tabEntityRuleConfig)) ||
                static::checkEntityIsValid(
                    $objEntity,
                    $objValidator,
                    $tabEntityRuleConfig,
                    $tabError,
                    $attrKey
                )
            );

            // Return result
            return $result;
        };

        // Init var
        $result = ToolBoxValidEntity::checkEntityCollectionIsValid(
            $objEntityCollection,
            $callCheckEntityIsValid,
            $tabError,
            $tabConfig
        );

        // Return result
        return $result;
    }



}