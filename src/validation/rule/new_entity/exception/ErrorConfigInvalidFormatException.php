<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\new_entity\exception;

use liberty_code\model\validation\rule\new_entity\library\ConstNewEntityRule;



class ErrorConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstNewEntityRule::EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid new required option
            (
                (!isset($config[ConstNewEntityRule::TAB_ERROR_CONFIG_KEY_NEW_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstNewEntityRule::TAB_ERROR_CONFIG_KEY_NEW_REQUIRE]) ||
                    is_int($config[ConstNewEntityRule::TAB_ERROR_CONFIG_KEY_NEW_REQUIRE]) ||
                    (
                        is_string($config[ConstNewEntityRule::TAB_ERROR_CONFIG_KEY_NEW_REQUIRE]) &&
                        ctype_digit($config[ConstNewEntityRule::TAB_ERROR_CONFIG_KEY_NEW_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}