<?php
/**
 * Description :
 * This class allows to define new entity rule class.
 * New entity rule allows to check if specified data is new entity or not.
 *
 * New entity rule uses the following specified configuration:
 * [
 *     Default rule configuration,
 *
 *     error_message_pattern(optional):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name,
 *         '%2$s' replaced by data value,
 *         '%3$s' replaced by entity status new required (boolean value)."
 * ]
 *
 * New entity rule uses the following specified validation configuration:
 * [
 *     Default rule validation configuration,
 *
 *     new_require(optional: got true if not found): true / false
 * ]
 *
 * New entity rule uses the following specified error configuration:
 * [
 *     Default rule error configuration,
 *
 *     new_require(optional: got true if not found): true / false,
 *
 *     error_message_pattern(optional: got 'error_message_pattern' from configuration if not found):
 *         "string sprintf pattern,
 *         to build an error message from specified data,
 *         where '%1$s' replaced by data name,
 *         '%2$s' replaced by data value,
 *         '%3$s' replaced by entity status new required (boolean value)."
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\new_entity\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\validation\rule\new_entity\library\ConstNewEntityRule;
use liberty_code\model\validation\rule\new_entity\exception\ValidConfigInvalidFormatException;
use liberty_code\model\validation\rule\new_entity\exception\ErrorConfigInvalidFormatException;



class NewEntityRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check new required.
     *
     * Configuration format:
     * Validation|error configuration can be provided.
     * @see NewEntityRule
     *
     * @param array $tabConfig = null
     * @param string $strConfigKey = ConstNewEntityRule::TAB_VALID_CONFIG_KEY_NEW_REQUIRE
     * @return boolean
     */
    protected function checkNewRequired(
        array $tabConfig = null,
        $strConfigKey = ConstNewEntityRule::TAB_VALID_CONFIG_KEY_NEW_REQUIRE
    )
    {
        // Init var
        $strConfigKey = (
            is_string($strConfigKey) ?
                $strConfigKey :
                ConstNewEntityRule::TAB_VALID_CONFIG_KEY_NEW_REQUIRE
        );
        $result = (
            (!is_array($tabConfig)) ||
            (!array_key_exists($strConfigKey, $tabConfig)) ||
            (intval($tabConfig[$strConfigKey]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $boolNew = $this->checkNewRequired(
            $tabConfig,
            ConstNewEntityRule::TAB_VALID_CONFIG_KEY_NEW_REQUIRE
        );
        $result = (
            ($value instanceof SaveEntityInterface) ?
                $value->checkIsNew() == $boolNew :
                false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstNewEntityRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstNewEntityRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * @inheritdoc
     * @throws ErrorConfigInvalidFormatException
     */
    protected function getStrErrorMessageEngine($strErrorMsgPattern, $strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ErrorConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $strValue = (
            ToolBoxString::checkConvertString($value) ?
                strval($value) :
                ConstRule::ERROR_MESSAGE_PATTERN_ARG_DEFAULT
        );
        $boolNew = $this->checkNewRequired(
            $tabConfig,
            ConstNewEntityRule::TAB_ERROR_CONFIG_KEY_NEW_REQUIRE
        );
        $strNew = ($boolNew ? 'true' : 'false');
        $result = sprintf($strErrorMsgPattern, $strName, $strValue, $strNew);

        // Return result
        return $result;
    }



}