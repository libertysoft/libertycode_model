<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\new_entity_collection\library;



class ConstNewEntityCollectionRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'new_entity_collection';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s must be an entity collection, where each specified entity has a valid status new (%3$s).';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_NEW_REQUIRE = 'new_require';
    const TAB_VALID_CONFIG_KEY_COLLECTION_CONFIG = 'collection_config';

    // Error configuration
    const TAB_ERROR_CONFIG_KEY_NEW_REQUIRE = 'new_require';



    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the new entity collection rule validation configuration standard.';
    const EXCEPT_MSG_ERROR_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the new entity collection rule error configuration standard.';
}