<?php
/**
 * Description :
 * This class allows to define validation entity rule class.
 * Validation entity rule allows to check if specified data is entity,
 * which successes validation.
 *
 * Validation entity rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Validation entity rule uses the following specified validation configuration:
 * [
 *     Default rule validation configuration,
 *
 *     attribute_key(optional):
 *         "string attribute key" OR ["string attribute key 1", ... "string attribute key N"]
 * ]
 *
 * Validation entity rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\validation_entity\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\validation\rule\library\ConstRule;
use liberty_code\validation\rule\exception\ValidationFailException;
use liberty_code\model\entity\api\ValidEntityInterface;
use liberty_code\model\validation\rule\validation_entity\library\ConstValidEntityRule;
use liberty_code\model\validation\rule\validation_entity\exception\ValidConfigInvalidFormatException;



class ValidEntityRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $attrKey = (
            isset($tabConfig[ConstValidEntityRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY]) ?
                $tabConfig[ConstValidEntityRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY] :
                null
        );
        $result = (
            ($value instanceof ValidEntityInterface) ?
                $value->checkAttributeValid($attrKey) :
                false
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstValidEntityRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN => ConstValidEntityRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



    /**
     * @inheritdoc
     */
    public function getObjErrorException(
        $strName,
        $value,
        array $tabConfig = null
    )
    {
        // Init var
        $result = null;
        $attrKey = (
            isset($tabConfig[ConstValidEntityRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY]) ?
                $tabConfig[ConstValidEntityRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY] :
                null
        );
        $strErrorMsg = $this->getStrErrorMessage($strName, $value, $tabConfig);
        $tabError = null;
        if($value instanceof ValidEntityInterface)
        {
            $tabError = array();
            $value->checkAttributeValid($attrKey, $tabError);
        }

        // Get error, if required
        if(
            // Check error message found
            (!is_null($strErrorMsg)) ||

            // Check array of error found
            (!is_null($tabError))
        )
        {
            $tabInfo = (
                (is_array($tabError)) ?
                    array(ConstValidEntityRule::TAB_ERROR_INFO_KEY_ENTITY_ERROR => $tabError) :
                    array()
            );
            $result = new ValidationFailException(
                $strErrorMsg,
                $this,
                $strName,
                $value,
                $tabConfig,
                $tabInfo
            );
        }

        // Return result
        return $result;
    }



}