<?php
/**
 * Description :
 * This class allows to define entity collection rule class.
 * Entity collection rule allows to check specified data validation,
 * using entity collection.
 *
 * Entity collection rule uses the following specified configuration:
 * [
 *     Default rule configuration
 * ]
 *
 * Entity collection rule uses the following specified validation configuration:
 * [
 *     Default rule validation configuration,
 *
 *     entity_collection(required): entity collection object
 * ]
 *
 * Entity collection rule uses the following specified error configuration:
 * [
 *     Default rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\entity_collection\model;

use liberty_code\validation\rule\model\DefaultRule;

use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\validation\rule\entity_collection\library\ConstEntityCollectionRule;
use liberty_code\model\validation\rule\entity_collection\exception\ValidConfigInvalidFormatException;



abstract class EntityCollectionRule extends DefaultRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified data is valid,
     * from specified configuration.
     *
     * Configuration format:
     * @see SqlRule validation configuration.
     *
     * @param string $strName
     * @param mixed $value
     * @param array $tabConfig
     * @return boolean
     */
    abstract protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabConfig
    );



    /**
     * @inheritdoc
     */
    public function checkIsValid($strName, $value, array $tabConfig = null)
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $result = false;

        // Check configuration found
        if(is_array($tabConfig))
        {
            // Check valid data
            $result = $this->checkIsValidEngine(
                $strName,
                $value,
                $tabConfig
            );
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity collection object,
     * from specified configuration.
     *
     * Configuration format:
     * @see EntityCollectionRule validation configuration.
     *
     * @param array $tabConfig
     * @return EntityCollectionInterface
     */
    protected function getObjEntityCollection(array $tabConfig)
    {
        // Init var
        $tabValidConfig = $tabConfig;
        $result = $tabValidConfig[ConstEntityCollectionRule::TAB_VALID_CONFIG_KEY_ENTITY_COLLECTION];

        // Return result
        return $result;
    }



}