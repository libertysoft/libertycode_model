<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\entity_collection\attribute_exist\library;



class ConstAttrExistEntityCollectionRule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONFIG_DEFAULT_VALUE_KEY = 'entity_collection_attribute_exist';
    const CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN = '%1$s not found.';

    // Validation configuration
    const TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY = 'attribute_key';
    const TAB_VALID_CONFIG_KEY_COUNT_MIN = 'count_min';
    const TAB_VALID_CONFIG_KEY_COUNT_MAX = 'count_max';
    const TAB_VALID_CONFIG_KEY_INCLUDE = 'include';
    const TAB_VALID_CONFIG_KEY_EXCLUDE = 'exclude';



    // Exception message constants
    const EXCEPT_MSG_VALID_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the entity collection attribute exist rule validation configuration standard.';
}