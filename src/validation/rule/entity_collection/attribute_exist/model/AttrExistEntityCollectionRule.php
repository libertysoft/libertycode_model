<?php
/**
 * Description :
 * This class allows to define attribute exist entity collection rule class.
 * Attribute exist entity collection rule allows to check if specified data exists,
 * in specified entity collection.
 *
 * Attribute exist entity collection rule uses the following specified configuration:
 * [
 *     Entity collection rule configuration
 * ]
 *
 * Attribute exist entity collection rule uses the following specified validation configuration:
 * [
 *     Entity collection rule validation configuration,
 *
 *     attribute_key(optional: got data name, if not found): "string attribute key",
 *
 *     count_min(optional: got 1, if not found): positive integer,
 *
 *     count_max(optional): positive integer,
 *
 *     include(optional: got [], if not found): [
 *         // Value or list of values 1 to include in exist check
 *         attribute_key(required): mixed value OR [mixed value 1, ..., mixed value N],
 *
 *         ...,
 *
 *         // Value or list of values N to include in exist check
 *         ...
 *     ],
 *
 *     exclude(optional: got [], if not found): [
 *         // Value or list of values 1 to exclude from exist check
 *         attribute_key(required): mixed value OR [mixed value 1, ..., mixed value N],
 *
 *         ...,
 *
 *         // Value or list of values N to exclude from exist check
 *         ...
 *     ]
 * ]
 *
 * Attribute exist entity collection rule uses the following specified error configuration:
 * [
 *     Entity collection rule error configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\validation\rule\entity_collection\attribute_exist\model;

use liberty_code\model\validation\rule\entity_collection\model\EntityCollectionRule;

use liberty_code\model\entity\api\EntityInterface;
use liberty_code\validation\rule\library\ConstRule;
use liberty_code\model\validation\rule\entity_collection\attribute_exist\library\ConstAttrExistEntityCollectionRule;
use liberty_code\model\validation\rule\entity_collection\attribute_exist\exception\ValidConfigInvalidFormatException;



class AttrExistEntityCollectionRule extends EntityCollectionRule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkIsValidEngine(
        $strName,
        $value,
        array $tabConfig
    )
    {
        // Set check argument
        ValidConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $objEntityCollection = $this->getObjEntityCollection($tabConfig);
        $strAttrKey = (
            isset($tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY]) ?
                $tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_ATTRIBUTE_KEY] :
                $strName
        );
        $intCountMin = (
        isset($tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_COUNT_MIN]) ?
            $tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_COUNT_MIN] :
            1
        );
        $intCountMax = (
        isset($tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_COUNT_MAX]) ?
            $tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_COUNT_MAX] :
            null
        );
        $tabInclude = (
            isset($tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_INCLUDE]) ?
                $tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_INCLUDE] :
                array()
        );
        $tabExclude = (
            isset($tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_EXCLUDE]) ?
                $tabConfig[ConstAttrExistEntityCollectionRule::TAB_VALID_CONFIG_KEY_EXCLUDE] :
                array()
        );

        // Init check scope condition function
        $checkScopeConfig = function(
            array $tabScopeConfig,
            EntityInterface $objEntity,
            $boolExclude
        )
        {
            $result = true;

            // Run each attribute, from scope
            $tabAttrKey = array_keys($tabScopeConfig);
            for($intCpt = 0; ($intCpt < count($tabAttrKey)) && $result; $intCpt++)
            {
                // Get attribute info
                $strAttrKey = $tabAttrKey[$intCpt];
                $attrValue = $tabScopeConfig[$strAttrKey];

                // Check attribute selected
                $result = (
                    $objEntity->checkAttributeExists($strAttrKey) &&
                    ($objEntity->getAttributeValue($strAttrKey) === $attrValue)
                );
                $result = (
                    ($boolExclude && (!$result)) ||
                    ((!$boolExclude) && $result)
                );
            }

            return $result;
        };

        // Run each entity
        $countAttrValue = 0;
        $tabEntityKey = $objEntityCollection->getTabKey();
        foreach($tabEntityKey as $strEntityKey)
        {
            $objEntity = $objEntityCollection->getItem($strEntityKey);

            // Count value, if required
            $countAttrValue = (
                (
                    $checkScopeConfig($tabInclude, $objEntity, false) &&
                    $checkScopeConfig($tabExclude, $objEntity, true) &&
                    $objEntity->checkAttributeExists($strAttrKey) &&
                    ($objEntity->getAttributeValue($strAttrKey) === $value)
                ) ?
                    ($countAttrValue + 1) :
                    $countAttrValue
            );
        }

        // Check data found
        $result = (
            ($countAttrValue >= $intCountMin) &&
            (
                is_null($intCountMax) ||
                ($countAttrValue <= $intCountMax)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstRule::TAB_CONFIG_KEY_KEY => ConstAttrExistEntityCollectionRule::CONFIG_DEFAULT_VALUE_KEY,
            ConstRule::TAB_CONFIG_KEY_ERROR_MESSAGE_PATTERN =>
                ConstAttrExistEntityCollectionRule::CONFIG_DEFAULT_VALUE_ERROR_MESSAGE_PATTERN
        );
    }



}