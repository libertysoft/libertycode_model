<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\model\datetime\factory\library\ConstDateTimeFactory;
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;



// Init var
$tabConfig = array(
    // ConstDateTimeFactory::TAB_CONFIG_KEY_REF_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME => 'Europe/Paris',
    ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT => array(
        'd/m/Y H:i:s',
        'd/m/Y'
    ),
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME => 'America/New_York',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT => //'m/d/Y H:i:s',
    //*
    array(
        'long' => 'm/d/Y H:i:s',
        'short' => 'm/d/Y'
    ),
    //*/
    ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT_BASE_KEY => 'short',
    // ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME => 'UTC',
    ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT => 'Y-m-d H:i:s.u'
);
$objDateTimeFactory = new DefaultDateTimeFactory($tabConfig);


