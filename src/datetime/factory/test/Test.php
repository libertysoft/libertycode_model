<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/datetime/factory/test/DateTimeFactoryTest.php');



// Test datetime set/get
$tabFormat = array(
    '07/11/1987 09:27:03', // Ok
	'test', // Ko: invalid format
	'07/01/1984 17:06:29', // Ok
    7, // Ko: invalid format
    '02/21/2020', // Ok
    '05-17-2025 23:48:35', // Ko: not found
    '05/17/2023 14:12:37' // Ok
);

foreach($tabFormat as $strFormat)
{
	echo('Test datetime set/get format "'.strval($strFormat).'": <br />');
	try{
        $objRefDt = $objDateTimeFactory->getObjDtFromSet($strFormat);
        echo('Get reference datetime object: <pre>');var_dump($objRefDt);echo('</pre>');

        if(!is_null($objRefDt))
        {
            $strGetDt = $objDateTimeFactory->getStrGetDt($objRefDt);
            $strSetDt = $objDateTimeFactory->getStrSetDt($objRefDt);
            $objRefDt = $objDateTimeFactory->getObjDtFromGet($strGetDt);
            echo('Get string get datetime: <pre>');var_dump($strGetDt);echo('</pre>');
            echo('Get string set datetime: <pre>');var_dump($strSetDt);echo('</pre>');
            echo('Get reference datetime object: <pre>');var_dump($objRefDt);echo('</pre>');
        }

	} catch(\Exception $e) {
		echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
		echo('<br />');
	}
	echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test datetime save
$tabFormat = array(
    '1987-07-11 09:27:03.0', // Ok
    'test', // Ko: invalid format
    '1984-07-01 17:06:29.0', // Ok
    7, // Ko: invalid format
    '2020-02-21 22:11:57.0', // Ok
    '2025/05/17 23:48:35.0', // Ko: not found
    '2023-05-17 14:12:37.0' // Ok
);

foreach($tabFormat as $strFormat)
{
    echo('Test datetime save format "'.strval($strFormat).'": <br />');
    try{
        $objRefDt = $objDateTimeFactory->getObjDtFromSave($strFormat);
        echo('Get reference datetime object: <pre>');var_dump($objRefDt);echo('</pre>');

        if(!is_null($objRefDt))
        {
            $strSaveDt = $objDateTimeFactory->getStrSaveDt($objRefDt);
            echo('Get string save datetime: <pre>');var_dump($strSaveDt);echo('</pre>');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


