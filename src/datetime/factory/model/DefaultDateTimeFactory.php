<?php
/**
 * Description :
 * This class allows to define default datetime factory class.
*  Can be consider is base of all datetime factory type.
 *
 * Default datetime factory uses the following specified configuration, to get datetime:
 * [
 *     ref_timezone_name(optional: got @see ConstDateTimeFactory::CONFIG_DEFAULT_VALUE_TIMEZONE_NAME, if not found):
 *         "string timezone name, for reference datetime",
 *
 *     get_timezone_name(optional: got @see date_default_timezone_get(), if not found):
 *         "string timezone name, for get datetime",
 *
 *     get_datetime_format(optional: got @see ConstDateTimeFactory::CONFIG_DEFAULT_VALUE_DATETIME_FORMAT, if not found):
 *         "string datetime format, for get datetime"
 *         OR
 *         ["string datetime format 1", ... "string datetime format N"],
 *
 *     get_datetime_format_base_key(optional: got 0, if not found. Only used if get_datetime_format is array):
 *         integer index or string key.
 *
 *     set_timezone_name(optional: got @see date_default_timezone_get(), if not found):
 *         "string timezone name, for set datetime",
 *
 *     set_datetime_format(optional: got @see ConstDateTimeFactory::CONFIG_DEFAULT_VALUE_DATETIME_FORMAT, if not found):
 *         "string datetime format, for set datetime"
 *         OR
 *         ["string datetime format 1", ... "string datetime format N"],
 *
 *     set_datetime_format_base_key(optional: got 0, if not found. Only used if set_datetime_format is array):
 *         integer index or string key.
 *
 *     save_timezone_name(optional: got 'ref_timezone_name', if not found):
 *         "string timezone name, for save datetime",
 *
 *     save_datetime_format(optional: got @see ConstDateTimeFactory::CONFIG_DEFAULT_VALUE_DATETIME_FORMAT, if not found):
 *         "string datetime format, for save datetime"
 * ]
 *
 * Note:
 * -> Datetime format base:
 * Datetime format base is used on features,
 * when array of datetime formats provided in configuration,
 * and only one specific datetime format required.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\datetime\factory\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;

use DateTime;
use DateTimeZone;
use liberty_code\model\datetime\factory\library\ConstDateTimeFactory;
use liberty_code\model\datetime\factory\exception\ConfigInvalidFormatException;



class DefaultDateTimeFactory extends FixBean implements DateTimeFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabConfig
     */
    public function __construct(array $tabConfig)
    {
        // Call parent constructor
        parent::__construct();

        // Init configuration
        $this->setConfig($tabConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        if(!$this->beanExists(ConstDateTimeFactory::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstDateTimeFactory::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDateTimeFactory::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDateTimeFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstDateTimeFactory::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get datetime format base.
     *
     * @param string|string[] $format
     * @param integer|string $key = null
     * @return string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrFormatBase($format, $key = null)
    {
        // Init var
        $tabFormat = (is_array($format) ? $format : array($format));
        $tabFormat = (
            ((is_int($key) || (count($tabFormat) == 1))) ?
                array_values($tabFormat) :
                $tabFormat
        );
        $result = (
            (count($tabFormat) == 1) ?
                $tabFormat[0] :
                (
                    ((!is_null($key)) && isset($tabFormat[$key])) ?
                        $tabFormat[$key] :
                        null
                )
        );

        // Check format found
        if(is_null($result))
        {
            $tabConfig = $this->getTabConfig();
            throw new ConfigInvalidFormatException(serialize($tabConfig));
        }

        // Return result
        return $result;
    }



    /**
     * Get timezone name,
     * for reference datetime.
     *
     * @return string
     */
    public function getStrRefTz()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_REF_TIMEZONE_NAME]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_REF_TIMEZONE_NAME] :
                ConstDateTimeFactory::CONFIG_DEFAULT_VALUE_TIMEZONE_NAME
        );

        // Return result
        return $result;
    }



    /**
     * Get timezone name,
     * for get datetime.
     *
     * @return string
     */
    public function getStrGetTz()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME] :
                date_default_timezone_get()
        );

        // Return result
        return $result;
    }



    /**
     * Get index or associative array of datetime formats,
     * for get datetime.
     *
     * @return string[]
     */
    public function getTabGetFormat()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT] :
                ConstDateTimeFactory::CONFIG_DEFAULT_VALUE_DATETIME_FORMAT
        );
        $result = (
            is_array($result) ?
                $result :
                array($result)
        );

        // Return result
        return $result;
    }



    /**
     * Get datetime format base key,
     * for get datetime.
     *
     * @return integer|string
     */
    protected function getGetFormatBaseKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT_BASE_KEY]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT_BASE_KEY] :
                0
        );

        // Return result
        return $result;
    }



    /**
     * Get datetime format base,
     * for get datetime.
     *
     * @return string
     * @throws ConfigInvalidFormatException
     */
    public function getStrGetFormatBase()
    {
        // Return result
        return $this->getStrFormatBase(
            $this->getTabGetFormat(),
            $this->getGetFormatBaseKey()
        );
    }



    /**
     * Get timezone name,
     * for set datetime.
     *
     * @return string
     */
    public function getStrSetTz()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME] :
                date_default_timezone_get()
        );

        // Return result
        return $result;
    }



    /**
     * Get index or associative array of datetime formats,
     * for set datetime.
     *
     * @return string[]
     */
    public function getTabSetFormat()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT] :
                ConstDateTimeFactory::CONFIG_DEFAULT_VALUE_DATETIME_FORMAT
        );
        $result = (
            is_array($result) ?
                $result :
                array($result)
        );

        // Return result
        return $result;
    }



    /**
     * Get datetime format base key,
     * for set datetime.
     *
     * @return integer|string
     */
    protected function getSetFormatBaseKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT_BASE_KEY]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT_BASE_KEY] :
                0
        );

        // Return result
        return $result;
    }



    /**
     * Get datetime format base,
     * for set datetime.
     *
     * @return string
     * @throws ConfigInvalidFormatException
     */
    public function getStrSetFormatBase()
    {
        // Return result
        return $this->getStrFormatBase(
            $this->getTabSetFormat(),
            $this->getSetFormatBaseKey()
        );
    }



    /**
     * Get timezone name,
     * for save datetime.
     *
     * @return string
     */
    public function getStrSaveTz()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME] :
                $this->getStrRefTz()
        );

        // Return result
        return $result;
    }



    /**
     * Get datetime format,
     * for save datetime.
     *
     * @return string
     */
    public function getStrSaveFormat()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT]) ?
                $tabConfig[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT] :
                ConstDateTimeFactory::CONFIG_DEFAULT_VALUE_DATETIME_FORMAT
        );

        // Return result
        return $result;
    }





    // Methods datetime getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjRefDt(DateTime $objDt)
    {
        // Init var
        $objTz = new DateTimeZone($this->getStrRefTz());
        $result = clone $objDt;

        // Set timezone
        $result->setTimezone($objTz);

        // Return result
        return $result;
    }



    /**
     * Get datetime object,
     * from specified string datetime.
     *
     * @param string $strDt
     * @param string|string[] $format
     * @param string $strTz
     * @param boolean $boolRef = true
     * @return null|DateTime
     */
    protected function getObjDtFromStrDt($strDt, $format, $strTz, $boolRef = true)
    {
        // Init var
        $result = null;

        // Check argument valid
        if(is_string($strDt))
        {
            // Init var
            $tabFormat = (is_array($format) ? array_values($format) : array($format));
            $boolRef = (is_bool($boolRef) ? $boolRef : true);
            $objTz = new DateTimeZone($strTz);

            // Get datetime object
            $objDt = false;
            for($intCpt = 0; ($intCpt < count($tabFormat)) && (!($objDt instanceof DateTime)); $intCpt++)
            {
                $strFormat = $tabFormat[$intCpt];
                $objDt = DateTime::createFromFormat($strFormat, $strDt, $objTz);
            }

            // Register datetime object, if required
            $result = (
                ($objDt instanceof DateTime) ?
                    ($boolRef ? $this->getObjRefDt($objDt) : $objDt) :
                    $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjDtFromGet($strDt, $boolRef = true)
    {
        // Return result
        return $this->getObjDtFromStrDt(
            $strDt,
            $this->getTabGetFormat(),
            $this->getStrGetTz(),
            $boolRef
        );
    }



    /**
     * @inheritdoc
     */
    public function getObjDtFromSet($strDt, $boolRef = true)
    {
        // Return result
        return $this->getObjDtFromStrDt(
            $strDt,
            $this->getTabSetFormat(),
            $this->getStrSetTz(),
            $boolRef
        );
    }



    /**
     * @inheritdoc
     */
    public function getObjDtFromSave($strDt, $boolRef = true)
    {
        // Return result
        return $this->getObjDtFromStrDt(
            $strDt,
            $this->getStrSaveFormat(),
            $this->getStrSaveTz(),
            $boolRef
        );
    }



    /**
     * Get string datetime,
     * from specified datetime object.
     *
     * @param DateTime $objDt
     * @param string $strFormat
     * @param string $strTz
     * @return string
     */
    protected function getStrDt(DateTime $objDt, $strFormat, $strTz)
    {
        // Init var
        $objDt = clone $objDt;
        $objTz = new DateTimeZone($strTz);

        // Set timezone
        $objDt->setTimezone($objTz);

        // Get string datetime
        $result = $objDt->format($strFormat);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrGetDt(DateTime $objDt)
    {
        // Return result
        return $this->getStrDt(
            $objDt,
            $this->getStrGetFormatBase(),
            $this->getStrGetTz()
        );
    }



    /**
     * @inheritdoc
     */
    public function getStrSetDt(DateTime $objDt)
    {
        // Return result
        return $this->getStrDt(
            $objDt,
            $this->getStrSetFormatBase(),
            $this->getStrSetTz()
        );
    }



    /**
     * @inheritdoc
     */
    public function getStrSaveDt(DateTime $objDt)
    {
        // Return result
        return $this->getStrDt(
            $objDt,
            $this->getStrSaveFormat(),
            $this->getStrSaveTz()
        );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstDateTimeFactory::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



}