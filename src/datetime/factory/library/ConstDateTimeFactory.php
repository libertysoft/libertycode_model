<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\datetime\factory\library;



class ConstDateTimeFactory
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_REF_TIMEZONE_NAME = 'ref_timezone_name';
    const TAB_CONFIG_KEY_GET_TIMEZONE_NAME = 'get_timezone_name';
    const TAB_CONFIG_KEY_GET_DATETIME_FORMAT = 'get_datetime_format';
    const TAB_CONFIG_KEY_GET_DATETIME_FORMAT_BASE_KEY = 'get_datetime_format_base_key';
    const TAB_CONFIG_KEY_SET_TIMEZONE_NAME = 'set_timezone_name';
    const TAB_CONFIG_KEY_SET_DATETIME_FORMAT = 'set_datetime_format';
    const TAB_CONFIG_KEY_SET_DATETIME_FORMAT_BASE_KEY = 'set_datetime_format_base_key';
    const TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME = 'save_timezone_name';
    const TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT = 'save_datetime_format';

    const CONFIG_DEFAULT_VALUE_TIMEZONE_NAME = 'UTC';
    const CONFIG_DEFAULT_VALUE_DATETIME_FORMAT = 'Y-m-d H:i:s';


	
    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default datetime factory configuration standard.';



}