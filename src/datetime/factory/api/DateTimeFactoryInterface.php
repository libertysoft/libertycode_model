<?php
/**
 * Description :
 * This class allows to describe behavior of datetime factory class.
 * Datetime factory allows to provide new datetime instances,
 * from specified configuration,
 * can be used on entities attributes, repositories, ....
 *
 * Note:
 * -> Datetime types:
 * - Set datetime: used by external libraries, to set datetime.
 * - Get datetime: used by external libraries, to render datetime.
 * - Reference datetime: used by internal components (from this library).
 * - Save datetime: used to load and save datetime, from storage support.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\datetime\factory\api;

use DateTime;



interface DateTimeFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();





    // Methods datetime getters
    // ******************************************************************************

    /**
     * Get reference datetime object,
     * from specified datetime object.
     *
     * @param DateTime $objDt
     * @return DateTime
     */
    public function getObjRefDt(DateTime $objDt);



    /**
     * Get datetime object,
     * from specified string get datetime.
     * If reference option is true, it return reference datetime object.
     *
     * @param string $strDt
     * @param boolean $boolRef = true
     * @return null|DateTime
     */
    public function getObjDtFromGet($strDt, $boolRef = true);



    /**
     * Get datetime object,
     * from specified string set datetime.
     * If reference option is true, it return reference datetime object.
     *
     * @param string $strDt
     * @param boolean $boolRef = true
     * @return null|DateTime
     */
    public function getObjDtFromSet($strDt, $boolRef = true);



    /**
     * Get datetime object,
     * from specified string save datetime.
     * If reference option is true, it return reference datetime object.
     *
     * @param string $strDt
     * @param boolean $boolRef = true
     * @return null|DateTime
     */
    public function getObjDtFromSave($strDt, $boolRef = true);



    /**
     * Get string get datetime,
     * from specified datetime object.
     *
     * @param DateTime $objDt
     * @return string
     */
    public function getStrGetDt(DateTime $objDt);



    /**
     * Get string set datetime,
     * from specified datetime object.
     *
     * @param DateTime $objDt
     * @return string
     */
    public function getStrSetDt(DateTime $objDt);



    /**
     * Get string save datetime,
     * from specified datetime object.
     *
     * @param DateTime $objDt
     * @return string
     */
    public function getStrSaveDt(DateTime $objDt);





    // Methods setters
    // ******************************************************************************

    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);
}