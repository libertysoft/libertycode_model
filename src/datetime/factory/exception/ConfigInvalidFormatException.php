<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\datetime\factory\exception;

use liberty_code\model\datetime\factory\library\ConstDateTimeFactory;



class ConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstDateTimeFactory::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid reference timezone name
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_REF_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_REF_TIMEZONE_NAME]) &&
                    (trim($config[ConstDateTimeFactory::TAB_CONFIG_KEY_REF_TIMEZONE_NAME]) != '') &&
                    in_array(
                        $config[ConstDateTimeFactory::TAB_CONFIG_KEY_REF_TIMEZONE_NAME],
                        timezone_identifiers_list()
                    )
                )
            ) &&

            // Check valid get timezone name
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME]) &&
                    (trim($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME]) != '') &&
                    in_array(
                        $config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_TIMEZONE_NAME],
                        timezone_identifiers_list()
                    )
                )
            ) &&

            // Check valid get datetime format
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT])) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT]) &&
                    (trim($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT]) != '')
                ) ||
                (
                    is_array($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT]) &&
                    (count($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT]) > 0) &&
                    $checkTabStrIsValid($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT])
                )
            ) &&

            // Check valid get datetime format base key
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT_BASE_KEY])) ||
                (
                    is_integer($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT]) &&
                    ($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT] >= 0)
                ) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_GET_DATETIME_FORMAT])
                )
            ) &&

            // Check valid set timezone name
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME]) &&
                    (trim($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME]) != '') &&
                    in_array(
                        $config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_TIMEZONE_NAME],
                        timezone_identifiers_list()
                    )
                )
            ) &&

            // Check valid set datetime format
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT])) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT]) &&
                    (trim($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT]) != '')
                ) ||
                (
                    is_array($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT]) &&
                    (count($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT]) > 0) &&
                    $checkTabStrIsValid($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT])
                )
            ) &&

            // Check valid set datetime format base key
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT_BASE_KEY])) ||
                (
                    is_integer($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT_BASE_KEY]) &&
                    ($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT_BASE_KEY] >= 0)
                ) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SET_DATETIME_FORMAT_BASE_KEY])
                )
            ) &&

            // Check valid save timezone name
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME])) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME]) &&
                    (trim($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME]) != '') &&
                    in_array(
                        $config[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_TIMEZONE_NAME],
                        timezone_identifiers_list()
                    )
                )
            ) &&

            // Check valid save datetime format
            (
                (!isset($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT])) ||
                (
                    is_string($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT]) &&
                    (trim($config[ConstDateTimeFactory::TAB_CONFIG_KEY_SAVE_DATETIME_FORMAT]) != '')
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}