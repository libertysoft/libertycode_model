<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\model\datetime\library;

use liberty_code\library\instance\model\Multiton;

use DateTime;
use liberty_code\model\entity\library\ToolBoxEntity;
use liberty_code\model\entity\api\EntityInterface;
use liberty_code\model\entity\repository\api\SaveEntityInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;



class ToolBoxDateTime extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate attribute datetime create and update,
     * on specified entity.
     * Uses datetime now, if no datetime provided.
     *
     * Attribute key date create and update format:
     * @see ToolBoxEntity::setEntityAttrValue() attribute key format.
     *
     * Check set callable format:
     * @see ToolBoxEntity::setEntityAttrValue() check set callable format.
     *
     * @param EntityInterface $objEntity
     * @param string|array $attrKeyDtCreate
     * @param string|array $attrKeyDtUpdate
     * @param DateTime $objDt = null
     * @param callable $callCheckSet = null
     */
    public static function hydrateEntityAttrDtCreateUpdate(
        EntityInterface $objEntity,
        $attrKeyDtCreate,
        $attrKeyDtUpdate,
        DateTime $objDt = null,
        $callCheckSet = null
    )
    {
        // Init var
        $objDt = (is_null($objDt) ? new DateTime() : $objDt);

        // Set datetime create on entity
        ToolBoxEntity::setEntityAttrValue(
            $objEntity,
            $attrKeyDtCreate,
            $objDt,
            function (EntityInterface $objEntity, $strAttrKey) use ($callCheckSet)
            {
                return (
                    ($objEntity instanceof SaveEntityInterface) &&
                    $objEntity->checkIsNew() &&
                    (
                        (!is_callable($callCheckSet)) ||
                        $callCheckSet($objEntity, $strAttrKey)
                    )
                );
            }
        );

        // Set datetime update on entity
        ToolBoxEntity::setEntityAttrValue(
            $objEntity,
            $attrKeyDtUpdate,
            $objDt,
            $callCheckSet
        );
    }



    /**
     * Hydrate attribute datetime create and update,
     * on specified entity collection.
     * Uses datetime now, if no datetime provided.
     *
     * Attribute key date create and update format:
     * @see ToolBoxEntity::setEntityCollectionAttrValue() attribute key datetime format.
     *
     * Check set callable format:
     * @see ToolBoxEntity::setEntityCollectionAttrValue() check set callable format.
     *
     * @param EntityCollectionInterface $objEntityCollection
     * @param string|array $attrKeyDtCreate
     * @param string|array $attrKeyDtUpdate
     * @param DateTime $objDt = null
     * @param callable $callCheckSet = null
     */
    public static function hydrateEntityCollectionAttrDtCreateUpdate(
        EntityCollectionInterface $objEntityCollection,
        $attrKeyDtCreate,
        $attrKeyDtUpdate,
        DateTime $objDt = null,
        $callCheckSet = null
    )
    {
        // Init var
        $objDt = (is_null($objDt) ? new DateTime() : $objDt);

        // Set datetime create on entity
        ToolBoxEntity::setEntityCollectionAttrValue(
            $objEntityCollection,
            $attrKeyDtCreate,
            $objDt,
            function (EntityInterface $objEntity, $strAttrKey) use ($callCheckSet)
            {
                return (
                    ($objEntity instanceof SaveEntityInterface) &&
                    $objEntity->checkIsNew() &&
                    (
                        (!is_callable($callCheckSet)) ||
                        $callCheckSet($objEntity, $strAttrKey)
                    )
                );
            }
        );

        // Set datetime update on entity
        ToolBoxEntity::setEntityCollectionAttrValue(
            $objEntityCollection,
            $attrKeyDtUpdate,
            $objDt,
            $callCheckSet
        );
    }



}