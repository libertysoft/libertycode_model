LibertyCode_Model
=================



Version "1.0.0"
---------------

- Create repository

- Set entity

- Set persistence

- Set repository

---



Version "1.0.1"
---------------

- Update entity

    - Update entity: remove attributes configuration
    - Set valid entity
    - Set configured entity
    - Update save entity: use valid entity
    - Set save configured entity
    - Remove item save configured entity
    - Update entity collection: remove entities validation
    - Set valid entity collection

- Update repository

    - Update repository: remove entity validation
    - Update sub-repository: remove entity validation
    - Update simple repository: remove entity validation
    - Update multi repository: remove entity validation
    - Update collection repository: remove entities validation
    - Update sub-collection repository: remove entities validation
    - Update simple collection repository: remove entities validation
    - Update multi collection repository: remove entities validation

- Set datetime

- Set rule

---


