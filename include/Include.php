<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/entity/library/ConstEntity.php');
include($strRootPath . '/src/entity/library/ToolBoxEntity.php');
include($strRootPath . '/src/entity/library/ToolBoxValidEntity.php');
include($strRootPath . '/src/entity/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/entity/exception/ValidationFailException.php');
include($strRootPath . '/src/entity/exception/CollectionItemClassPathInvalidFormatException.php');
include($strRootPath . '/src/entity/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/entity/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/entity/exception/CollectionGetConfigInvalidFormatException.php');
include($strRootPath . '/src/entity/api/EntityInterface.php');
include($strRootPath . '/src/entity/api/ItemEntityInterface.php');
include($strRootPath . '/src/entity/api/ValidEntityInterface.php');
include($strRootPath . '/src/entity/api/EntityCollectionInterface.php');
include($strRootPath . '/src/entity/api/ValidEntityCollectionInterface.php');
include($strRootPath . '/src/entity/model/DefaultEntity.php');
include($strRootPath . '/src/entity/model/DefaultItemEntity.php');
include($strRootPath . '/src/entity/model/DefaultValidEntity.php');
include($strRootPath . '/src/entity/model/ConfigEntity.php');
include($strRootPath . '/src/entity/model/ValidatorConfigEntity.php');
include($strRootPath . '/src/entity/model/DefaultEntityCollection.php');
include($strRootPath . '/src/entity/model/DefaultValidEntityCollection.php');

include($strRootPath . '/src/entity/fix/library/ConstFixEntity.php');
include($strRootPath . '/src/entity/fix/exception/CollectionItemClassPathInvalidFormatException.php');
include($strRootPath . '/src/entity/fix/model/FixEntityCollection.php');

include($strRootPath . '/src/entity/repository/library/ConstSaveEntity.php');
include($strRootPath . '/src/entity/repository/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/entity/repository/exception/IsNewInvalidFormatException.php');
include($strRootPath . '/src/entity/repository/api/SaveEntityInterface.php');
include($strRootPath . '/src/entity/repository/model/DefaultSaveEntity.php');
include($strRootPath . '/src/entity/repository/model/SaveConfigEntity.php');

include($strRootPath . '/src/entity/factory/library/ConstEntityFactory.php');
include($strRootPath . '/src/entity/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/entity/factory/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/entity/factory/api/EntityFactoryInterface.php');
include($strRootPath . '/src/entity/factory/model/DefaultEntityFactory.php');

include($strRootPath . '/src/entity/factory/fix/library/ConstFixEntityFactory.php');
include($strRootPath . '/src/entity/factory/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/entity/factory/fix/model/FixEntityFactory.php');

include($strRootPath . '/src/persistence/library/ToolBoxPersistor.php');
include($strRootPath . '/src/persistence/api/PersistorInterface.php');

include($strRootPath . '/src/repository/library/ConstRepository.php');
include($strRootPath . '/src/repository/library/ToolBoxEntityRepository.php');
include($strRootPath . '/src/repository/exception/PersistorInvalidFormatException.php');
include($strRootPath . '/src/repository/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/exception/EntityInvalidFormatException.php');
include($strRootPath . '/src/repository/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/exception/CollectionEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/repository/exception/CollectionRepositoryInvalidFormatException.php');
include($strRootPath . '/src/repository/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/exception/CollectionInvalidFormatException.php');
include($strRootPath . '/src/repository/exception/CollectionExecConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/api/RepositoryInterface.php');
include($strRootPath . '/src/repository/api/CollectionRepositoryInterface.php');
include($strRootPath . '/src/repository/model/DefaultRepository.php');
include($strRootPath . '/src/repository/model/DefaultCollectionRepository.php');

include($strRootPath . '/src/repository/sub_repository/library/ConstSubRepoRepository.php');
include($strRootPath . '/src/repository/sub_repository/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/sub_repository/exception/ExecConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/sub_repository/exception/SubEntityInvalidFormatException.php');
include($strRootPath . '/src/repository/sub_repository/exception/CriteriaInvalidFormatException.php');
include($strRootPath . '/src/repository/sub_repository/exception/CollectionRepositoryInvalidFormatException.php');
include($strRootPath . '/src/repository/sub_repository/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/sub_repository/exception/CollectionExecConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/sub_repository/library/ToolBoxSubRepo.php');
include($strRootPath . '/src/repository/sub_repository/model/SubRepoRepository.php');
include($strRootPath . '/src/repository/sub_repository/model/SubRepoCollectionRepository.php');

include($strRootPath . '/src/repository/simple/library/ConstSimpleRepository.php');
include($strRootPath . '/src/repository/simple/exception/CollectionExecConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/simple/model/SimpleRepository.php');
include($strRootPath . '/src/repository/simple/model/SimpleCollectionRepository.php');

include($strRootPath . '/src/repository/simple/fix/library/ConstFixSimpleRepository.php');
include($strRootPath . '/src/repository/simple/fix/exception/PersistorInvalidFormatException.php');
include($strRootPath . '/src/repository/simple/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/simple/fix/exception/CollectionEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/repository/simple/fix/exception/CollectionRepositoryInvalidFormatException.php');
include($strRootPath . '/src/repository/simple/fix/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/simple/fix/model/FixSimpleRepository.php');
include($strRootPath . '/src/repository/simple/fix/model/FixSimpleCollectionRepository.php');

include($strRootPath . '/src/repository/multi/library/ConstMultiRepository.php');
include($strRootPath . '/src/repository/multi/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/exception/IdInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/exception/CollectionRepositoryInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/exception/CollectionExecConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/model/MultiRepository.php');
include($strRootPath . '/src/repository/multi/model/MultiCollectionRepository.php');

include($strRootPath . '/src/repository/multi/fix/library/ConstFixMultiRepository.php');
include($strRootPath . '/src/repository/multi/fix/exception/PersistorInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/fix/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/fix/exception/CollectionEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/fix/exception/CollectionRepositoryInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/fix/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/repository/multi/fix/model/FixMultiRepository.php');
include($strRootPath . '/src/repository/multi/fix/model/FixMultiCollectionRepository.php');

include($strRootPath . '/src/datetime/library/ToolBoxDateTime.php');

include($strRootPath . '/src/datetime/factory/library/ConstDateTimeFactory.php');
include($strRootPath . '/src/datetime/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/datetime/factory/api/DateTimeFactoryInterface.php');
include($strRootPath . '/src/datetime/factory/model/DefaultDateTimeFactory.php');

include($strRootPath . '/src/validation/rule/library/ToolBoxEntityRule.php');

include($strRootPath . '/src/validation/rule/entity_collection/library/ConstEntityCollectionRule.php');
include($strRootPath . '/src/validation/rule/entity_collection/exception/ValidConfigInvalidFormatException.php');
include($strRootPath . '/src/validation/rule/entity_collection/model/EntityCollectionRule.php');

include($strRootPath . '/src/validation/rule/entity_collection/attribute_exist/library/ConstAttrExistEntityCollectionRule.php');
include($strRootPath . '/src/validation/rule/entity_collection/attribute_exist/exception/ValidConfigInvalidFormatException.php');
include($strRootPath . '/src/validation/rule/entity_collection/attribute_exist/model/AttrExistEntityCollectionRule.php');

include($strRootPath . '/src/validation/rule/validation_entity/library/ConstValidEntityRule.php');
include($strRootPath . '/src/validation/rule/validation_entity/exception/ValidConfigInvalidFormatException.php');
include($strRootPath . '/src/validation/rule/validation_entity/model/ValidEntityRule.php');

include($strRootPath . '/src/validation/rule/validation_entity_collection/library/ConstValidEntityCollectionRule.php');
include($strRootPath . '/src/validation/rule/validation_entity_collection/exception/ValidConfigInvalidFormatException.php');
include($strRootPath . '/src/validation/rule/validation_entity_collection/model/ValidEntityCollectionRule.php');

include($strRootPath . '/src/validation/rule/new_entity/library/ConstNewEntityRule.php');
include($strRootPath . '/src/validation/rule/new_entity/exception/ValidConfigInvalidFormatException.php');
include($strRootPath . '/src/validation/rule/new_entity/exception/ErrorConfigInvalidFormatException.php');
include($strRootPath . '/src/validation/rule/new_entity/model/NewEntityRule.php');

include($strRootPath . '/src/validation/rule/new_entity_collection/library/ConstNewEntityCollectionRule.php');
include($strRootPath . '/src/validation/rule/new_entity_collection/exception/ValidConfigInvalidFormatException.php');
include($strRootPath . '/src/validation/rule/new_entity_collection/exception/ErrorConfigInvalidFormatException.php');
include($strRootPath . '/src/validation/rule/new_entity_collection/model/NewEntityCollectionRule.php');